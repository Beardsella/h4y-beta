<div class="space_100px"></div>

<cfif isDefined("CurrentPage") AND #CurrentPage# NEQ "index.cfm">


<div class="panel-group hidden-print affix" id="accordion">
	<div class="panel panel-default">

		<div class="panel-heading">
			<h5 class="panel-title">
				<a href="#myCarousel">Home</a>
			</h5>
		</div>		
		<div class="panel-heading">
			<h5 class="panel-title">
				<a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">Products</a>
			</h5>
		</div>
		<div id="collapseOne" class="panel-collapse collapse">
			<div class="panel-body">
				<table class="table">
					<cfoutput query="get_top_cats">
						<tr>
							<td>
								<a href="#SEO_url#">#URLDecode(lcase(cat_name))#</a>
							</td>
						</tr>
					</cfoutput>                    	
				</table>
			</div>
		</div>

		<div class="panel-heading">
			<h5 class="panel-title">
				<a href="#who-we-are">Who We Are</a>
			</h5>
		</div>
		<div class="panel-heading">
			<h5 class="panel-title">
				<a href="#contact_details">Contact Us</a>
			</h5>
		</div>
		<div class="panel-heading">
			<h5 class="panel-title">
				<a href="Downloads/HPI-Catalog.zip">Download Catalog</a>
			</h5>
		</div>

	</div>
</div>

<cfelse>
<!--- IF ITS NOT THE HOME PAGE	 --->


<div class="panel-group hidden-print affix" id="accordion">
	<div class="panel panel-default">

		<div class="panel-heading">
			<h5 class="panel-title">
				<a href="index.cfm#myCarousel">Home</a>
			</h5>
		</div>		
		<div class="panel-heading">
			<h5 class="panel-title">
				<a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">Products</a>
			</h5>
		</div>
		<div id="collapseOne" class="panel-collapse collapse">
			<div class="panel-body">
				<table class="table">
					<cfoutput query="get_top_cats">
						<tr>
							<td>
								<a href="#SEO_url#">#URLDecode(lcase(cat_name))#</a>
							</td>
						</tr>
					</cfoutput>                    	
				</table>
			</div>
		</div>

		<div class="panel-heading">
			<h5 class="panel-title">
				<a href="index.cfm#who-we-are">Who We Are</a>
			</h5>
		</div>
		<div class="panel-heading">
			<h5 class="panel-title">
				<a href="index.cfm#contact_details">Contact Us</a>
			</h5>
		</div>
		<div class="panel-heading">
			<h5 class="panel-title">
				<a href="#">Download Catalog</a>
			</h5>
		</div>

	</div>
</div>


</cfif>
