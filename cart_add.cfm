<cfparam name="FORM.quantity" type="integer" default="1">
<cfparam name="FORM.id" type="integer">

<cfif IsDefined("session.client") AND session.client neq ''>
		<!--- HEAR4YOU if client is logged in, get the price break for this quantity --->
        <cfquery name="price_break_client" datasource="#application.dsn#" maxrows="1">
        SELECT bprice FROM product_price_breaks WHERE break_qty <= #FORM.quantity# AND client_id = #session.client# AND bproduct_id = #FORM.id# ORDER BY break_qty DESC
        </cfquery>
        <cfif price_break_client.recordcount gt 0>
        	<cfset pprice = price_break_client.bprice>
        <cfelse><!--- get the generic price breaks --->
            <cfquery name="price_break_std" datasource="#application.dsn#" maxrows="1">
            SELECT bprice FROM product_price_breaks WHERE break_qty <= #FORM.quantity# AND client_id is null AND bproduct_id = #FORM.id# ORDER BY break_qty DESC
            </cfquery>
        	<cfset pprice = price_break_std.bprice>
        </cfif>
<cfelse>
        <cfquery name="getdetail" datasource="#application.dsn#">
        SELECT id, price, delivery
        FROM products
        WHERE ID = #FORM.id#
        </cfquery>
        <cfif IsDefined("FORM.child_product") AND #FORM.child_product# NEQ "">
            <cfquery name="getkid" datasource="#application.dsn#">
            SELECT id, child_price
            FROM child_products
            WHERE ID = #FORM.child_product#
            </cfquery>
        </cfif>
        
        <cfif IsDefined("getkid.child_price") AND getkid.child_price NEQ "">
            <cfset pprice = getkid.child_price>
        <cfelse>
            <cfset pprice = getdetail.price>
        </cfif>
</cfif>

  <cfquery datasource="#application.dsn#">
  INSERT INTO shoppingcart (cartid, price, vat, delivery, quantity, prodid, cartdate, product_option, child_product) VALUES (
  '#session.visitorID#',
  <cfif IsDefined("pprice") AND #pprice# NEQ "">
    #numberformat(pprice/(1+session.vatrate), '.00')#
      <cfelse>
      0
  </cfif>
  ,
  <cfif IsDefined("pprice") AND #pprice# NEQ "">
    #numberformat(pprice-(pprice/(1+session.vatrate)), '.00')#
      <cfelse>
      0
  </cfif>
  ,
  <cfif IsDefined("getdetail.delivery") AND #getdetail.delivery# NEQ "">
    #getdetail.delivery#
      <cfelse>
      0
  </cfif>
  ,
  <cfif IsDefined("FORM.quantity") AND #FORM.quantity# NEQ "">
    #FORM.quantity#
      <cfelse>
      1
  </cfif>
,
  <cfif IsDefined("FORM.id") AND #FORM.id# NEQ "">
    #FORM.id#
      <cfelse>
      NULL
  </cfif>
,
#now()#
  ,
  <cfif IsDefined("FORM.product_option") AND #FORM.product_option# NEQ "">
    '#FORM.product_option#'
      <cfelse>
      NULL
  </cfif>
  ,
  <cfif IsDefined("FORM.child_product") AND #FORM.child_product# NEQ "">
    #FORM.child_product#
      <cfelse>
      NULL
  </cfif>
)
</cfquery>
<cflocation url="cart_view.cfm" addtoken="no">