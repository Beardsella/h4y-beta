<cfparam name="FORM.id" type="integer">

<cfsetting showdebugoutput="no" enablecfoutputonly="yes">

<cfif FORM.qty eq '' OR FORM.qty eq 0>
	<cfset quant = 1>
<cfelse>
	<cfset quant = FORM.qty>
</cfif>

<cfquery datasource="#application.dsn#">
UPDATE shoppingcart
SET quantity = #quant#
WHERE id = #form.id#
</cfquery> 

<cfquery name="linetotal" datasource="#application.dsn#">
SELECT sum(price*quantity) AS linegoods, sum(vat*quantity) AS linevat, vat_exempt, prodid
FROM shoppingcart
WHERE id = #FORM.id#
GROUP BY vat_exempt, prodid
</cfquery>

<cfquery name="prod" datasource="#application.dsn#">
SELECT vatexempt
FROM products
WHERE id = #linetotal.prodid#
</cfquery>

<!--- if customer qualifies, and the product qualifies as vat exempt then zero vat --->
<cfif linetotal.vat_exempt eq 0 OR linetotal.vat_exempt eq 1 AND prod.vatexempt eq 0>	
	<cfset thislinevat = linetotal.linevat>
<cfelse>
	<cfset thislinevat = numberformat(0.00, '0.00')>
</cfif>
<!--- check the cart value --->
<cfinvoke 
	component="cfcs.cart"
	method="get_totals"
	returnvariable="totals">
</cfinvoke>


<cfset result = structNew()>

<cfset result.id = #FORM.id#>
<cfset result.linetotal = #numberformat(linetotal.linegoods+thislinevat, '0.00')#>
<cfset result.linegoods = #numberformat(linetotal.linegoods, '0.00')#>
<cfset result.linevat = #numberformat(thislinevat, '0.00')#>
<cfset result.goods = #numberformat(totals.goods, '0.00')#>
<cfset result.tvat = #numberformat(totals.tvat, '0.00')#>
<cfset result.subtotal = #numberformat(totals.subtotal, '0.00')#>
<cfset result.ship = #numberformat(totals.delivery, '0.00')#>
<cfset result.total = #numberformat(totals.total, '0.00')#>

<cfwddx action="CFML2JS" input="#result#" toplevelvariable="r">	