<cfsetting showdebugoutput="no" enablecfoutputonly="yes">

<cfif IsDefined("FORM.pqty") AND FORM.pqty NEQ ''>
	<cfset cquantity = FORM.pqty>
<cfelse>
	<cfset cquantity = 1>
</cfif>


<cfif IsDefined("session.client") AND session.client neq ''>
		<cfif IsDefined("FORM.child") AND FORM.child NEQ '' AND FORM.child NEQ 9999>
                <cfquery name="price_break_client_child" datasource="#application.dsn#" maxrows="1">
                SELECT bprice FROM product_price_breaks WHERE bchild_id = #FORM.child# AND break_qty <= #cquantity# AND client_id = #session.client# ORDER BY break_qty DESC
                </cfquery>
        		<cfif price_break_client_child.recordcount gt 0>
                	<cfset pprice = price_break_client_child.bprice>
                <cfelse><!--- get the generic child price breaks --->
                    <cfquery name="price_break_std_child" datasource="#application.dsn#" maxrows="1">
                    SELECT bprice FROM product_price_breaks WHERE bchild_id = #FORM.child# AND break_qty <= #cquantity# AND client_id is null ORDER BY break_qty DESC
                    </cfquery>
                    <cfset pprice = price_break_std_child.bprice>
                </cfif>
        		<cfquery name="getprod" datasource="#application.dsn#" >
                SELECT product_id FROM child_products WHERE ID = #FORM.child#
                </cfquery>
				
                <cfset shipping = 0>
                <cfset pid = getprod.product_id>
            
        <cfelseif IsDefined("FORM.prodid") AND FORM.prodid NEQ '' AND FORM.prodid NEQ 9999>
            <cfquery name="getprod" datasource="#application.dsn#">
            SELECT price, delivery
            FROM products
            WHERE ID = #FORM.prodid#
            </cfquery>
        
            <cfset pprice = getprod.price>
            <cfset shipping = getprod.delivery>
            <cfset pid = FORM.prodid>
        </cfif>
<cfelse>
		<cfif IsDefined("FORM.child") AND FORM.child NEQ '' AND FORM.child NEQ 9999>
            <cfquery name="getkid" datasource="#application.dsn#">
            SELECT A.id, A.child_price, B.delivery, B.id AS prod
            FROM child_products A INNER JOIN products B ON A.product_id = B.id
            WHERE A.ID = #FORM.child#
            </cfquery>
        
            <cfset pprice = getkid.child_price>
            <cfset shipping = getkid.delivery>
            <cfset pid = getkid.prod>
            
        <cfelseif IsDefined("FORM.prodid") AND FORM.prodid NEQ '' AND FORM.prodid NEQ 9999>
            <cfquery name="getprod" datasource="#application.dsn#">
            SELECT price, delivery
            FROM products
            WHERE ID = #FORM.prodid#
            </cfquery>
        
            <cfset pprice = getprod.price>
            <cfset shipping = getprod.delivery>
            <cfset pid = FORM.prodid>
        </cfif>
</cfif>

  <cfquery datasource="#application.dsn#">
  INSERT INTO shoppingcart (cartid, price, vat, delivery, quantity, prodid, cartdate, child_product) VALUES (
  '#session.visitorID#',
  <cfif IsDefined("pprice") AND #pprice# NEQ "">
    #numberformat(pprice/(1+session.vatrate), '.00')#
      <cfelse>
      0
  </cfif>
  ,
  <cfif IsDefined("pprice") AND #pprice# NEQ "">
    #numberformat(pprice-(pprice/(1+session.vatrate)), '.00')#
      <cfelse>
      0
  </cfif>
  ,
  <cfif IsDefined("shipping") AND shipping NEQ "">
    #shipping#
      <cfelse>
      0
  </cfif>
  ,
      #cquantity#
,
  <cfif IsDefined("pid") AND pid NEQ "">
    #pid#
      <cfelse>
      NULL
  </cfif>
,
#now()#
  ,
  <cfif IsDefined("FORM.child") AND FORM.child NEQ "" AND FORM.child neq 9999>
    #FORM.child#
      <cfelse>
      NULL
  </cfif>
)
</cfquery>

<!--- check the cart value --->
<cfinvoke 
	component="cfcs.cart"
	method="get_totals"
	returnvariable="totals">
</cfinvoke>

<!--- set results--->
<cfset result = structNew()>
<cfif totals.delivery eq 0>
	<cfset result.rcartval = '&pound; ' & #numberformat(totals.total, '.00')#>
<cfelse>
	<cfset result.rcartval = '&pound; ' & #numberformat(totals.total, '.00')# & ' inc. delivery'>
</cfif>
<cfif IsDefined("FORM.child") AND FORM.child NEQ '' AND FORM.child NEQ 9999>
	<cfset result.thiskid = FORM.child>
<cfelse>
	<cfset result.thiskid = 0>
</cfif>

<cfwddx action="CFML2JS" input="#result#" toplevelvariable="r">	