<cfset path = expandPath("../email_errors")>

<!--- set the send from email address --->
<cfset sendfrom = 'steve@publicquest.co.uk'>

<!--- get the clients --->
<cfquery name="clientcount" datasource="#application.dsn#">
SELECT ID, client_email
FROM clients
WHERE eselect = 1 AND client_eunsubscribed = 0
</cfquery>

<!--- get the customers --->
<cfquery name="customercount" datasource="#application.dsn#">
SELECT custid, email
FROM customerinfo
WHERE eselect = 1 AND eunsubscribed = 0
</cfquery>

<!--- get the subscribers --->
<cfquery name="mailercount" datasource="#application.dsn#">
SELECT ID, memail
FROM mailing_list
WHERE eselect = 1 AND eunsubscribed = 0
</cfquery>

<!--- Get the message --->
<cfquery name="getmessage" datasource="#application.dsn#">
SELECT ID, econtent, esubject
FROM email_messages
WHERE ID = #FORM.message#
</cfquery>

<cfset totalcount = evaluate(clientcount.recordcount + customercount.recordcount + mailercount.recordcount)>

<!--- record in email batch table--->
<cfquery datasource="#application.dsn#">
INSERT INTO email_batch (bmessage, bvolume)
VALUES (#getmessage.ID#, #totalcount#)
</cfquery>
<!--- Get the new batch ID --->
<cfquery name="thisbatch" datasource="#application.dsn#">
SELECT max(ID) AS thisid
FROM email_batch
</cfquery>


<!--- send copy email to admin --->
<cfmail to="#sendfrom#" from="#sendfrom#" subject="Test copy email '#getmessage.esubject#'">
        	<!---Plain Text--->
            <cfmailpart type="text/plain" wraptext="60">
            	#URLDecode(getmessage.econtent)#
				<p>To choose not to receive future emails <a href="http://www.hear4you.com/goodbye.cfm?email=#sendfrom#&tbl=22">click here</a></p>
            </cfmailpart>
            <!--- Send rich formatted part. --->
            <cfmailpart type="text/html">
            	#URLDecode(getmessage.econtent)#
				<p>To choose not to receive future emails <a href="http://www.hear4you.com/goodbye.cfm?email=#sendfrom#&tbl=22">click here</a></p>
            </cfmailpart>
</cfmail>

<!--- send mail shot CLIENTS--->
<cfset errorno = 0>
<cfset send_count = 0>
<cfloop query="clientcount">
	<cftry>
		<cfmail to="#URLDecode(clientcount.client_email)#" from="#sendfrom#" subject="#getmessage.esubject#">
		<cfmailparam name="Disposition-Notification-To" value="#sendfrom#">
        	<!---Plain Text--->
            <cfmailpart type="text/plain" wraptext="60">
            	#URLDecode(getmessage.econtent)#
 				<span style="font-family:Arial, Verdana, Helvetica, sans-serif;font-size:0.7em">
 				To choose not to receive future emails <a href="http://www.hear4you.com/goodbye.cfm?email=#clientcount.client_email#&tbl=22">click here</a>
				</span>
           </cfmailpart>
            <!--- Send rich formatted part. --->
            <cfmailpart type="text/html">
            	#URLDecode(getmessage.econtent)#
 				<span style="font-family:Arial, Verdana, Helvetica, sans-serif;font-size:0.7em">
 				To choose not to receive future emails <a href="http://www.hear4you.com/goodbye.cfm?email=#clientcount.client_email#&tbl=22">click here</a>
				</span>
           </cfmailpart>
			
		</cfmail>
		<cfloop index="Z" from="1" to="5000">
		<!--- just a time delay--->
		</cfloop>
		<cfset send_count = send_count +1>
		
		<!--- record in emails_sent table--->
		<cfquery datasource="#application.dsn#">
		INSERT INTO emails_sent (ebatch, ecandidate) VALUES (#thisbatch.thisid#, #clientcount.ID#)
		</cfquery>
		
		<!--- if email address is not valid format record it as an error --->
		<cfcatch type="any">
		<!--- <cfdump var="#cfcatch#">--->
		<cfif errorno eq 0>
			<cffile action="write" nameconflict="overwrite"
				file="#path#/Email_errors_clients_#dateFormat(now(), "ddmmyy")#_errors.csv"
				output="#clientcount.client_email##chr(13)#"
		>
		
		<cfelse>
		<cffile action="append"
				file="#path#/Email_errors_clients_#dateFormat(now(), "ddmmyy")#_errors.csv"
				output="#clientcount.client_email##chr(13)#"
		>
		</cfif>
		<cfset errorno = errorno +1>
		<!--- and unsubscribe this email address --->
		<cfquery datasource="#application.dsn#">
		UPDATE clients
		SET client_eunsubscribed = 1
		WHERE ID = #clientcount.ID#
		</cfquery>
		</cfcatch>
	</cftry>
</cfloop>

<cfloop query="customercount"><!--- second loop to customers --->
	<cftry>
		<cfmail to="#URLDecode(customercount.email)#" from="#sendfrom#" subject="#getmessage.esubject#">
		<cfmailparam name="Disposition-Notification-To" value="#sendfrom#">
        	<!---Plain Text--->
            <cfmailpart type="text/plain" wraptext="60">
            	#URLDecode(getmessage.econtent)#
 				<span style="font-family:Arial, Verdana, Helvetica, sans-serif;font-size:0.7em">
 				To choose not to receive future emails <a href="http://www.hear4you.com/goodbye.cfm?email=#customercount.email#&tbl=33">click here</a>
				</span>
           </cfmailpart>
            <!--- Send rich formatted part. --->
            <cfmailpart type="text/html">
            	#URLDecode(getmessage.econtent)#
 				<span style="font-family:Arial, Verdana, Helvetica, sans-serif;font-size:0.7em">
 				To choose not to receive future emails <a href="http://www.hear4you.com/goodbye.cfm?email=#customercount.email#&tbl=33">click here</a>
				</span>
           </cfmailpart>
			
		</cfmail>
		<cfloop index="Z" from="1" to="5000">
		<!--- just a time delay--->
		</cfloop>
		<cfset send_count = send_count +1>
		
		<!--- record in emails_sent table--->
		<cfquery datasource="#application.dsn#">
		INSERT INTO emails_sent (ebatch, ecandidate) VALUES (#thisbatch.thisid#, #customercount.custid#)
		</cfquery>
		
		<!--- if email address is not valid format record it as an error --->
		<cfcatch type="any">
		<!--- <cfdump var="#cfcatch#">--->
		<cfif errorno eq 0>
			<cffile action="write" nameconflict="overwrite"
				file="#path#/Email_errors_custs_#dateFormat(now(), "ddmmyy")#_errors.csv"
				output="#customercount.email##chr(13)#"
		>
		
		<cfelse>
		<cffile action="append"
				file="#path#/Email_errors_custs_#dateFormat(now(), "ddmmyy")#_errors.csv"
				output="#customercount.email##chr(13)#"
		>
		</cfif>
		<cfset errorno = errorno +1>
		<!--- and unsubscribe this email address --->
		<cfquery datasource="#application.dsn#">
		UPDATE customerinfo
		SET eunsubscribed = 1
		WHERE custid = #customercount.custid#
		</cfquery>
		</cfcatch>
	</cftry>
</cfloop>

<cfloop query="mailercount"><!--- third loop to subscribers --->
	<cftry>
		<cfmail to="#mailercount.memail#" from="#sendfrom#" subject="#getmessage.esubject#">
		<cfmailparam name="Disposition-Notification-To" value="#sendfrom#">
        	<!---Plain Text--->
            <cfmailpart type="text/plain" wraptext="60">
            	#URLDecode(getmessage.econtent)#
 				<span style="font-family:Arial, Verdana, Helvetica, sans-serif;font-size:0.7em">
 				To choose not to receive future emails <a href="http://www.hear4you.com/goodbye.cfm?email=#mailercount.memail#&tbl=44">click here</a>
				</span>
           </cfmailpart>
            <!--- Send rich formatted part. --->
            <cfmailpart type="text/html">
            	#URLDecode(getmessage.econtent)#
 				<span style="font-family:Arial, Verdana, Helvetica, sans-serif;font-size:0.7em">
 				To choose not to receive future emails <a href="http://www.hear4you.com/goodbye.cfm?email=#mailercount.memail#&tbl=44">click here</a>
				</span>
           </cfmailpart>
			
		</cfmail>
		<cfloop index="Z" from="1" to="5000">
		<!--- just a time delay--->
		</cfloop>
		<cfset send_count = send_count +1>
		
		<!--- record in emails_sent table--->
		<cfquery datasource="#application.dsn#">
		INSERT INTO emails_sent (ebatch, ecandidate) VALUES (#thisbatch.thisid#, #mailercount.ID#)
		</cfquery>
		
		<!--- if email address is not valid format record it as an error --->
		<cfcatch type="any">
		<!--- <cfdump var="#cfcatch#">--->
		<cfif errorno eq 0>
			<cffile action="write" nameconflict="overwrite"
				file="#path#/Email_errors_mailers_#dateFormat(now(), "ddmmyy")#_errors.csv"
				output="#mailercount.memail##chr(13)#"
		>
		
		<cfelse>
		<cffile action="append"
				file="#path#/Email_errors_mailers_#dateFormat(now(), "ddmmyy")#_errors.csv"
				output="#mailercount.memail##chr(13)#"
		>
		</cfif>
		<cfset errorno = errorno +1>
		<!--- and unsubscribe this email address --->
		<cfquery datasource="#application.dsn#">
		UPDATE mailing_list
		SET eunsubscribed = 1
		WHERE ID = #mailercount.ID#
		</cfquery>
		</cfcatch>
	</cftry>
</cfloop>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/Templates/HPIL_admin.dwt.cfm" codeOutsideHTMLIsLocked="false" -->
<head>
<cfif NOT IsDefined ("session.admininitials") OR session.admininitials eq 999>
	<cflocation url="index.cfm?passwordcheck=failed" addtoken="no">
</cfif>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<title>Web admin</title>

<link href="../css/HPIL_admin.css" rel="stylesheet" type="text/css" media="all">




</head>

<body>
<div id="container">
	<div id="logo"><img src="../images/HPIL_logo_gif.gif" /></div>
 	<div class="clearline"></div>
	<div id="nav">
			<cfinclude template="nav_admin.cfm">	
	</div>
	<div id="pagecontent">
	  	<div id="pagetitle">
	  	
		Mail sent		  
		
	  	</div>
<div class="clearline"></div>
		

				<p>You have sent <cfoutput>#totalcount#</cfoutput> emails.</p>
					<p><cfif errorno gt 0>This included <cfoutput>#errorno#</cfoutput> invalid addresses, which have now been marked as 'unsubscribed'. <cfelse>There were no invalid addresses.</cfif></p>
	
	
	</div>
</div>
<div class="clearline"></div>
<!--- end container div --->
</body>
</html>
