<cfif NOT IsDefined ("session.admininitials")>
<cflocation url="index.cfm?passwordcheck=failed">
</cfif>
<cfset path = expandPath("../product-images/")>

<cfquery datasource="#application.dsn#">
DELETE
FROM products
WHERE id = #URL.id#
</cfquery>

<cfquery datasource="#application.dsn#">
DELETE
FROM product_cats
WHERE product_id = #URL.id#
</cfquery>

<cfquery datasource="#application.dsn#">
DELETE
FROM product_tags
WHERE product_id = #URL.id#
</cfquery>

<cftry>
	<cffile action="delete" file="#path##URL.id#.jpg">
	
	<cffile action="delete" file="#path#thumbs/#URL.id#.jpg">
<cfcatch type="any"></cfcatch>
</cftry>

<cflocation url="products.cfm" addtoken="no">
