<cfsetting requesttimeout="3600">

<cfset path = expandPath("../empty_me/")>

<!--- first a bit of housekeeping --->
<cfdirectory directory="#path#" action="list" name="currentDir">
  <cfloop query="currentDir">
<cftry>
	<cffile action="delete" file="#path##currentDir.Name#">
<cfcatch></cfcatch>
</cftry>
  </cfloop>

<cfset CurrentPage=GetFileFromPath(GetTemplatePath())>
<cfif IsDefined("FORM.MM_InsertRecord") AND FORM.MM_InsertRecord EQ "form1">
  <cfquery datasource="#application.dsn#">
    INSERT INTO Products (in_stock, displaythis, featured, supplier_code, title, Description, technical, price, price_was, delivery, product_options, prelated, vatexempt) VALUES (
  <cfif IsDefined("FORM.in_stock")>
    1
      <cfelse>
    0
  </cfif>
     ,
  <cfif IsDefined("FORM.displaythis")>
    1
      <cfelse>
    0
  </cfif>
     ,
  <cfif IsDefined("FORM.featured")>
    1
      <cfelse>
    0
  </cfif>
     ,
  <cfif IsDefined("FORM.supplier_code") AND #FORM.supplier_code# NEQ "">
    '#FORM.supplier_code#'
      <cfelse>
    NULL
  </cfif>
   ,
  <cfif IsDefined("FORM.title") AND #FORM.title# NEQ "">
    '#FORM.title#'
      <cfelse>
    NULL
  </cfif>
    ,
  <cfif IsDefined("FORM.Description") AND #FORM.Description# NEQ "">
    '#FORM.Description#'
      <cfelse>
    NULL
  </cfif>
    ,
  <cfif IsDefined("FORM.technical") AND #FORM.technical# NEQ "">
    '#FORM.technical#'
      <cfelse>
    NULL
  </cfif>
    ,
  <cfif IsDefined("FORM.price") AND #FORM.price# NEQ "">
    #FORM.price#
      <cfelse>
    NULL
  </cfif>
    ,
  <cfif IsDefined("FORM.price_was") AND #FORM.price_was# NEQ "">
    #FORM.price_was#
      <cfelse>
    NULL
  </cfif>
    ,
  <cfif IsDefined("FORM.delivery") AND #FORM.delivery# NEQ "">
    #FORM.delivery#
      <cfelse>
    NULL
  </cfif>
    ,
  <cfif IsDefined("FORM.product_options") AND #FORM.product_options# NEQ "">
    '#URLEncodedFormat(FORM.product_options)#'
      <cfelse>
    NULL
  </cfif>
    ,
  <cfif IsDefined("FORM.prelated") AND #FORM.prelated# NEQ "">
    '#FORM.prelated#'
      <cfelse>
    NULL
  </cfif>
	,
  <cfif IsDefined("FORM.vatexempt")>
    1
      <cfelse>
    0
  </cfif>
)
  </cfquery>
	<cfquery name="getid" datasource="#application.dsn#">
    SELECT max(id) AS thisid FROM products
    </cfquery>
 <!--- Upload image and resize, make a thumbnail too --->
  
  	<cfif IsDefined("FORM.newImage") AND #FORM.newImage# NEQ "">
		
			<cfset accept_upload = 1>
        
            <!--- only allow these mime types. Edit as required --->
            <cffile action="upload" filefield="newImage" destination="#GetTempDirectory()#" nameconflict="overwrite" result="uploadResult" accept="image/jpg, image/bmp, image/gif, image/jpeg">
            
            <!--- allow images only if needed --->
            <cfif Not IsImageFile(getTempDirectory() & uploadResult.serverFile)>
                <cffile action="delete" file="#getTempDirectory()##uploadResult.serverFile#">
                <cfset accept_upload = 0>
            </cfif>
            
            <!--- only allow these file extensions. Edit as required --->
            <cfif NOT ListFindNoCase("bmp,gif,jpg,jpeg,png", uploadResult.ServerFileExt)>
                <cffile action="delete" file="#getTempDirectory()##uploadResult.serverFile#">
            
                <cfset display_error = 'This file type cannot currently be uploaded. Please contact PQL.'>
                <cfset accept_upload = 0>
            
            </cfif>
            
            <cfif accept_upload eq 1>
            
                    <!--- IMAGES: move the file. #path# is normally already set in the page header---> 
                    <cfimage action="WRITE" source="#getTempDirectory()##uploadResult.serverFile#" destination="#path##getid.thisid#.jpg" overwrite="yes">
                            
            </cfif>
                  
            <!--- delete the temp file in all cases --->
            <cffile action="delete" file="#getTempDirectory()##uploadResult.serverFile#">
		
        <cfimage source="#path##getid.thisid#.jpg" name="lpic">		
<!--- make big image --->
		<cfset ImageScaleToFit(lpic,800,600)>

<cfimage
    action = "write"
    destination = "../product-images/#getid.thisid#.jpg"
    source = "#lpic#"
    overwrite = "yes">
		
<!--- make thumbnail --->
		<cfset ImageScaleToFit(lpic,200,200)>

<cfimage
    action = "write"
    destination = "../product-images/thumbs/#getid.thisid#.jpg"
    source = "#lpic#"
    overwrite = "yes">

</cfif>
    
  <cflocation url="images_additional.cfm?id=#getid.thisid#" addtoken="no">
</cfif>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"><html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/templates/HPIL_admin.dwt.cfm" codeOutsideHTMLIsLocked="false" -->
<head>
<cfif NOT IsDefined ("session.admininitials") OR session.admininitials eq 999>
	<cflocation url="index.cfm?passwordcheck=failed" addtoken="no">
</cfif>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<title>Administration</title>

<link href="../css/HPIL_admin.css" rel="stylesheet" type="text/css" media="all">



</head>

<body>
<div id="container">
	<div id="logo"><img src="../images/HPIL_logo_gif.gif" /></div>
 	<div class="clearline"></div>
	<div id="nav">
			<cfinclude template="nav_admin.cfm">	
	</div>
	<div id="pagecontent">
	  	<div id="pagetitle">
	  	
		ADD A NEW PRODUCT
	  	</div>
<div class="clearline"></div>
		

          <table width="96%" border="0">
            <tr>
              <td>
                <cfform action="#CurrentPage#" method="post" enctype="multipart/form-data" name="form1" id="form1">
                  <table>
                    <tr>
                      <td class="formlabel">In Stock:</td>
                      <td><input name="in_stock" type="checkbox" value="" checked="checked"> 
                      tick if in stock</td>
                    </tr>
                    <tr>
                      <td class="formlabel">Display this:</td>
                      <td><input name="displaythis" type="checkbox" value="" checked="checked"> 
                      tick this to display</td>
                    </tr>
                    <tr>
                      <td class="formlabel">Vat exempt ?</td>
                      <td><input name="vatexempt" type="checkbox" value=""> 
                      tick if this can be vat exempt</td>
                    </tr>
                    <tr>
                      <td class="formlabel">Feature this:</td>
                      <td><input name="featured" type="checkbox" value=""> 
                      tick this to display on home page</td>
                    </tr>
                    <tr>
                      <td class="formlabel">Supplier code:</td>
                      <td><input type="text" name="supplier_code" value="" size="40" /></td>
                    </tr>
                    <tr>
                      <td class="formlabel">Name:</td>
                      <td><input type="text" name="title" value="" size="70" /></td>
                    </tr>
                    <tr>
                      <td class="formlabel">Description:</td>
                      <td>
				<textarea name="Description" width="500" height="400" richtext="true" toolbar="pql" fontsizes="1/1,2/2,3/3,4/4,5/5,6/6,7/7"></textarea>                      
					</td>
                    </tr>
                     <tr>
                      <td class="formlabel">Technical spec:</td>
                      <td>Enter the item label, then a pipe (|), then the text. One item per line.</td>
                    </tr>
                       <tr>
                      <td class="formlabel">&nbsp;</td>
                      <td>
				<textarea name="technical" width="500" height="400" richtext="true" toolbar="pql" fontsizes="1/1,2/2,3/3,4/4"></textarea>                      
					</td>
                    </tr>
                 <tr>
                      <td class="formlabel">Price:</td>
                      <td><input type="text" name="price" value="" size="6" /></td>
                    </tr>
                    <tr>
                      <td class="formlabel">Price was:</td>
                      <td><input type="text" name="price_was" value="" size="6" />&nbsp;optional field, displays in red next to price</td>
                    </tr>
                    <tr>
                      <td class="formlabel">Delivery:</td>
                      <td><input type="text" name="delivery" value="" size="6" /></td>
                    </tr>
                     <tr>
                      <td class="formlabel">Choices:</td>
                      <td><textarea name="product_options" cols="50" rows="3"></textarea>With a comma after all but the last</td>
                    </tr>
                   <tr>
                      <td class="formlabel">Upload Photo:</td>
                      <td><input name="newImage" type="file" /></td>
                    </tr>
                   <tr>
                      <td class="formlabel">Related product IDs:</td>
                      <td><input type="text" name="prelated" value="" size="30" />&nbsp;Comma separated</td>
                 </tr>
                 <tr>
                     <td class="formlabel">&nbsp;</td>
                     <td>&nbsp;</td>
                   </tr>
                    <tr>
                      <td>&nbsp;</td>
                      <td><input type="submit" value="Add product" /></td>
                    </tr>
                  </table>
                  <input type="hidden" name="MM_InsertRecord" value="form1" />
                </cfform>
              </td>
            </tr>
          </table>
        
	</div>
</div>
<div class="clearline"></div>
<!--- end container div --->
</body>
</html>