<cfsetting showdebugoutput="no" enablecfoutputonly="yes">

<cfif IsDefined("FORM.catid") AND IsDefined("FORM.listid")>
	<cfquery datasource="#application.dsn#">
	INSERT INTO product_cats (category_id, product_id)
	VALUES (#FORM.catid#, #FORM.listid#)
	</cfquery>
	
	<cfquery name="getcats" datasource="#application.dsn#">
	SELECT A.id, B.cat_name, B.parentid
	FROM product_cats A INNER JOIN categories B ON A.category_id = B.id
	WHERE A.product_id = #FORM.listid#
	ORDER BY A.id ASC
	</cfquery>
<cfelseif IsDefined("FORM.catlistid") AND FORM.catlistid neq ''>

	<cfquery datasource="#application.dsn#">
	DELETE FROM product_cats
	WHERE id = #FORM.catlistid#
	</cfquery>

	<cfquery name="getcats" datasource="#application.dsn#">
	SELECT A.id, B.cat_name, B.parentid
	FROM product_cats A INNER JOIN categories B ON A.category_id = B.id
	WHERE A.product_id = #FORM.listid#
	ORDER BY A.id ASC
	</cfquery>

</cfif>


<cfset result = structNew()>

<cfif getcats.recordcount>
	<cfset result.message = '<p>This listing will appear in the following categories.</p>
	<p>Click remove to unassign a category.</p>
	<table cellpadding="6">'>
	<cfloop query="getcats">
<!--- get the parents name too --->
	<cfquery name="getparent" datasource="#application.dsn#">
	SELECT cat_name
	FROM categories
	WHERE id = #getcats.parentid#
	</cfquery>
	
		<cfset result.message = result.message & '<tr><td>' & #getparent.cat_name# & '&nbsp;&gt;&nbsp;' & #getcats.cat_name# & '</td><td><a href="##" onClick="removeCat(' & #FORM.listid# & ',' & #getcats.id# & ')">remove</a></td></tr>'>
	</cfloop>
    <cfset result.message = result.message & '</table>'>
<cfelse>
	<cfset result.message = 'This listing is not assigned to any categories'>
</cfif>

<cfwddx action="CFML2JS" input="#result#" toplevelvariable="r">	
