<cfset CurrentPage=GetFileFromPath(GetBaseTemplatePath())>
<cfquery name="allprods" datasource="#application.dsn#">
SELECT id, title
FROM products
<cfif IsDefined("FORM.prodtitle") AND FORM.prodtitle neq ''>
	WHERE title LIKE '%#FORM.prodtitle#%'
</cfif>
ORDER BY title ASC
</cfquery>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/templates/HPIL_admin.dwt.cfm" codeOutsideHTMLIsLocked="false" -->
<head>
<cfif NOT IsDefined ("session.admininitials") OR session.admininitials eq 999>
	<cflocation url="index.cfm?passwordcheck=failed" addtoken="no">
</cfif>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<title>Hearing Aids and Speech Amplification</title>


<link href="../css/HPIL_admin.css" rel="stylesheet" type="text/css" media="all">



</head>

<body>
<div id="container">
	<div id="logo"><img src="../images/HPIL_logo_gif.gif" /></div>
 	<div class="clearline"></div>
	<div id="nav">
			<cfinclude template="nav_admin.cfm">	
	</div>
	<div id="pagecontent">
	  	<div id="pagetitle">
	  	Products
	  	</div>
<div class="clearline"></div>
		

<table width="500" border="0">
  <tr>
    <td colspan="3"><hr color="#999999" /></td>
  </tr>
  <tr>
    <td colspan="3"><cfoutput>
    	<form action="#CurrentPage#" method="post" enctype="multipart/form-data" name="prodsearch">
        <table width="300" border="0">
          <tr>
            <td colspan="3">Filter Products</td>
            </tr>
          <tr>
            <td width="73" class="formlabel">Name</td>
            <td colspan="2"><input type="text" name="prodtitle" id="prodtitle" <cfif IsDefined("FORM.prodtitle") AND FORM.prodtitle neq ''>value="<cfoutput>#FORM.prodtitle#</cfoutput>"</cfif>></td>
            </tr>
          <tr>
            <td>&nbsp;</td>
            <td width="162">&nbsp;</td>
            <td width="51"><input type="submit" name="button" id="button" value="Filter" /></td>
          </tr>
		</table>
		</form>
    </cfoutput>
	</td>
  </tr>
  <tr>
    <td colspan="3"><hr color="#999999" /></td>
  </tr>

  
  
<cfoutput query="allprods">
  <cfquery datasource="#application.DSN#" name="prod_pictures">
    SELECT *
    FROM product_images
    WHERE product_id = #id#
  </cfquery>

  <tr>
    <td>
	<img src="../product-images/thumbs/#prod_pictures.image_name#" height="60" /><br />
	</td>
    <td>#URLDecode(title)#</td>
    <td><a href="edit_product.cfm?id=#id#">edit</a></td>
    <td><a href="product_delete_check.cfm?id=#id#">delete</a></td>
    
  </tr>
</cfoutput>

  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
</table>



	</div>
</div>
<div class="clearline"></div>
<!--- end container div --->
</body>
</html>
