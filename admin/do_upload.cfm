<cfsetting requesttimeout="3600">

<cfset path = expandPath("../uploaded_files/")>

<!--- upload new file --->
<cfif IsDefined("FORM.MM_UpdateRecord1") AND FORM.MM_UpdateRecord1 EQ "form1">

	<cfif IsDefined("FORM.newUpload") AND FORM.newUpload NEQ "">
		
			<cfset accept_upload = 1>
        
            <!--- only allow these mime types. Edit as required --->
            <cffile action="upload" filefield="newUpload" destination="#GetTempDirectory()#" nameconflict="overwrite" result="uploadResult" accept="image/jpg, application/msword, image/bmp, image/gif, image/jpeg, application/pdf, application/rtf, text/plain, application/x-excel">
            
            
            <!--- only allow these file extensions. Edit as required --->
            <cfif NOT ListFindNoCase("pdf,doc,rtf,txt,bmp,gif,jpg,jpeg,png,xls", uploadResult.ServerFileExt)>
                <cffile action="delete" file="#getTempDirectory()##uploadResult.serverFile#">
            
                <cfset display_error = 'This file type cannot currently be uploaded. Please contact PQL.'>
                <cfset accept_upload = 0>
            
            </cfif>
            
            <cfif accept_upload eq 1>
            
                    <!--- NON IMAGES: move the file. #path# is normally already set in the page header --->
                    <cffile action="copy" source="#getTempDirectory()##uploadResult.serverFile#" destination="#path#/#uploadResult.serverFile#" nameconflict="makeunique">
                    
                    <cfset sf = uploadResult.serverFile>
                    
            </cfif>
                  
            <!--- delete the temp file in all cases --->
            <cffile action="delete" file="#getTempDirectory()##uploadResult.serverFile#">
        
          <cfquery datasource="#application.dsn#">
            INSERT INTO uploads (filename, filelabel, product_id)
            VALUES ('#sf#', '#URLEncodedformat(FORM.filelabel)#'
            ,
          <cfif IsDefined("FORM.prodid") AND #FORM.prodid# NEQ "">
            #FORM.prodid#
              <cfelse>
            NULL
          </cfif>
            )
          </cfquery>
		  <cfif IsDefined("FORM.prodid") AND FORM.prodid NEQ "">
                <cflocation url="uploads_edit.cfm?id=#FORM.prodid#" addtoken="no">
          <cfelse>
                <cflocation url="uploaded_files.cfm" addtoken="no">
          </cfif>
	</cfif>
</cfif>
