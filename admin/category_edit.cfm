<cfsetting requesttimeout="3600">

<cfset CurrentPage=GetFileFromPath(GetTemplatePath())>
<cfset path = expandPath("../empty_me/")>
<cfset final_path = expandPath("../product-images/")>

<!--- first a bit of housekeeping --->
<cfdirectory directory="#path#" action="list" name="currentDir">
  <cfloop query="currentDir">
<cftry>
  <cffile action="delete" file="#path##currentDir.Name#">
<cfcatch></cfcatch>
</cftry>
  </cfloop>


<cfif isDefined("FORM.meta_tag_update") AND FORM.meta_tag_update NEQ "">
  <cfquery datasource="#application.DSN#">
    INSERT INTO meta_tags (name, cat_id)
    VALUES ("#urlEncodedFormat(new_meta_tag)#", '#FORM.ID#')
  </cfquery>
</cfif>


<cfif IsDefined("FORM.main_var_update") AND FORM.main_var_update EQ "form1">
<cfif IsDefined("FORM.main_var_update") AND FORM.main_var_update EQ "form1">


 <!--- Upload image and resize, make a thumbnail too --->
  
    <cfif IsDefined("FORM.cat_img") AND #FORM.cat_img# NEQ "">
    
      <cfset accept_upload = 1>
        
            <!--- only allow these mime types. Edit as required --->
            <cffile action="upload" filefield="cat_img" destination="#GetTempDirectory()#" nameconflict="overwrite" result="uploadResult" accept="image/jpg, image/jpeg">
            
            <!--- allow images only if needed --->
            <cfif Not IsImageFile(getTempDirectory() & uploadResult.serverFile)>
                <cffile action="delete" file="#getTempDirectory()##uploadResult.serverFile#">
                <cfset accept_upload = 0>
            </cfif>
            
            <!--- only allow these file extensions. Edit as required --->
            <cfif NOT ListFindNoCase("jpg, jpeg", uploadResult.ServerFileExt)>
                <cffile action="delete" file="#getTempDirectory()##uploadResult.serverFile#">
            
                <cfset display_error = 'This file type cannot currently be uploaded. Please contact PQL.'>
                <cfset accept_upload = 0>
            
            </cfif>
            
            <cfif accept_upload eq 1>


<cfif IsDefined("FORM.img_title") AND FORM.img_title NEQ "">



  <cfset new_name_with_no_spaces = #REReplace('#URLDecode(FORM.img_title)#',"[^0-9A-Za-z &/]","","all")# >

  <cfset new_name_with_no_spaces = Replace('#new_name_with_no_spaces#', ' ', '-', 'All')>

  <cfset new_name_with_no_spaces = Replace('#new_name_with_no_spaces#', '/', '-', 'All')>

  <cfset new_name_with_no_spaces = Replace('#new_name_with_no_spaces#', '--', '', 'All')>

  <cfset new_name_with_no_spaces = Replace('#new_name_with_no_spaces#', '---', '', 'All')>

  <cfset new_name_with_no_spaces = Replace('#new_name_with_no_spaces#', '&', 'and', 'All')>

  <cfset new_name_with_no_spaces = "#new_name_with_no_spaces#" & ".jpg">

  <cfset new_name_with_no_spaces = Replace('#new_name_with_no_spaces#', '-.jpg', '.jpg', 'All')>

  <cfset new_name_with_no_spaces = "#LCase(new_name_with_no_spaces)#">

<cfelse>

  <cfset new_name_with_no_spaces = #REReplace('#URLDecode(uploadResult.serverFile)#',"[^0-9A-Za-z &/]","","all")# >

  <cfset new_name_with_no_spaces = Replace('#new_name_with_no_spaces#', ' ', '-', 'All')>

  <cfset new_name_with_no_spaces = Replace('#new_name_with_no_spaces#', '/', '-', 'All')>

  <cfset new_name_with_no_spaces = Replace('#new_name_with_no_spaces#', '--', '', 'All')>

  <cfset new_name_with_no_spaces = Replace('#new_name_with_no_spaces#', '---', '', 'All')>

  <cfset new_name_with_no_spaces = Replace('#new_name_with_no_spaces#', '&', 'and', 'All')>

  <cfset new_name_with_no_spaces = "#LCase(new_name_with_no_spaces)#">

</cfif>
              
            
                    <!--- IMAGES: move the file. #path# is normally already set in the page header---> 
                    <cfimage action="WRITE" source="#getTempDirectory()##uploadResult.serverFile#" destination="#path##new_name_with_no_spaces#" overwrite="yes">
                            
            </cfif>
                  
            <!--- delete the temp file in all cases --->
            <cffile action="delete" file="#getTempDirectory()##uploadResult.serverFile#">
    
    <cfimage source="#path##New_name_with_no_spaces#" name="lpic">    
    
    <!--- make big image --->
    <cfset ImageScaleToFit(lpic,1000,1000)>

<cfimage
    action = "write"
    destination = "../category_images/#New_name_with_no_spaces#"
    source = "#lpic#"
    overwrite = "yes">
    
<!--- make thumbnail --->
    <cfset ImageScaleToFit(lpic,200,200)>

<cfimage
    action = "write"
    destination = "../category_images/thumbs/#New_name_with_no_spaces#"
    source = "#lpic#"
    overwrite = "yes">

  <cfquery datasource="#application.dsn#">
    UPDATE categories 
    SET cat_img = '#new_name_with_no_spaces#'
    WHERE ID=#FORM.ID#
  </cfquery>

</cfif>




</cfif>

<cfset formatted_seo_url = "">

<cfif isDefined("FORM.SEO_URL") AND FORM.SEO_URL NEQ "">

  <cfset formatted_seo_url = #REReplace('#URLDecode(FORM.SEO_URL)#',"[^0-9A-Za-z &/-]","","all")# >

  <cfset formatted_seo_url = Replace('#formatted_seo_url#', ' ', '-', 'All')>

  <cfset formatted_seo_url = Replace('#formatted_seo_url#', '/', '-', 'All')>

  <cfset formatted_seo_url = Replace('#formatted_seo_url#', '--', '', 'All')>

  <cfset formatted_seo_url = Replace('#formatted_seo_url#', '---', '', 'All')>

  <cfset formatted_seo_url = Replace('#formatted_seo_url#', '&', 'and', 'All')>

  <cfset formatted_seo_url = Replace('#formatted_seo_url#', 'cfm', '', 'All')>

  <cfset formatted_seo_url_suffix = Right('#formatted_seo_url#', 3)>

  <cfif formatted_seo_url_suffix neq "cfm">
    
    <cfset formatted_seo_url = "#formatted_seo_url#" & ".cfm">

  </cfif>

  <cfset formatted_seo_url_prefix = left('#formatted_seo_url#', 1)>

  <cfif formatted_seo_url_prefix eq "-">
    
    <cfset formatted_seo_url_length = #len(formatted_seo_url)#>
    <cfset formatted_seo_url_length = #formatted_seo_url_length# - 1>
    <cfset formatted_seo_url = right('#formatted_seo_url#', #formatted_seo_url_length#)>

  </cfif>

  <cfset formatted_seo_url = Replace('#formatted_seo_url#', '-.cfm', '.cfm', 'All')>

  <cfset formatted_seo_url = "#LCase(formatted_seo_url)#">




<cfquery datasource="#application.dsn#" name="seo_name_test">
  SELECT * 
  FROM categories
  WHERE id = #URL.id#
</cfquery>



</cfif>

  <cfquery datasource="#application.dsn#">
    UPDATE categories SET cat_name=
  <cfif IsDefined("FORM.cat_name") AND #FORM.cat_name# NEQ "">
    '#URLEncodedFormat(FORM.cat_name)#'
      <cfelse>
    NULL
  </cfif>
  , SEO_url=
  <cfif IsDefined("FORM.SEO_url") AND #FORM.SEO_url# NEQ "">
    '#formatted_seo_url#'
      <cfelse>
    NULL
  </cfif>
    , cat_description=
  <cfif IsDefined("FORM.cat_description") AND #FORM.cat_description# NEQ "">
    '#urlEncodedFormat(FORM.cat_description)#'
      <cfelse>
    NULL
  </cfif>
      , meta_description=
  <cfif IsDefined("FORM.meta_description") AND #FORM.meta_description# NEQ "">
    '#urlEncodedFormat(FORM.meta_description)#'
      <cfelse>
    NULL
  </cfif>
    , meta_title=
  <cfif IsDefined("FORM.meta_title") AND #FORM.meta_title# NEQ "">
    '#FORM.meta_title#'
      <cfelse>
    NULL
  </cfif>
    , img_title=
  <cfif IsDefined("FORM.img_title") AND #FORM.img_title# NEQ "">
    '#urlEncodedFormat(FORM.img_title)#'
      <cfelse>
    NULL
  </cfif>
    WHERE ID=#FORM.ID#
  </cfquery>

  </cfif>

  <cfquery name="get_cat" datasource="#application.dsn#">
    SELECT * 
    FROM categories 
    WHERE ID = #URL.id#
  </cfquery>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/templates/HPIL_admin.dwt.cfm" codeOutsideHTMLIsLocked="false" -->
<head>
<cfif NOT IsDefined ("session.admininitials") OR session.admininitials eq 999>
	<cflocation url="index.cfm?passwordcheck=failed" addtoken="no">
</cfif>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<title>Hearing Aids and Speech Amplification</title>


<link href="../css/HPIL_admin.css" rel="stylesheet" type="text/css" media="all">


<!-- Bootstrap CSS-->
 <link href="../css/bootstrap.css" rel="stylesheet">  



<!--- get existing images --->
<cfquery name="getpics" datasource="#application.dsn#">
SELECT id, image_name, image_caption FROM product_images WHERE product_id = #url.id#
</cfquery>



<!-- Place inside the <head> of your HTML -->
<script type="text/javascript" src="../js/tinymce/tinymce.min.js"></script>
<script type="text/javascript">
tinyMCE.init({
    mode : "textareas",
    editor_selector : "mytextarea"
});
</script>



</head>

<body>
<div id="container">
	<div id="logo"><img src="../images/HPIL_logo_gif.gif" /></div>
 	<div class="clearline"></div>
	<div id="nav">
			<cfinclude template="nav_admin.cfm">	
	</div>
	<div id="pagecontent">

<cfif isDefined("redirect") AND redirect EQ "true">
      <cfoutput><h3><a href="../#URLDecode(get_cat.seo_url)#" target="_blank">#URLDecode(get_cat.cat_name)#</a> Updated!</h3></cfoutput>
<cfelseif isDefined("get_cat.seo_url") AND #get_cat.seo_url# NEQ "">
      <cfoutput><h3>Edit <a href="../#URLDecode(get_cat.seo_url)#" target="_blank">#URLDecode(get_cat.cat_name)#</a></h3></cfoutput>
<cfelse>
      <cfoutput><h3>Edit <a href="../product_detail.cfm?id=#get_cat.id#" target="_blank">#URLDecode(get_cat.cat_name)#</a></h3></cfoutput>
</cfif>

<div class="clearline"></div>

<cfoutput>
<form method="post" name="form1" action="category_edit.cfm?id=#URL.id#" enctype="multipart/form-data"></cfoutput>
  <table cellpadding="4" cellspacing="4" class="edit_table" style="width:100%;">
     <tr>
      <td colspan="4">&nbsp;</td>
    </tr>
   <tr>
      <th>Name:</td>
      <td colspan="3"><input type="text" name="cat_name" value="<cfoutput>#URLDecode(get_cat.cat_name)#</cfoutput>" size="70"></td>
    </tr>
    <tr>
      <th>SEO URL:</td>
      <td colspan="3"><input type="text" name="SEO_url" value="<cfoutput>#URLDecode(get_cat.SEO_url)#</cfoutput>" size="70"></td>
    </tr>
    <tr>
      <th>Page Meta Title:</td>
      <td colspan="3"><input type="text" name="meta_title" value="<cfoutput>#URLDecode(get_cat.meta_title)#</cfoutput>" size="70"></td>
    </tr>
    <tr>
      <th>Page Meta Description:</td>
      <td colspan="3"><textarea type="text" name="meta_description" id="meta_description" cols="70" rows="3"><cfoutput>#URLDecode(get_cat.meta_description)#</cfoutput></textarea></td>
    </tr>
    <tr>
      <th>Category Description:</td>
      <td colspan="3"><textarea type="text" name="cat_description" id="cat_description" cols="70" rows="3"><cfoutput>#URLDecode(get_cat.cat_description)#</cfoutput></textarea></td>
    </tr>
    <tr>
      <th>Cat Image:</td>
      <td colspan="3">

        <cfif isDefined("get_cat.cat_img") AND #get_cat.cat_img# NEQ "">
          
            <img src="../category_images/#get_cat.cat_img#" title="#URLDecode(img_title)#">

          <cfelse>

            No Image

        </cfif>

      </td>
    </tr>



    <tr>
      <th>Upload New Cat Image:</td>
      <td><input name="cat_img" type="file"/> (.jpgs only)</td>
    </tr>

    <tr>
      <th>Image title:</td>
      <td colspan="3"><input type="text" name="img_title" value="<cfoutput>#URLDecode(get_cat.img_title)#</cfoutput>" size="70"></td>
    </tr>
    <tr>
      <td colspan="4"><input type="submit" class="btn btn-primary btn-block" value="Update Category"></td>
    </tr>

  <input type="hidden" name="ID" value="<cfoutput>#get_cat.ID#</cfoutput>">
  <input type="hidden" name="main_var_update" value="form1">
</form>
		
    
  <tr>
    <td colspan="4" class="full_line"></td>
  </tr>   


<cfquery datasource="#application.DSN#" name="meta_tag_check">
  SELECT * 
  FROM meta_tags
  WHERE cat_id = #get_cat.id#
</cfquery>



  <tr>
    <td colspan="4"><h3>Page Meta Tags - <span class="badge badge-primary"><cfoutput>#meta_tag_check.recordcount#</cfoutput></span></h3></td>
  </tr>   

<tr>
<cfoutput><form method="post" name="form1" action="category_edit.cfm?id=#URL.id#" enctype="multipart/form-data">
  <td><input type="text" placeholder="New Meta Tag..." name="new_meta_tag" id="new_meta_tag"></td>
  <td><input type="submit" class="btn btn-primary btn-block" value="Add New Tag"></td>
  <input type="hidden" name="ID" value="#get_cat.id#">
  <input type="hidden" name="meta_tag_update" value="form1">
  </form></cfoutput>
</tr>
<!--- 
</table>
  <table cellpadding="4" cellspacing="4" class="edit_table" style="width:100%;">
 --->

   <tr>
    <td colspan="4" class="full_line"></td>
  </tr>   

    <tr>
      <cfset count = 0>
      <cfoutput query="meta_tag_check">

      <cfif count eq 4>
        </tr>
        <tr>
      <cfset count = 0>
      </cfif>
      <cfset count = #count# + 1>

        <td><a>#urlDecode(name)#</a> <a href="meta_tag_delete.cfm?id=#id#" title="delete '#urlDecode(name)#' Meta Tag"><span class="glyphicon glyphicon-remove"></span></a></td>
      
      </cfoutput>

<cfif isDefined("meta_tag_check.recordcount") AND meta_tag_check.recordcount lt 1>
  <td colspan="2"><h3><span class="label label-danger">No Assigned Tags!</span></h3></td>
</cfif>

    </tr>

  <tr>
    <td colspan="4" class="full_line"></td>
  </tr>

    <tr>
    <td colspan="2"><a href="tag_assign.cfm?id=<cfoutput>#URL.id#</cfoutput>"><h3>Update Tags</h3></a></td>
  </tr>
</table>
<div class="full_line"></div>
<div class="gofloat">

</div>
	</div>
</div>
<div class="clearline"></div>
<!--- end container div --->


</body>
</html>
