<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/Templates/HPIL_admin.dwt.cfm" codeOutsideHTMLIsLocked="false" -->
<head>
<cfif NOT IsDefined ("session.admininitials") OR session.admininitials eq 999>
	<cflocation url="index.cfm?passwordcheck=failed" addtoken="no">
</cfif>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<title>Website admin</title>

<link href="../css/HPIL_admin.css" rel="stylesheet" type="text/css" media="all">


<cfquery name="subscribers" datasource="#application.dsn#">
SELECT ID, memail, madded, eunsubscribed
FROM mailing_list
ORDER BY memail ASC
</cfquery>
<script src="../javascripts/engine.js"></script>
<script src="../javascripts/prototype.js"></script>
<script src="../javascripts/scriptaculous.js"></script>
<script type="text/JavaScript">
<!--
function updateSub(id){
	param = new Object();
	param.id = id
	http( 'POST'  , 'aj_subscribers.cfm' , subscribe_response, param ); 
}
function subscribe_response(){
}
//-->
</script>

</head>

<body>
<div id="container">
	<div id="logo"><img src="../images/HPIL_logo_gif.gif" /></div>
 	<div class="clearline"></div>
	<div id="nav">
			<cfinclude template="nav_admin.cfm">	
	</div>
	<div id="pagecontent">
	  	<div id="pagetitle">
	  	
<h2>Mailing list subscribers</h2>
		
	  	</div>
<div class="clearline"></div>
		

<table width="600">
	<tr>
		<td>Email</td>
		<td>Subscribed ?</td>
		<td>Date subscribed</td>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td colspan="4">&nbsp;</td>
	</tr>
<cfoutput query="subscribers">
	<tr>
		<td>#memail#</td>
		<td><input name="subscribed" type="checkbox" onclick="updateSub(#ID#)" value="1" <cfif subscribers.eunsubscribed eq 0>checked</cfif> /></td>
		<td>#dateformat(madded, 'dd-mm-yyyy')#</td>
		<td class="editlink"><a href="subscriber_delete.cfm?ID=#ID#">delete</a></td>
	</tr>
</cfoutput>
</table>
    
	</div>
</div>
<div class="clearline"></div>
<!--- end container div --->
</body>
</html>
