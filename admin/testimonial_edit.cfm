<cfset CurrentPage=GetFileFromPath(GetTemplatePath())>
<cfif IsDefined("FORM.MM_InsertRecord") AND FORM.MM_InsertRecord EQ "form1">
  <cfquery datasource="#application.dsn#">
    UPDATE testimonials
  	SET tsignature =
  <cfif IsDefined("FORM.tsignature") AND #FORM.tsignature# NEQ "">
    '#URLEncodedformat(FORM.tsignature)#'
      <cfelse>
    NULL
  </cfif>
   ,
   ttext =
  <cfif IsDefined("FORM.ttext") AND #FORM.ttext# NEQ "">
    '#URLEncodedformat(PreserveSingleQuotes(FORM.ttext))#'
      <cfelse>
    NULL
  </cfif>
     ,
	 torder =
  <cfif IsDefined("FORM.torder") AND #FORM.torder# NEQ "">
    #FORM.torder#
      <cfelse>
    20
  </cfif>
     ,
	 tproduct =
  <cfif IsDefined("FORM.tproduct") AND #FORM.tproduct# NEQ "">
    #FORM.tproduct#
      <cfelse>
    null
  </cfif>
    WHERE ID = #FORM.ID#
  </cfquery>
  <cflocation url="testimonials.cfm" addtoken="no">
</cfif>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/Templates/HPIL_admin.dwt.cfm" codeOutsideHTMLIsLocked="false" -->
<head>
<cfif NOT IsDefined ("session.admininitials") OR session.admininitials eq 999>
	<cflocation url="index.cfm?passwordcheck=failed" addtoken="no">
</cfif>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<title>Website admin</title>

<link href="../css/HPIL_admin.css" rel="stylesheet" type="text/css" media="all">


<cfquery name="getcomment" datasource="#application.dsn#">
SELECT ID, tsignature, ttext, torder, tproduct
FROM testimonials
WHERE ID = #URL.ID#
</cfquery>

</head>

<body>
<div id="container">
	<div id="logo"><img src="../images/HPIL_logo_gif.gif" /></div>
 	<div class="clearline"></div>
	<div id="nav">
			<cfinclude template="nav_admin.cfm">	
	</div>
	<div id="pagecontent">
	  	<div id="pagetitle">
	  	
		<h2>Edit Customer Comment</h2>	  
		
	  	</div>
<div class="clearline"></div>
		

    <cfform method="post" name="form1" action="#CurrentPage#" enctype="multipart/form-data">
      <table>
        <tr>
          <td class="formlabel">Signature:</td>
          <td><textarea name="tsignature" cols="50" rows="3"><cfoutput>#URLDecode(getcomment.tsignature)#</cfoutput></textarea></td>
        </tr>
        <tr>
          <td class="formlabel">Display order:</td>
          <td><cfinput type="text" name="torder" value="#getcomment.torder#" size="6"></td>
        </tr>
        <tr>
          <td class="formlabel">Text:</td>
          <td>
<textarea name="ttext" richtext="true" toolbar="pql" width="600" height="400" fontsizes="1/1,2/2,3/3,4/4,5/5,6/6,7/7">
<cfoutput>#URLDecode(getcomment.ttext)#</cfoutput>
</textarea>
		</td>
        </tr>
        <tr>
          <td class="formlabel">Product ID:</td>
          <td><cfinput type="text" name="tproduct" value="#getcomment.tproduct#" size="4"> ID of product this relates to (optional)</td>
        </tr>
        <tr>
          <td>&nbsp;</td>
          <td><input type="submit" value="Update Comment"></td>
        </tr>
      </table>
      <cfinput type="hidden" name="ID" value="#URL.ID#">
      <cfinput type="hidden" name="MM_InsertRecord" value="form1">
    </cfform>
    
	</div>
</div>
<div class="clearline"></div>
<!--- end container div --->
</body>
</html>
