<cfset CurrentPage=GetFileFromPath(GetTemplatePath())>
<cfif IsDefined("FORM.MM_InsertRecord") AND FORM.MM_InsertRecord EQ "form1">
  <cfquery datasource="#application.dsn#">
    INSERT INTO price_increases (price_percent, customer_group) VALUES (
  <cfif IsDefined("FORM.price_percent") AND FORM.price_percent NEQ "">
    #FORM.price_percent#
      <cfelse>
    0
  </cfif>
,
  <cfif IsDefined("FORM.customer_group") AND FORM.customer_group NEQ "">
    #FORM.customer_group#
      <cfelse>
    0
  </cfif>
    )
  </cfquery>
	<cfif FORM.customer_group eq 1>
			<!--- update retail prices --->
            <cfquery datasource="#application.dsn#">
            UPDATE products SET price = price * (1 + #FORM.price_percent#/100)
            </cfquery>
            <cfquery datasource="#application.dsn#">
            UPDATE child_products SET child_price = child_price * (1 + #FORM.price_percent#/100)
            </cfquery>
    <cfelseif FORM.customer_group eq 2>
			<!--- update default authority prices --->
            <cfquery datasource="#application.dsn#">
            UPDATE product_price_breaks SET bprice = bprice * (1 + #FORM.price_percent#/100)
            WHERE client_id IS NULL
            </cfquery>
	</cfif>

</cfif>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/Templates/HPIL_admin.dwt.cfm" codeOutsideHTMLIsLocked="false" -->
<head>
<cfif NOT IsDefined ("session.admininitials") OR session.admininitials eq 999>
	<cflocation url="index.cfm?passwordcheck=failed" addtoken="no">
</cfif>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<title>Website admin</title>

<link href="../css/HPIL_admin.css" rel="stylesheet" type="text/css" media="all">


<cfquery name="allincreases" datasource="#application.dsn#">
SELECT ID, price_percent, price_date, customer_group
FROM price_increases
ORDER BY price_date DESC
</cfquery>

</head>

<body>
<div id="container">
	<div id="logo"><img src="../images/HPIL_logo_gif.gif" /></div>
 	<div class="clearline"></div>
	<div id="nav">
			<cfinclude template="nav_admin.cfm">	
	</div>
	<div id="pagecontent">
	  	<div id="pagetitle">
	  	
<h2>Price Increases</h2>
		
	  	</div>
<div class="clearline"></div>
		

    <div class="gofloat">
    	<p>Use this form to increase either ALL retail prices or DEFAULT Local Authority prices by a specified percentage.</p>
        <p>The price increase takes immediate effect.</p>
    </div>
    
    <cfform method="post" name="form1" action="#CurrentPage#" enctype="multipart/form-data">
      <table cellspacing="8">
         <tr>
          <td class="formlabel">Customer Type:</td>
          <td>
            <table width="240">
              <tr>
                <td><label>
                  <input name="customer_group" type="radio" id="customer_group_0" value="1" checked="checked" />
                  Retail</label>
                </td>
                <td><label>
                  <input type="radio" name="customer_group" value="2" id="customer_group_1" />
                  L.A.</label>
                </td>
              </tr>
           </table></td>
        </tr>
       <tr>
          <td class="formlabel">Percent Increase:</td>
          <td><cfinput type="text" name="price_percent" value="" size="4"></td>
        </tr>
        <tr>
          <td>&nbsp;</td>
          <td><input type="submit" value="Apply Increase"></td>
        </tr>
      </table>
      <cfinput type="hidden" name="MM_InsertRecord" value="form1">
    </cfform>
<div class="blueline"></div>
<h2>Price Increase History</h2>
<table cellspacing="8">
	<tr>
		<td class="label">Date</td>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
		<td class="label">%</td>
	</tr>
<cfoutput query="allincreases">
	<tr>
		<td>#dateformat(price_date, 'dd-mm-yyyy')#</td>
		<td>&nbsp;</td>
		<td>
        <cfif IsDefined("allincreases.customer_group") AND allincreases.customer_group eq 1>
        	Retail
        <cfelseif IsDefined("allincreases.customer_group") AND allincreases.customer_group eq 2>
        	L.A.
        </cfif>
        </td>
		<td>&nbsp;</td>
		<td>#numberformat(price_percent, '.00')#</td>
	</tr>
</cfoutput>
</table>
    
	</div>
</div>
<div class="clearline"></div>
<!--- end container div --->
</body>
</html>
