<cfsetting requesttimeout="3600">

<cfset path = expandPath("../empty_me/")>
<cfset CurrentPage=GetFileFromPath(GetTemplatePath())>
<cfif IsDefined("FORM.MM_UpdateRecord") AND FORM.MM_UpdateRecord EQ "form1">
  <cfquery datasource="#application.dsn#">
    UPDATE categories SET cat_name=
  <cfif IsDefined("FORM.cat_name") AND #FORM.cat_name# NEQ "">
    '#FORM.cat_name#'
      <cfelse>
    NULL
  </cfif>
    , parentid=
  <cfif IsDefined("FORM.parentid") AND #FORM.parentid# NEQ "">
    #FORM.parentid#
      <cfelse>
    NULL
  </cfif>
    , display_order=
  <cfif IsDefined("FORM.display_order") AND #FORM.display_order# NEQ "">
    #FORM.display_order#
      <cfelse>
    99
  </cfif>
    , cat_description=
  <cfif IsDefined("FORM.cat_description") AND #FORM.cat_description# NEQ "">
    '#FORM.cat_description#'
      <cfelse>
    NULL
  </cfif>
     , gallery=
  <cfif IsDefined("FORM.gallery") AND #FORM.gallery# EQ 1>
    1
      <cfelse>
    0
  </cfif>
   WHERE id=#FORM.id#
  </cfquery>
 <!--- Upload image and resize, make a thumbnail too --->
  
  	<cfif IsDefined("FORM.newImage") AND #FORM.newImage# NEQ "">
		
		<cffile action="upload" 
		filefield="newImage" 
		destination="#path##FORM.id#.jpg" 
		nameconflict="overwrite">
		
		<cfimage source="#path##FORM.id#.jpg" name="lpic">		
<!--- make big image --->
<!---		<cfset ImageScaleToFit(lpic,800,600)>

<cfimage
    action = "write"
    destination = "../category_images/#getid.thisid#.jpg"
    source = "#lpic#"
    overwrite = "yes">
--->		
<!--- make thumbnail --->
		<cfset ImageScaleToFit(lpic,150,150)>

<cfimage
    action = "write"
    destination = "../category_images/#FORM.id#.jpg"
    source = "#lpic#"
    overwrite = "yes">

	</cfif>
  <cflocation url="categories.cfm" addtoken="no">
</cfif>
<cfquery name="thiscat" datasource="#application.dsn#">
SELECT ID, cat_name, parentid, cat_description, display_order, gallery
FROM categories
WHERE id = #URL.id#
</cfquery>

<cfquery name="allcats" datasource="#application.dsn#">
SELECT id, cat_name, parentid
FROM categories
ORDER BY cat_name ASC
</cfquery>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/templates/HPIL_admin.dwt.cfm" codeOutsideHTMLIsLocked="false" -->
<head>
<cfif NOT IsDefined ("session.admininitials") OR session.admininitials eq 999>
	<cflocation url="index.cfm?passwordcheck=failed" addtoken="no">
</cfif>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<title>Website Admin</title>
<link href="../css/HPIL_admin.css" rel="stylesheet" type="text/css" media="all">




<!-- Place inside the <head> of your HTML -->
<script type="text/javascript" src="../js/tinymce/tinymce.min.js"></script>
<script type="text/javascript">
tinymce.init({
    selector: "textarea"
 });
</script>


</head>

<body>
<div id="container">
	<div id="logo"><img src="../images/HPIL_logo_gif.gif" /></div>
 	<div class="clearline"></div>
	<div id="nav">
			<cfinclude template="nav_admin.cfm">	
	</div>
	<div id="pagecontent">
	  	<div id="pagetitle">
	  	
		<h1>Edit <cfoutput>#thiscat.cat_name#</cfoutput> Category</h1>
  
	  	</div>
<div class="clearline"></div>
		

<cfset image_path = ExpandPath("../category_images/#thiscat.id#.jpg")>
<form method="post" name="form1" action="#CurrentPage#" enctype="multipart/form-data">
  <table>
    <tr>
      <td class="formlabel">Name:</td>
      <td><input type="text" name="cat_name" value="<cfoutput>#thiscat.cat_name#</cfoutput>" size="32"></td>
    </tr>
    <tr>
      <td class="formlabel">SEO URL:</td>
      <td><input type="text" name="SEO_url" value="<cfoutput>#thiscat.SEO_url#</cfoutput>" size="32"></td>
    </tr>
    <tr>
      <td class="formlabel">Meta title:</td>
      <td><input type="text" name="meta_title" value="<cfoutput>#thiscat.meta_title#</cfoutput>" size="32"></td>
    </tr>    
    <tr>
      <td class="formlabel">Meta Description:</td>
      <td><input type="text" name="meta_description" value="<cfoutput>#thiscat.meta_description#</cfoutput>" size="32"></td>
    </tr>    

              <tr>
                <td class="formlabel">Parent category :</td>
                <td class="longtext"><select name="parentid">
                    <cfoutput query="allcats">
                      <option value="#allcats.id#" <cfif allcats.id eq thiscat.parentid>selected</cfif>>
					  #allcats.cat_name#
					  </option>
                    </cfoutput>
                  </select>
                </td>
              </tr>
     <tr>
      <td class="formlabel">Display order:</td>
      <td><input type="text" name="display_order" value="<cfoutput>#thiscat.display_order#</cfoutput>" size="3"></td>
    </tr>
   <tr>
      <td class="formlabel">Description:</td>
      <td>
	  <textarea name="cat_description" richtext="true" toolbar="pql" width="600" height="500" fontsizes="1/1,2/2,3/3,4/4,5/5,6/6,7/7">
	  	<cfoutput>#thiscat.cat_description#</cfoutput>
	  </textarea>
      </td>
    </tr>
	<cfif FileExists(image_path)>
    <tr>
       <td colspan="2"><img src="../category_images/<cfoutput>#thiscat.id#</cfoutput>.jpg" width="150" border="0" /></td>
    </tr>
	</cfif>
    <tr>
       <td class="formlabel">Upload Photo:</td>
        <td><input name="newImage" type="file" /></td>
    </tr>
    <tr>
       <td class="formlabel">Show gallery:</td>
        <td><input name="gallery" type="checkbox" value="1" <cfif IsDefined("thiscat.gallery") AND thiscat.gallery eq 1>checked</cfif> />&nbsp;&nbsp;Displays gallery link if checked</td>
    </tr>
    <tr>
      <td nowrap align="right">&nbsp;</td>
      <td><input type="submit" value="Update category"></td>
    </tr>
  </table>
  <input type="hidden" name="id" value="<cfoutput>#thiscat.id#</cfoutput>">
  <input type="hidden" name="MM_UpdateRecord" value="form1">
</form>

	</div>
</div>
<div class="clearline"></div>
<!--- end container div --->
</body>
</html>