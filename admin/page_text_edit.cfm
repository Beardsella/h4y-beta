<cfset CurrentPage=GetFileFromPath(GetTemplatePath())>
<cfif IsDefined("FORM.MM_InsertRecord") AND FORM.MM_InsertRecord EQ "form1">
  <cfquery datasource="#application.dsn#"
>
    UPDATE page_text SET page_content =
  <cfif IsDefined("FORM.content") AND #FORM.content# NEQ "">
    '#URLEncodedformat(PreserveSingleQuotes(FORM.content))#'
      <cfelse>
    NULL
  </cfif>
WHERE id = #FORM.id#
	</cfquery>
</cfif>

<cfif IsDefined("URL.id") AND URL.id neq ''>
	<cfquery name="gettext" datasource="#application.dsn#">
	SELECT page_name, page_content
	FROM page_text
	WHERE id = #URL.id#
	</cfquery>
</cfif>

<cfquery name="getpages" datasource="#application.dsn#">
SELECT id, page_name
FROM page_text
ORDER BY ID ASC
</cfquery>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/templates/HPIL_admin.dwt.cfm" codeOutsideHTMLIsLocked="false" -->
<head>
<cfif NOT IsDefined ("session.admininitials") OR session.admininitials eq 999>
	<cflocation url="index.cfm?passwordcheck=failed" addtoken="no">
</cfif>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<title>Website Admin</title>

<link href="../css/HPIL_admin.css" rel="stylesheet" type="text/css" media="all">


<script type="text/JavaScript">
<!--
function MM_jumpMenu(targ,selObj,restore){ //v3.0
  eval(targ+".location='"+selObj.options[selObj.selectedIndex].value+"'");
  if (restore) selObj.selectedIndex=0;
}
//-->
</script>

</head>

<body>
<div id="container">
	<div id="logo"><img src="../images/HPIL_logo_gif.gif" /></div>
 	<div class="clearline"></div>
	<div id="nav">
			<cfinclude template="nav_admin.cfm">	
	</div>
	<div id="pagecontent">
	  	<div id="pagetitle">
	  	Edit page text
	  	</div>
<div class="clearline"></div>
		

<form name="form1" id="form2">
  <select name="menu1" onchange="MM_jumpMenu('parent',this,0)">
 <cfoutput query="getpages">
    <option value="#CurrentPage#?id=#id#" <cfif IsDefined("URL.id") AND URL.id eq getpages.id>selected="selected"</cfif>>#page_name#</option>
</cfoutput>
  </select>
</form>
<cfif IsDefined("URL.id") AND URL.id neq ''>
    <table width="96%" border="0">
      <tr>
        <td>Use this form to edit the text on the <cfoutput>#gettext.page_name#</cfoutput> page.</td>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td><cfform action="#CurrentPage#" enctype="multipart/form-data">
            <table width="100%">
              <tr>
                <td colspan="2" class="longtext">
	<textarea name="content" richtext="true" toolbar="pql" width="700" height="700" fontsizes="1/1,2/2,3/3,4/4,5/5,6/6,7/7"><cfoutput>#URLDecode(gettext.page_content)#</cfoutput></textarea>                </td>
              </tr>
              <tr>
                <td>&nbsp;</td>
                <td><input type="submit" value="Update text" /></td>
              </tr>
            </table>
            <input type="hidden" name="MM_InsertRecord" value="form1" />
            <input type="hidden" name="ID" value="<cfoutput>#URL.id#</cfoutput>" />
          </cfform>        </td>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
      </tr>
    </table>
</cfif>
  
	</div>
</div>
<div class="clearline"></div>
<!--- end container div --->
</body>
</html>
