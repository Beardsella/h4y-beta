<cfif NOT IsDefined ("session.admininitials")>
	<cflocation url="index.cfm?passwordcheck=failed">
</cfif>
<cfset CurrentPage=GetFileFromPath(GetTemplatePath())>

<cfif IsDefined("FORM.MM_UpdateRecord") AND FORM.MM_UpdateRecord EQ "form1">
	<cfloop index="OneProduct" list="#Form.products#">
	
		<cfloop index="ListTags" list="#Form.tags#">
			<cfquery datasource="#application.dsn#">
			INSERT INTO product_tags (tag_id, product_id) VALUES(
 	 	<cfif IsDefined("ListTags") AND #ListTags# NEQ "">
    		#ListTags#
     	<cfelse>
   			 NULL
  		</cfif>
		,
 	 	<cfif IsDefined("OneProduct") AND #OneProduct# NEQ "">
    		#OneProduct#
     	<cfelse>
   			 NULL
  		</cfif>
 )
			</cfquery>
		</cfloop>
	</cfloop>
</cfif>

<cfquery name="alltags" datasource="#application.dsn#">
SELECT ID, tag FROM tags ORDER BY tag ASC
</cfquery>
<cfquery name="allprods" datasource="#application.dsn#">
SELECT id, title
FROM products
ORDER BY title ASC
</cfquery>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"><html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/templates/HPIL_admin.dwt.cfm" codeOutsideHTMLIsLocked="false" -->
<head>
<cfif NOT IsDefined ("session.admininitials") OR session.admininitials eq 999>
	<cflocation url="index.cfm?passwordcheck=failed" addtoken="no">
</cfif>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<title>Website Admin</title>

<link href="../css/HPIL_admin.css" rel="stylesheet" type="text/css" media="all">


</head>

<body>
<div id="container">
	<div id="logo"><img src="../images/HPIL_logo_gif.gif" /></div>
 	<div class="clearline"></div>
	<div id="nav">
			<cfinclude template="nav_admin.cfm">	
	</div>
	<div id="pagecontent">
	  	<div id="pagetitle">
	  	
Assign tags to products
		
	  	</div>
<div class="clearline"></div>
		

<form action="<cfoutput>#CurrentPage#</cfoutput>" method="post" enctype="multipart/form-data" name="form1" id="form1">
     <table width="100%" border="0">
          <tr>
            <td width="20%" valign="top">
			<select name="tags" size="20" onclick="getProds(this.value)">
			<cfoutput query="alltags">
              <option value="#alltags.ID#">#alltags.tag#</option>
            </cfoutput>
            </select>
			</td>
			<td width="35%" valign="top">
			<p>PRODUCTS ASSIGNED TO THE HIGHLIGHTED TAG <span id="taggedc"></span></p>
			<span id="showtags"></span>
			</td>
            <td width="5%" valign="top">&nbsp;</td>
            <td width="40%" valign="top">
			<p>AVAILABLE PRODUCTS <span id="untaggedc"></span></p>
			<span id="freeprods">
			<select name="products" size="20" multiple="multiple">
			<cfloop query="allprods">
              <option value="<cfoutput>#allprods.id#</cfoutput>">
			  	<cfoutput>#URLDecode(allprods.title)#</cfoutput></option>
            </cfloop>
            </select>
			</span>
			</td>
          </tr>
		  <tr>
            <td colspan="2">
			<input name="submit" type="submit" value="Assign tags" />
			<input type="hidden" name="MM_UpdateRecord" value="form1" />			</td>
            <td>&nbsp;</td>
		    <td>&nbsp;</td>
		  </tr>
        </table>
</form>

  
	</div>
</div>
<div class="clearline"></div>
<!--- end container div --->
</body>
</html>