<cfif NOT IsDefined ("session.admininitials")>
<cflocation url="index.cfm?passwordcheck=failed">
</cfif>

<cfquery name="orderinfo" datasource="#application.dsn#">
SELECT A.orderdate, A.cartid, A.ordervalue, A.deliverycost, A.customername, B.*
FROM ordersummary A INNER JOIN customerinfo B ON A.customerid = B.custid
WHERE A.orderid = #url.id#
</cfquery>

<cfquery name="orderlines" datasource="#application.dsn#">
SELECT A.size, A.colour, A.quantity, A.price, A.product_option, A.child_product, B.title
FROM orders A INNER JOIN products B ON A.prodid = B.id
WHERE A.cartid = '#orderinfo.cartid#'
ORDER BY A.id ASC
</cfquery>

<!--- create the invoice pdf --->

<cfdocument format="PDF" filename="PDFs/HPIL_Invoice_010#URL.id#.pdf" overwrite="yes" pagetype="a4">
<style type="text/css">
@import url(../css/psc_admin_email.css);
</style>

<div id="invoice_content">
		<div id="logo">
			<img src="../images/logo.gif">
		</div>
        <div id="ft_address">
			<span class="largelabel">INVOICE <cfoutput>010#URL.id#</cfoutput></span><br />
        	<span class="longtext">Hearing Products International Ltd<br>
www.hear4you.com<br>
<br>
info@hear4you.com<br>
Tel: 0161 480 8003  <br>
VAT No : 606 4762 40
</span>
		</div>
<div id="address">
<p class="label">INVOICE ADDRESS</p>
<cfoutput>#URLDecode(orderinfo.customername)#<br>
<cfif IsDefined("orderinfo.companyname") AND orderinfo.companyname neq ''>
	#URLDecode(orderinfo.companyname)#<br>
</cfif>
#URLDecode(orderinfo.add1)#<br>
#URLDecode(orderinfo.add2)#<br>
#URLDecode(orderinfo.add3)#<br>
#orderinfo.county#<br>
#orderinfo.postcode#<br>
</cfoutput>
</div>

<div class="clearline"></div>
		<div id="order_header">
     <table border="0" cellpadding="1" cellspacing="0">
          <tr>
            <td class="formlabel">Order Number</td>
            <td class="longtext"><cfoutput>PSC010#URL.id#</cfoutput></td>
          </tr>
          <tr>
            <td class="formlabel">Order Date</td>
            <td class="longtext">
			  <cfoutput>#orderinfo.orderdate#</cfoutput>
			  </td>
          </tr>
          <tr>
            <td class="formlabel">Customer</td>
            <td class="longtext"><cfoutput>#URLDecode(orderinfo.customername)#</cfoutput></td>
          </tr>
       </table>
		</div>
<div class="clearline"></div>

		<div id ="order_lines">		
		<table width="90%" border="0" cellpadding="1" cellspacing="0">
      <tr height="20">
        <td width="51%" class="label">Description</td>
        <td width="13%" class="formlabel">Quantity</td>
        <td width="16%" class="formlabel">Unit Price (ex VAT)</td>
        <td width="20%" class="formlabel">Total &pound;</td>
      </tr>
<cfset netgoods = 0>
<cfset vat = 0>
<cfloop query="orderlines">
  <cfset netgoods = netgoods + numberformat(price*quantity/(1+session.vatrate), '.00')>
  <cfset vat = vat + numberformat((price*quantity)-(price*quantity/(1+session.vatrate)), '.00')>
         <cfif IsDefined("orderlines.child_product") AND orderlines.child_product neq ''>
                 <cfquery name="kid" datasource="#application.dsn#">
                SELECT child_title FROM child_products WHERE id = #orderlines.child_product#
                </cfquery>
         </cfif>
        <tr valign="top" class="longtext" height="20">
          <td><cfoutput>#URLDecode(title)#</cfoutput>
				<cfif IsDefined("orderlines.product_option") AND orderlines.product_option neq ''>
                    <cfoutput>#orderlines.product_option#</cfoutput>
                </cfif>
                <cfif IsDefined("kid.child_title") AND kid.child_title neq ''>
                    <br /><cfoutput>#kid.child_title#</cfoutput>
                </cfif>
          </td>
          <td align="right"><cfoutput>#quantity#</cfoutput></td>
          <td align="right"><cfoutput>#LSCurrencyFormat(price/(1+session.vatrate),'none')#</cfoutput></td>
          <td align="right"><cfoutput>#LSCurrencyFormat(evaluate(price/(1+session.vatrate)*quantity),'none')#</cfoutput>
		  </td>
       </tr>
</cfloop>
  <cfset netdel = numberformat(orderinfo.deliverycost/(1+session.vatrate), '.00')>
  <cfset vatdel = numberformat(orderinfo.deliverycost-(orderinfo.deliverycost/(1+session.vatrate)), '.00')>

<tr height="20">
	<td>&nbsp;</td>
	<td>&nbsp;</td>
	<td align="right" class="formlabel">Total Goods</td>
	<td align="right" class="longtext"><cfoutput>#netgoods#</cfoutput></td>
</tr>
<tr>
	<td>&nbsp;</td>
	<td>&nbsp;</td>
	<td>&nbsp;</td>
	<td>&nbsp;</td>
</tr>
<tr height="20">
        <td class="longtext">&nbsp;</td>
        <td align="right">&nbsp;</td>
        <td align="right" class="formlabel">Post &amp; Packing</td>
        <td align="right" class="longtext">
		<cfoutput>#netdel#</cfoutput>		
		</td>
</tr>
     <tr height="20">
       <td class="longtext">&nbsp;</td>
       <td align="right">&nbsp;</td>
       <td align="right" class="formlabel">&nbsp;</td>
       <td align="right" class="longtext">&nbsp;</td>
     </tr>
<tr height="20">
        <td class="longtext">&nbsp;</td>
        <td align="right">&nbsp;</td>
        <td align="right" class="formlabel">VAT</td>
        <td align="right" class="longtext">
		<cfoutput>#vat+vatdel#</cfoutput>		
		</td>
</tr>
     <tr height="20">
       <td class="longtext">&nbsp;</td>
       <td align="right">&nbsp;</td>
       <td align="right" class="formlabel">&nbsp;</td>
       <td align="right" class="longtext">&nbsp;</td>
     </tr>
     <tr height="20">
       <td class="longtext">&nbsp;</td>
       <td align="right">&nbsp;</td>
       <td align="right" class="formlabel">Order Total </td>
       <td align="right" class="longtext">
	   <cfoutput>#numberformat(orderinfo.ordervalue+orderinfo.deliverycost, '.00')#</cfoutput>
       </td>
     </tr>
      <tr>
        <td class="longtext">&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
      </tr>
    </table>
		</div>
<div class="clearline"></div>
		<div id="deladdress">
	<table width="90%" cellspacing="1" cellpadding="1">
	  <cfoutput>
      <cfif orderinfo.deladd1 neq "">
	  <tr>
        <td width="30%" class="formlabel">Deliver To:</td>
        <td class="longtext">#orderinfo.delname#</td>
      </tr>
       <tr>
        <td>&nbsp;</td>
        <td class="longtext">#orderinfo.delcompany#</td>
      </tr>
     <tr>
        <td>&nbsp;</td>
        <td class="longtext">#orderinfo.deladd1#</td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td class="longtext">#orderinfo.deladd2#</td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td class="longtext">#orderinfo.deladd3#</td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td class="longtext">#orderinfo.delcounty#</td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td class="longtext">#orderinfo.delpostcode#</td>
      </tr>
 <cfelse>
       <tr>
        <td width="30%" class="formlabel">Deliver To:</td>
        <td class="longtext">#URLDecode(orderinfo.customername)#</td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td class="longtext">#URLDecode(orderinfo.companyname)#</td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td class="longtext">#URLDecode(orderinfo.add1)#</td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td class="longtext">#URLDecode(orderinfo.add2)#</td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td class="longtext">#URLDecode(orderinfo.add3)#</td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td class="longtext"></td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td class="longtext">#orderinfo.county#</td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td class="longtext">#orderinfo.postcode#</td>
      </tr>
</cfif>
	  </cfoutput>
    </table>
		</div>
</div>

</cfdocument>
		
<!--- now attach and send email --->
<cfmail to="#URLDecode(orderinfo.email)#" bcc="steve@publicquest.co.uk" from="info@hear4you.com" subject="Hearing Products International Ltd invoice" type="html">
<style type="text/css">
<!--
p {
	font-size:0.9em;
	font-family:Arial, Helvetica, sans-serif;
}
-->
</style>
<p><img src="http://www.hear4you.com/images/logo.gif"></p>

<p>Thank you for your online order at www.hear4you.com.</p>

<p>Order Number HPIL_010#URL.id#</p>

<p>Your invoice is attached.</p>

<p>If you have any questions regarding this order, please contact Customer Services on +44(0)161 480 8003</p>

<cfmailparam file="PDFs/HPIL_Invoice_010#URL.id#.pdf" />

</cfmail>

<!--- and update the invoice date field --->
<cfquery datasource="#application.dsn#">
UPDATE ordersummary SET invoice_date = '#dateformat(now(), 'dd/mm/yy')#'
WHERE orderid = #URL.id#
</cfquery>

<span class="largelabel">PDF Invoice Sent to <cfoutput>#URLDecode(orderinfo.email)#</cfoutput>  <cfoutput>#dateformat(now(), 'dd/mm/yy')#</cfoutput></span>

