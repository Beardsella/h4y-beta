<cfset CurrentPage=GetFileFromPath(GetTemplatePath())>
<cfif IsDefined("FORM.MM_InsertRecord") AND FORM.MM_InsertRecord EQ "form1">
  <cfquery datasource="#application.dsn#">
    INSERT INTO testimonials (ttext, tsignature, torder, tproduct) VALUES (
  <cfif IsDefined("FORM.ttext") AND #FORM.ttext# NEQ "">
    '#URLEncodedformat(FORM.ttext)#'
      <cfelse>
    NULL
  </cfif>
     ,
  <cfif IsDefined("FORM.tsignature") AND #FORM.tsignature# NEQ "">
    '#URLEncodedformat(FORM.tsignature)#'
      <cfelse>
    NULL
  </cfif>
     ,
  <cfif IsDefined("FORM.torder") AND #FORM.torder# NEQ "">
    #FORM.torder#
      <cfelse>
    20
  </cfif>
      ,
  <cfif IsDefined("FORM.tproduct") AND #FORM.tproduct# NEQ "">
    #FORM.tproduct#
      <cfelse>
    null
  </cfif>
   )
  </cfquery>
</cfif>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/Templates/HPIL_admin.dwt.cfm" codeOutsideHTMLIsLocked="false" -->
<head>
<cfif NOT IsDefined ("session.admininitials") OR session.admininitials eq 999>
	<cflocation url="index.cfm?passwordcheck=failed" addtoken="no">
</cfif>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<title>Website admin</title>

<link href="../css/HPIL_admin.css" rel="stylesheet" type="text/css" media="all">


<cfquery name="allcomments" datasource="#application.dsn#">
SELECT ID, tsignature
FROM testimonials
ORDER BY torder ASC
</cfquery>

</head>

<body>
<div id="container">
	<div id="logo"><img src="../images/HPIL_logo_gif.gif" /></div>
 	<div class="clearline"></div>
	<div id="nav">
			<cfinclude template="nav_admin.cfm">	
	</div>
	<div id="pagecontent">
	  	<div id="pagetitle">
	  	
		<h2>Customer Comments</h2>	  
		
	  	</div>
<div class="clearline"></div>
		

    <cfform method="post" name="form1" action="#CurrentPage#" enctype="multipart/form-data">
      <table>
        <tr>
          <td class="formlabel">Signature:</td>
          <td><textarea name="tsignature" cols="50" rows="3"></textarea></td>
        </tr>
        <tr>
          <td class="formlabel">Display order:</td>
          <td><cfinput type="text" name="torder" value="20" size="6"></td>
        </tr>
        <tr>
          <td class="formlabel">Text:</td>
          <td>
<textarea name="ttext" richtext="true" toolbar="pql" width="600" height="400" fontsizes="1/1,2/2,3/3,4/4,5/5,6/6,7/7"></textarea>		  </td>
        </tr>
        <tr>
          <td class="formlabel">Product ID:</td>
          <td><cfinput type="text" name="tproduct" value="" size="4"> ID of product this relates to (optional)</td>
        </tr>
        <tr>
          <td>&nbsp;</td>
          <td><input type="submit" value="Add Testimonial"></td>
        </tr>
      </table>
      <cfinput type="hidden" name="MM_InsertRecord" value="form1">
    </cfform>
<div class="greyline"></div>
<h2>Existing Customer Comments</h2>
<table>
<cfoutput query="allcomments">
	<tr>
		<td>#URLDecode(tsignature)#</td>
		<td class="editlink"><a href="testimonial_edit.cfm?ID=#ID#">edit</a></td>
		<td class="editlink"><a href="testimonial_delete.cfm?ID=#ID#">delete</a></td>
	</tr>
</cfoutput>
</table>
    
	</div>
</div>
<div class="clearline"></div>
<!--- end container div --->
</body>
</html>
