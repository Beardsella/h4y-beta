<cfsetting requesttimeout="3600">

<cfset CurrentPage=GetFileFromPath(GetTemplatePath())>
<cfset path = expandPath("../empty_me/")>
<cfset final_path = expandPath("../product-images/")>

<!--- first a bit of housekeeping --->
<cfdirectory directory="#path#" action="list" name="currentDir">
  <cfloop query="currentDir">
<cftry>
  <cffile action="delete" file="#path##currentDir.Name#">
<cfcatch></cfcatch>
</cftry>
  </cfloop>




<cfif isDefined("FORM.meta_tag_update") AND FORM.meta_tag_update NEQ "">
  <cfquery datasource="#application.DSN#">
    INSERT INTO meta_tags (name, prod_id)
    VALUES ("#urlEncodedFormat(new_meta_tag)#", '#FORM.ID#')
  </cfquery>
</cfif>


<cfif IsDefined("FORM.main_var_update") AND FORM.main_var_update EQ "form1">


 <!--- Upload image and resize, make a thumbnail too --->
  
    <cfif IsDefined("FORM.newImage") AND #FORM.newImage# NEQ "">
    
      <cfset accept_upload = 1>
        
            <!--- only allow these mime types. Edit as required --->
            <cffile action="upload" filefield="newImage" destination="#GetTempDirectory()#" nameconflict="overwrite" result="uploadResult" accept="image/jpg, image/bmp, image/gif, image/jpeg">
            
            <!--- allow images only if needed --->
            <cfif Not IsImageFile(getTempDirectory() & uploadResult.serverFile)>
                <cffile action="delete" file="#getTempDirectory()##uploadResult.serverFile#">
                <cfset accept_upload = 0>
            </cfif>
            
            <!--- only allow these file extensions. Edit as required --->
            <cfif NOT ListFindNoCase("bmp,gif,jpg,jpeg,png", uploadResult.ServerFileExt)>
                <cffile action="delete" file="#getTempDirectory()##uploadResult.serverFile#">
            
                <cfset display_error = 'This file type cannot currently be uploaded. Please contact PQL.'>
                <cfset accept_upload = 0>
            
            </cfif>
            
            <cfif accept_upload eq 1>


<cfif IsDefined("FORM.new_thumb_description") AND FORM.new_thumb_description NEQ "">



  <cfset new_name_with_no_spaces = #REReplace('#URLDecode(FORM.new_thumb_description)#',"[^0-9A-Za-z &/]","","all")# >

  <cfset new_name_with_no_spaces = Replace('#new_name_with_no_spaces#', ' ', '-', 'All')>

  <cfset new_name_with_no_spaces = Replace('#new_name_with_no_spaces#', '/', '-', 'All')>

  <cfset new_name_with_no_spaces = Replace('#new_name_with_no_spaces#', '--', '', 'All')>

  <cfset new_name_with_no_spaces = Replace('#new_name_with_no_spaces#', '---', '', 'All')>

  <cfset new_name_with_no_spaces = Replace('#new_name_with_no_spaces#', '&', 'and', 'All')>

  <cfset new_name_with_no_spaces = "#new_name_with_no_spaces#" & ".jpg">

  <cfset new_name_with_no_spaces = Replace('#new_name_with_no_spaces#', '-.jpg', '.jpg', 'All')>

  <cfset new_name_with_no_spaces = "#LCase(new_name_with_no_spaces)#">

<cfelse>

  <cfset new_name_with_no_spaces = #REReplace('#URLDecode(uploadResult.serverFile)#',"[^0-9A-Za-z &/]","","all")# >

  <cfset new_name_with_no_spaces = Replace('#new_name_with_no_spaces#', ' ', '-', 'All')>

  <cfset new_name_with_no_spaces = Replace('#new_name_with_no_spaces#', '/', '-', 'All')>

  <cfset new_name_with_no_spaces = Replace('#new_name_with_no_spaces#', '--', '', 'All')>

  <cfset new_name_with_no_spaces = Replace('#new_name_with_no_spaces#', '---', '', 'All')>

  <cfset new_name_with_no_spaces = Replace('#new_name_with_no_spaces#', '&', 'and', 'All')>

  <cfset new_name_with_no_spaces = "#LCase(new_name_with_no_spaces)#">

</cfif>
              
            
                    <!--- IMAGES: move the file. #path# is normally already set in the page header---> 
                    <cfimage action="WRITE" source="#getTempDirectory()##uploadResult.serverFile#" destination="#path##new_name_with_no_spaces#" overwrite="yes">
                            
            </cfif>
                  
            <!--- delete the temp file in all cases --->
            <cffile action="delete" file="#getTempDirectory()##uploadResult.serverFile#">
    
    <cfimage source="#path##New_name_with_no_spaces#" name="lpic">    
    
    <!--- make big image --->
    <cfset ImageScaleToFit(lpic,1000,1000)>

<cfimage
    action = "write"
    destination = "../product-images/#New_name_with_no_spaces#"
    source = "#lpic#"
    overwrite = "yes">
    
<!--- make thumbnail --->
    <cfset ImageScaleToFit(lpic,150,150)>

<cfimage
    action = "write"
    destination = "../product-images/thumbs/#New_name_with_no_spaces#"
    source = "#lpic#"
    overwrite = "yes">

  <cfquery datasource="#application.dsn#">
    INSERT INTO product_images (product_id, image_name, image_caption) 
    VALUES (#FORM.id#, '#new_name_with_no_spaces#', '#form.new_thumb_description#')
  </cfquery>

</cfif>






<cfset formatted_seo_url = "">

<cfif isDefined("FORM.SEO_URL") AND FORM.SEO_URL NEQ "">

  <cfset formatted_seo_url = #REReplace('#URLDecode(FORM.SEO_URL)#',"[^0-9A-Za-z &/-]","","all")# >

  <cfset formatted_seo_url = Replace('#formatted_seo_url#', ' ', '-', 'All')>

  <cfset formatted_seo_url = Replace('#formatted_seo_url#', '/', '-', 'All')>

  <cfset formatted_seo_url = Replace('#formatted_seo_url#', '--', '', 'All')>

  <cfset formatted_seo_url = Replace('#formatted_seo_url#', '---', '', 'All')>

  <cfset formatted_seo_url = Replace('#formatted_seo_url#', '&', 'and', 'All')>

  <cfset formatted_seo_url = Replace('#formatted_seo_url#', 'cfm', '', 'All')>

  <cfset formatted_seo_url_suffix = Right('#formatted_seo_url#', 3)>

  <cfif formatted_seo_url_suffix neq "cfm">
    
    <cfset formatted_seo_url = "#formatted_seo_url#" & ".cfm">

  </cfif>

  <cfset formatted_seo_url_prefix = left('#formatted_seo_url#', 1)>

  <cfif formatted_seo_url_prefix eq "-">
    
    <cfset formatted_seo_url_length = #len(formatted_seo_url)#>
    <cfset formatted_seo_url_length = #formatted_seo_url_length# - 1>
    <cfset formatted_seo_url = right('#formatted_seo_url#', #formatted_seo_url_length#)>

  </cfif>

  <cfset formatted_seo_url = Replace('#formatted_seo_url#', '-.cfm', '.cfm', 'All')>

  <cfset formatted_seo_url = "#LCase(formatted_seo_url)#">




<cfquery datasource="#application.dsn#" name="seo_name_test">
  SELECT * 
  FROM products
  WHERE id = #URL.id#
</cfquery>

   <cfset current_seo_url = "#seo_name_test.seo_url#">

<cfif isDefined("seo_name_test.seo_url") AND #seo_name_test.seo_url# neq "#current_seo_url#">
  
<cfoutput query="seo_name_test">
<!---     <cffile action = "rename"
    destination="#seo_url#"
      source = "test.cfm"
      nameconflict="overwrite">
 ---></cfoutput>

</cfif>

</cfif>

  <cfquery datasource="#application.dsn#">
    UPDATE products SET title=
  <cfif IsDefined("FORM.title") AND #FORM.title# NEQ "">
    '#URLEncodedFormat(FORM.title)#'
      <cfelse>
    NULL
  </cfif>
	, supplier_code=
  <cfif IsDefined("FORM.supplier_code") AND #FORM.supplier_code# NEQ "">
    '#FORM.supplier_code#'
      <cfelse>
    NULL
  </cfif>
    , display_order=
  <cfif IsDefined("FORM.display_order") AND #FORM.display_order# NEQ "">
    #FORM.display_order#
      <cfelse>
    NULL
  </cfif>
    , Description=
  <cfif IsDefined("FORM.Description") AND #FORM.Description# NEQ "">
    '#URLEncodedFormat(FORM.Description)#'
      <cfelse>
    NULL
  </cfif>
    , thumb_description=
  <cfif IsDefined("FORM.thumb_description") AND #FORM.thumb_description# NEQ "">
    '#URLEncodedFormat(FORM.thumb_description)#'
      <cfelse>
    NULL
  </cfif>
    , SEO_URL=
  <cfif IsDefined("formatted_seo_url") AND #formatted_seo_url# NEQ "">
    '#formatted_seo_url#'
      <cfelse>
    NULL
  </cfif>
     , technical=
  <cfif IsDefined("FORM.technical") AND #FORM.technical# NEQ "">
    '#URLEncodedFormat(FORM.technical)#'
      <cfelse>
    NULL
  </cfif>
   , displaythis=
  <cfif IsDefined("FORM.displaythis")>
    1
      <cfelse>
    0
  </cfif>
   , in_stock=
  <cfif IsDefined("FORM.in_stock")>
    1
      <cfelse>
    0
  </cfif>
   , vatexempt=
  <cfif IsDefined("FORM.vatexempt")>
    1
      <cfelse>
    0
  </cfif>
   , featured=
  <cfif IsDefined("FORM.featured")>
    1
      <cfelse>
    0
  </cfif>
    , price=
  <cfif IsDefined("FORM.price") AND #FORM.price# NEQ "">
    #FORM.price#
      <cfelse>
    NULL
  </cfif>
    , price_was=
  <cfif IsDefined("FORM.price_was") AND #FORM.price_was# NEQ "">
    #FORM.price_was#
      <cfelse>
    NULL
  </cfif>
    , delivery=
  <cfif IsDefined("FORM.delivery") AND #FORM.delivery# NEQ "">
    #FORM.delivery#
      <cfelse>
    NULL
  </cfif>
    , product_options=
  <cfif IsDefined("FORM.product_options") AND #FORM.product_options# NEQ "">
    '#URLEncodedFormat(FORM.product_options)#'
      <cfelse>
    NULL
  </cfif>
  , prelated=
  <cfif IsDefined("FORM.prelated") AND #FORM.prelated# NEQ "">
    '#FORM.prelated#'
      <cfelse>
    NULL
  </cfif>

    WHERE ID=#FORM.ID#
  </cfquery>

<cfquery datasource="#application.dsn#" name="seo_name_test">
  SELECT * 
  FROM products
  WHERE id = #URL.id#
</cfquery>

<cfif isDefined("seo_name_test.seo_url") AND #seo_name_test.seo_url# neq "">
  
<cflocation url="products.cfm?winner" addtoken="no">

<!--- <cfoutput query="seo_name_test">
    <cffile action = "rename"
    destination="../../Hear4You-14/#seo_url#"
      source = "test.cfm"
      nameconflict="overwrite">
</cfoutput>
 --->
</cfif>

<cfset redirect = "true">
<!--- <meta http-equiv="refresh" content="8;URL='products.cfm'" target="_blank" /> 
 ---></cfif>

<cfquery name="getprod" datasource="#application.dsn#">
SELECT * FROM products WHERE ID = #URL.id#
</cfquery>
<!--- get any child products --->
<cfquery name="children" datasource="#application.dsn#">
SELECT id, child_title, child_price FROM child_products WHERE product_id = #URL.id#
</cfquery>

<!--- get the cats this product is in --->
<cfquery name="getcats" datasource="#application.dsn#">
SELECT A.id, B.cat_name, B.parentid
FROM product_cats A INNER JOIN categories B ON A.category_id = B.id
WHERE A.product_id = #URL.id#
ORDER BY A.id ASC
</cfquery>
<!--- get the downloads associated with this product --->
<cfquery name="getuploads" datasource="#application.dsn#">
SELECT ID, filename, filelabel
FROM uploads
WHERE product_id = #URL.id#
ORDER BY ID ASC
</cfquery>
<!--- get the price breaks for social service etc clients --->
<cfquery name="getbreaks" datasource="#application.dsn#">
SELECT ID, bprice, break_qty
FROM product_price_breaks
WHERE bproduct_id = #URL.ID#
ORDER BY break_qty ASC
</cfquery>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/templates/HPIL_admin.dwt.cfm" codeOutsideHTMLIsLocked="false" -->
<head>
<cfif NOT IsDefined ("session.admininitials") OR session.admininitials eq 999>
	<cflocation url="index.cfm?passwordcheck=failed" addtoken="no">
</cfif>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<title>Hearing Aids and Speech Amplification</title>


<link href="../css/HPIL_admin.css" rel="stylesheet" type="text/css" media="all">


<!-- Bootstrap CSS-->
 <link href="../css/bootstrap.css" rel="stylesheet">  



<!--- get existing images --->
<cfquery name="getpics" datasource="#application.dsn#">
SELECT id, image_name, image_caption FROM product_images WHERE product_id = #url.id#
</cfquery>



<!-- Place inside the <head> of your HTML -->
<script type="text/javascript" src="../js/tinymce/tinymce.min.js"></script>
<script type="text/javascript">
tinyMCE.init({
    mode : "textareas",
    editor_selector : "mytextarea"
});
</script>



</head>

<body>
<div id="container">
	<div id="logo"><img src="../images/HPIL_logo_gif.gif" /></div>
 	<div class="clearline"></div>
	<div id="nav">
			<cfinclude template="nav_admin.cfm">	
	</div>
	<div id="pagecontent">

<cfif isDefined("redirect") AND redirect EQ "true">
      <cfoutput><h3><a href="../#URLDecode(getprod.seo_url)#" target="_blank">#URLDecode(getprod.title)#</a> Updated!</h3></cfoutput>
<cfelseif isDefined("getprod.seo_url") AND #getprod.seo_url# NEQ "">
      <cfoutput><h3>Edit <a href="../#URLDecode(getprod.seo_url)#" target="_blank">#URLDecode(getprod.title)#</a></h3></cfoutput>
<cfelse>
      <cfoutput><h3>Edit <a href="../product_detail.cfm?id=#getprod.id#" target="_blank">#URLDecode(getprod.title)#</a></h3></cfoutput>
</cfif>

<div class="clearline"></div>

<cfoutput>
<form method="post" name="form1" action="edit_product.cfm?id=#URL.id#" enctype="multipart/form-data"></cfoutput>
  <table cellpadding="4" cellspacing="4" class="edit_table" style="width:100%;">
     <tr>
      <td colspan="4">&nbsp;</td>
    </tr>
   <tr>
      <th>Product Title:</td>
      <td colspan="3"><input type="text" name="title" value="<cfoutput>#URLDecode(getprod.title)#</cfoutput>" size="70"></td>
    </tr>
   <tr>
      <th>SEO URL:</td>
      <td colspan="3"><input type="text" name="seo_url" value="<cfoutput>#URLDecode(getprod.seo_url)#</cfoutput>" size="70"></td>
    </tr>
    <tr>
      <th>Page Meta Title:</td>
      <td colspan="3"><input type="text" name="meta_title" value="<cfoutput>#URLDecode(getprod.meta_title)#</cfoutput>" size="70"></td>
    </tr>
    <tr>
      <th>Page Meta Description:</td>
      <td colspan="3"><textarea type="text" name="meta_description" id="meta_description" cols="70" rows="3"><cfoutput>#URLDecode(getprod.meta_description)#</cfoutput></textarea></td>
    </tr>
    <tr>
      <th>Thumbnail Description:</td>
      <td colspan="3"><textarea type="text" name="thumb_description" id="thumb_description" cols="70" rows="3"><cfoutput>#URLDecode(getprod.thumb_description)#</cfoutput></textarea></td>
    </tr>
    <tr>
      <th>Price:</td>
      <td><input type="text" name="price" value="<cfoutput>#getprod.price#</cfoutput>" size="10"></td>
    </tr>
    <tr>
      <th>Old Price: </br>(optional)</td>
      <td><input type="text" name="price_was" value="<cfoutput>#getprod.price_was#</cfoutput>" size="10"></td>
    </tr>
    <tr>
      <th>UK Delivery:</td>
      <td><input type="text" name="delivery" value="<cfoutput>#getprod.delivery#</cfoutput>" size="10"></td>
    </tr>
   <tr>
      <th>In Stock:</td>
      <td><input type="checkbox" name="in_stock" value=""  <cfif (getprod.in_stock EQ 1)>checked</cfif>></td>
    </tr>
   <tr>
      <th>Display this:</td>
      <td><input type="checkbox" name="displaythis" value=""  <cfif (getprod.displaythis EQ 1)>checked</cfif>></td>
    </tr>
   <tr>
      <th>Vat exempt ?</td>
      <td>
      <input type="checkbox" name="vatexempt" value=""  <cfif (getprod.vatexempt EQ 1)>checked</cfif>>
      tick if this can be vat exempt
      </td>
    </tr>
   <tr>
      <th>Feature this:</td>
      <td><input type="checkbox" name="featured" value=""  <cfif (getprod.featured EQ 1)>checked</cfif>></td>
    </tr>
    <tr>
      <th>Display order:</td>
      <td><input type="text" name="display_order" value="<cfoutput>#getprod.display_order#</cfoutput>" size="6"></td>
    </tr>
     <tr>
      <th>Supplier code:</td>
      <td><input type="text" name="supplier_code" value="<cfoutput>#getprod.supplier_code#</cfoutput>" size="40"></td>
    </tr>
    <tr>

     <th>Related product IDs: <br>(Comma seperated List)</td>
      <td><textarea name="prelated" rows="2" cols="80" placeholder="1,2,3,4,5 ect...."><cfoutput>#getprod.prelated#</cfoutput></textarea></td>
    </tr>   
    <tr>
      <th>Product Options: <br>(Comma seperated List)</td>
      <td><textarea name="product_options" placeholder="Red, Blue, Green, ect...." rows="2" cols="80" ><cfoutput>#getprod.product_options#</cfoutput></textarea></td>
    </tr>
    <tr>
    <td  colspan="4" class="full_line"></td>
</tr>
    <tr>
      <th colspan="4" ><h4>Product Description:</h4></td>
    </tr>
    <tr>      <td colspan="4"><textarea name="Description" class="mytextarea" width="600" height="400"><cfoutput>#URLDecode(getprod.Description)#</cfoutput></textarea>
      </td>
    </tr>
    <tr>
    <td  colspan="4" class="full_line"></td>
    </tr>
    <tr>
      <th colspan="4"><h4>Technical spec:</h4></td>
    </tr>
    <tr>
      <td colspan="4"><textarea name="technical" class="mytextarea" width="500" height="400"><cfoutput>#URLDecode(getprod.technical)#</cfoutput></textarea>
      </td>
    </tr>
    <tr>
<cfquery datasource="#application.DSN#" name="prod_pictures">
    SELECT *
    FROM product_images
    WHERE product_id = #URL.id#
  </cfquery>

  <cfif isDefined("#prod_pictures.image_name#")>
    <cfoutput>

      <td colspan="2"><img src="../product-images/thumbs/#prod_pictures.image_name#" height="90" alt="Current Image" /><br><a href="../product-images/thumbs/#prod_pictures.image_name#" targert="_blank">../product-images/thumbs/#prod_pictures.image_name#</a></cfoutput></td>

  </cfif>

    </tr>

    <tr>
      <td colspan="4"><input type="submit" class="btn btn-primary btn-block" value="Update product"></td>
    </tr>

  <input type="hidden" name="ID" value="<cfoutput>#getprod.ID#</cfoutput>">
  <input type="hidden" name="main_var_update" value="form1">
</form>

    
  <tr>
    <td colspan="4" class="full_line"></td>
  </tr>   


<cfquery datasource="#application.DSN#" name="meta_tag_check">
  SELECT * 
  FROM meta_tags
  WHERE prod_id = #URL.id#
</cfquery>



  <tr>
    <td colspan="4"><h3>Page Meta Tags - <span class="badge badge-primary"><cfoutput>#meta_tag_check.recordcount#</cfoutput></span></h3></td>
  </tr>   

<tr>
<cfoutput><form method="post" name="form1" action="edit_product.cfm?id=#URL.id#" enctype="multipart/form-data">
  <td><input type="text" placeholder="New Meta Tag..." name="new_meta_tag" id="new_meta_tag"></td>
  <td><input type="submit" class="btn btn-primary btn-block" value="Add New Tag"></td>
  <input type="hidden" name="ID" value="#getprod.id#">
  <input type="hidden" name="meta_tag_update" value="form1">
  </form></cfoutput>
</tr>
<!--- 
</table>
  <table cellpadding="4" cellspacing="4" class="edit_table" style="width:100%;">
 --->

   <tr>
    <td colspan="4" class="full_line"></td>
  </tr>   

    <tr>
      <cfset count = 0>
      <cfoutput query="meta_tag_check">

      <cfif count eq 4>
        </tr>
        <tr>
      <cfset count = 0>
      </cfif>
      <cfset count = #count# + 1>

        <td><a>#urlDecode(name)#</a> <a href="meta_tag_delete.cfm?id=#id#" title="delete '#urlDecode(name)#' Meta Tag"><span class="glyphicon glyphicon-remove"></span></a></td>
      
      </cfoutput>

<cfif isDefined("meta_tag_check.recordcount") AND meta_tag_check.recordcount lt 1>
  <td colspan="2"><h3><span class="label label-danger">No Assigned Tags!</span></h3></td>
</cfif>

    </tr>

  <tr>
    <td colspan="4" class="full_line"></td>
  </tr>
    <tr>
    <td colspan="2"><a href="tag_assign.cfm?id=<cfoutput>#URL.id#</cfoutput>"><h3>Update Tags</h3></a></td>
  </tr>
  <tr>
    <td colspan="4" class="full_line"></td>
  </tr>
    <tr>
    <td colspan="2"><a href="categories_assign.cfm?id=<cfoutput>#URL.id#</cfoutput>"><h3>Update Categories</h3></a></td>
  </tr>
<cfoutput query="getcats">
  <tr>
    <td>#cat_name#</td>
    <td>&nbsp;</td>
  </tr>
</cfoutput>
	<tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
</table>
<div class="full_line"></div>
<table width="500" border="0">
  <tr>
    <td colspan="2"><a href="children_edit.cfm?id=<cfoutput>#URL.id#</cfoutput>"><h3>Update Child Products and set price breaks</h3></a></td>
  </tr>
<cfoutput query="children">
  <tr>
    <td>#child_title#</td>
    <td>#numberformat(child_price, '.00')#</td>
  </tr>
</cfoutput>
	<tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
</table>
<div class="full_line"></div>
<table width="500" border="0">
  <tr>
    <td colspan="2"><a href="uploads_edit.cfm?id=<cfoutput>#URL.id#</cfoutput>"><h3>Upload Documents / Files</h3></a></td>
  </tr>
<cfoutput query="getuploads">
  <tr>
    <td>#filename#</td>
    <td>#URLDecode(filelabel)#</td>
  </tr>
</cfoutput>
	<tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
</table>
<div class="full_line"></div>
<p><a href="images_additional.cfm?id=<cfoutput>#URL.id#</cfoutput>"><h3>Update Additional Images</h3></a></p>
    <cfloop query="getpics">
        <div class="gofloat_nowidth">
                    <img src="../product-images/thumbs/<cfoutput>#image_name#</cfoutput>">
                  <br /><cfoutput>#image_caption#</cfoutput>
        </div>
    </cfloop>
<div class="clearline"></div>
<cfif children.recordcount eq 0><!---price breaks for product no kids --->
        <div class="full_line"></div>
        <table>
          <tr>
            <td colspan="2"><a href="price_breaks.cfm?id=<cfoutput>#URL.id#</cfoutput>"><h3>Default Product Price Breaks</h3></a></td>
          </tr>
        <cfoutput query="getbreaks">
          <tr>
            <td>#break_qty#</td>
            <td>#numberformat(bprice, '.00')#</td>
          </tr>
        </cfoutput>
            <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
          </tr>
        </table>
</cfif>
<div class="full_line"></div>
<div class="gofloat">

</div>
	</div>
</div>
<div class="clearline"></div>
<!--- end container div --->


</body>
</html>
