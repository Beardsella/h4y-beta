<cfset CurrentPage=GetFileFromPath(GetTemplatePath())>
<cfif IsDefined("FORM.MM_InsertRecord") AND FORM.MM_InsertRecord EQ "form1">
  <cfquery datasource="#application.dsn#">
    INSERT INTO email_messages (esubject, econtent, esummary) VALUES (
  <cfif IsDefined("FORM.esubject") AND FORM.esubject NEQ "">
    '#FORM.esubject#'
      <cfelse>
    NULL
  </cfif>
    ,
  <cfif IsDefined("FORM.econtent") AND FORM.econtent NEQ "">
    '#URLEncodedFormat(Trim(FORM.econtent))#'
      <cfelse>
    NULL
  </cfif>
   ,
     <cfif IsDefined("FORM.esummary") AND FORM.esummary NEQ "">
    '#FORM.esummary#'
      <cfelse>
    NULL
  </cfif>
)
  </cfquery>
</cfif>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/Templates/HPIL_admin.dwt.cfm" codeOutsideHTMLIsLocked="false" -->
<head>
<cfif NOT IsDefined ("session.admininitials") OR session.admininitials eq 999>
	<cflocation url="index.cfm?passwordcheck=failed" addtoken="no">
</cfif>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<title>Web admin</title>

<link href="../css/HPIL_admin.css" rel="stylesheet" type="text/css" media="all">


<cfquery name="allemails" datasource="#application.dsn#">
SELECT ID, esubject, econtent, esummary
FROM email_messages
ORDER BY ID ASC
</cfquery>

</head>

<body>
<div id="container">
	<div id="logo"><img src="../images/HPIL_logo_gif.gif" /></div>
 	<div class="clearline"></div>
	<div id="nav">
			<cfinclude template="nav_admin.cfm">	
	</div>
	<div id="pagecontent">
	  	<div id="pagetitle">
	  	
		<h2>Email messages</h2>	  
	  
	  	</div>
<div class="clearline"></div>
		

<div class="gofloat">
    <cfform method="post" name="form1" enctype="multipart/form-data" action="#CurrentPage#">
      <table>
        <tr>
          <td class="formlabel">Subject line:</td>
          <td><input type="text" name="esubject" value="" size="40"></td>
        </tr>
        <tr>
          <td class="formlabel">Summary:</td>
          <td><input type="text" name="esummary" value="" size="40"></td>
        </tr>
        <tr>
          <td class="formlabel">Message:</td>
          <td><textarea name="econtent" richtext="true" toolbar="pql" width="660" height="700"></textarea></td>
        </tr>
        <tr>
          <td>&nbsp;</td>
          <td><input type="submit" value="Insert record"></td>
        </tr>
      </table>
      <input type="hidden" name="MM_InsertRecord" value="form1">
    </cfform>
</div>
<div class="forty" style="border-top:1px solid #CCCCCC">
<h2>Current Messages</h2>	  
<table>
<cfoutput query="allemails">
	<tr>
		<td><a href="email_message_edit.cfm?ID=#ID#" title="click to edit">#esummary#</a></td>
		<td><a href="email_message_delete.cfm?ID=#ID#">delete</a></td>
	</tr>
</cfoutput>
</table>
</div>
    
	</div>
</div>
<div class="clearline"></div>
<!--- end container div --->
</body>
</html>
