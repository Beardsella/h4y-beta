<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/Templates/HPIL_admin.dwt.cfm" codeOutsideHTMLIsLocked="false" -->
<head>
<cfif NOT IsDefined ("session.admininitials") OR session.admininitials eq 999>
	<cflocation url="index.cfm?passwordcheck=failed" addtoken="no">
</cfif>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<title>Website Admin</title>

<link href="../css/HPIL_admin.css" rel="stylesheet" type="text/css" media="all">



<cfquery datasource="#application.dsn#" name="csq">
	SELECT	*
    FROM questionare_cs
	WHERE id = #URL.id#
</cfquery>



</head>

<body>
<div id="container">
	<div id="logo"><img src="../images/HPIL_logo_gif.gif" /></div>
 	<div class="clearline"></div>
	<div id="nav">
			<cfinclude template="nav_admin.cfm">	
	</div>
	<div id="pagecontent">
	  	<div id="pagetitle">
	  	Customer satisfaction Questionnaire
	  	</div>
<div class="clearline"></div>
		

<cfoutput query="csq">

<table id="stats_table" cellspacing="4" cellpadding="4">
  <tr>
    <th colspan="6">&nbsp;</th>
  </tr>
  <tr>
    <th rowspan="4">Customer Contact</th>
    <th>Name:</td>
    <td>#URLDecode(cust_name)#</td>
    <th rowspan="3">Hear 4 you Contact</th>
    <th>Name:</td>
    <td>#URLDecode(h4y_name)#</td>
  </tr>
  <tr>
    <th>Tel:</th>
    <td>#URLDecode(cust_tel)#</td>
    <th>Tel:</th>
    <td>#URLDecode(h4y_tel)#</td>
  </tr>
  <tr>
    <th>Email:</th>
    <td><a href="mailto:#URLDecode(cust_email)#">#URLDecode(cust_email)#</a></td>
    <th>Email:</th>
    <td>#URLDecode(h4y_email)#</td>
  </tr>
  <tr>
    <th>Company:</th>
    <td>#URLDecode(cust_company)#</td>
    <td colspan="3">&nbsp;</td>
  </tr>
</table>
<table id="stats_table" cellspacing="4" cellpadding="4">
  <tr>
    <td colspan="3">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="3">&nbsp;</td>
  </tr>
  <tr>
    <td>Topic</td>
    <td>Rating</td>
    <td>Comments / Suggestions</td>
  </tr>
  <tr>
    <th>RESPONSIVENESS: How do you rate our responsiveness in dealing with you?</th>
    <td>#cs_q1#</td>
    <td>#URLDecode(cs_q1_com)#</td>
  </tr>
  <tr>
    <th>PROFESSIONALISM: How do you rate our professionalism in dealing with you?</th>
    <td>#cs_q2#</td>
    <td>#URLDecode(cs_q2_com)#</td>
  </tr>
  <tr>
    <th>TECHNICAL SUPPORT: If you received any technical support, how do you rate the technical competence of our employees?</th>
    <td>#cs_q3#</td>
    <td>#URLDecode(cs_q3_com)#</td>
  </tr>
  <tr>
    <th>PRODUCT QUALITY: How do you rate our products and services and did they meet your needs and expectations regarding quality and performance?</th>
    <td>#cs_q4#</td>
    <td>#URLDecode(cs_q4_com)#</td>
  </tr>
 <!--- Taken out by request <tr>
    <th>DELIVERY: How do you rate our delivery on time performance and our commitment to meet your delivery expectations?</th>
    <td>#cs_q5#</td>
    <td>#URLDecode(cs_q5_com)#</td>
  </tr>--->
  <tr>
    <th>COMPETITIVENESS: How do you rate the competitiveness of our products and do they represent value for money?</th>
    <td>#cs_q6#</td>
    <td>#URLDecode(cs_q6_com)#</td>
  </tr>
  <tr>
    <th>QUALITY: How do you rate our approach to quality management to ensure complete customer satisfaction?</th>
    <td>#cs_q7#</td>
    <td>#URLDecode(cs_q7_com)#</td>
  </tr>
  <tr>
    <th>Overall: How do you rate HEARING PRODUCTS INTERNATIONAL LIMITED?</th>
    <td>#cs_q8#</td>
    <td>#URLDecode(cs_q8_com)#</td>
  </tr>
  <tr>
    <th colspan="2">Do you have any comments or suggestions that would help us improve our quality of products and customer services?</th>
    <td>#URLDecode(cs_q9_com)#</td>
  </tr>
  <tr>
    <th colspan="2">What could you buy from us but choose to buy from a different supplier? What are the factors influencing your decision?</th>
    <td>#URLDecode(cs_q10_com)#</td>
  </tr>
  <tr>
    <th colspan="2">How thoroughly do you believe we understand your needs and are able to meet them?</th>
    <td>#URLDecode(cs_q11_com)#</td>
  </tr>
  <tr>
    <th colspan="2">What would we need to do to satisfy your requirements even more?</th>
    <td>#URLDecode(cs_q12_com)#</td>
  </tr>
  <tr>
    <th colspan="2">In the next 3 years how will we have to respond for you to remain a customer and grow our business together and what do we need to do to support you in this process?</th>
    <td>#URLDecode(cs_q13_com)#</td>
  </tr>
  <tr>
    <th colspan="2">How did you hear about Hearing Products International Limited? (Please select one)</th>
    <td>#URLDecode(cs_q14)#</td>
  </tr>
</table>

</cfoutput>

	</div>
</div>
<div class="clearline"></div>
<!--- end container div --->
</body>
</html>
