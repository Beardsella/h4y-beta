<cfset CurrentPage=GetFileFromPath(GetTemplatePath())>
<cfif IsDefined("URL.ID") AND URL.ID neq ''>
	<cfset thisid = URL.ID>
<cfelse>
	<cfset thisid = FORM.ID>
</cfif>

<cfif IsDefined("FORM.MM_InsertRecord") AND FORM.MM_InsertRecord EQ "form1">
  <cfquery datasource="#application.dsn#">
    UPDATE email_messages
	SET esubject =
  <cfif IsDefined("FORM.esubject") AND FORM.esubject NEQ "">
    '#FORM.esubject#'
      <cfelse>
    NULL
  </cfif>
    ,
	econtent =
  <cfif IsDefined("FORM.econtent") AND FORM.econtent NEQ "">
    '#URLEncodedFormat(Trim(FORM.econtent))#'
      <cfelse>
    NULL
  </cfif>
  ,
  esummary =
  <cfif IsDefined("FORM.esummary") AND FORM.esummary NEQ "">
    '#FORM.esummary#'
      <cfelse>
    NULL
  </cfif>
   WHERE ID = #FORM.ID#
  </cfquery>
</cfif>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/Templates/HPIL_admin.dwt.cfm" codeOutsideHTMLIsLocked="false" -->
<head>
<cfif NOT IsDefined ("session.admininitials") OR session.admininitials eq 999>
	<cflocation url="index.cfm?passwordcheck=failed" addtoken="no">
</cfif>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<title>Web admin</title>

<link href="../css/HPIL_admin.css" rel="stylesheet" type="text/css" media="all">


<cfquery name="getmessage" datasource="#application.dsn#">
SELECT ID, esubject, econtent, esummary
FROM email_messages
WHERE ID = #thisid#
</cfquery>

</head>

<body>
<div id="container">
	<div id="logo"><img src="../images/HPIL_logo_gif.gif" /></div>
 	<div class="clearline"></div>
	<div id="nav">
			<cfinclude template="nav_admin.cfm">	
	</div>
	<div id="pagecontent">
	  	<div id="pagetitle">
	  	
		<h2>Edit Email Message</h2>	  
	  
	  	</div>
<div class="clearline"></div>
		

<div class="gofloat">
	<cfif IsDefined("FORM.ID")><p>UPDATED !</p></cfif>
    <cfform method="post" name="form1" enctype="multipart/form-data" action="#CurrentPage#">
      <table>
        <tr>
          <td class="formlabel">Subject line:</td>
          <td><input type="text" name="esubject" value="<cfoutput>#getmessage.esubject#</cfoutput>" size="40"></td>
        </tr>
        <tr>
          <td class="formlabel">Summary:</td>
          <td><input type="text" name="esummary" value="<cfoutput>#getmessage.esummary#</cfoutput>" size="40"></td>
        </tr>
        <tr>
          <td class="formlabel">Message:</td>
          <td><textarea name="econtent" richtext="true" toolbar="pql" width="660" height="700"><cfoutput>#URLDecode(getmessage.econtent)#</cfoutput></textarea></td>
        </tr>
        <tr>
          <td>&nbsp;</td>
          <td><input type="submit" value="Update record"></td>
        </tr>
      </table>
      <input type="hidden" name="MM_InsertRecord" value="form1">
      <input type="hidden" name="ID" value="<cfoutput>#thisid#</cfoutput>">
    </cfform>
</div>
    
	</div>
</div>
<div class="clearline"></div>
<!--- end container div --->
</body>
</html>
