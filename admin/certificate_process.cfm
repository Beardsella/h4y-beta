<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/Templates/HPIL_admin.dwt.cfm" codeOutsideHTMLIsLocked="false" -->
<head>
<cfif NOT IsDefined ("session.admininitials") OR session.admininitials eq 999>
	<cflocation url="index.cfm?passwordcheck=failed" addtoken="no">
</cfif>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<title>Website Admin</title>

<link href="../css/HPIL_admin.css" rel="stylesheet" type="text/css" media="all">



    
<cfset path = expandPath("../product-images/certificates/empty_me/")>
<cfset main_path = expandPath("../product-images/certificates/")>
<cfset tn_path = expandPath("../product-images/certificates/tn/")>


<!--- first a bit of housekeeping --->
<cfdirectory directory="#path#" action="list" name="currentDir">
  <cfloop query="currentDir">
<cftry>
	<cffile action="delete" file="#path##currentDir.Name#">
<cfcatch></cfcatch>
</cftry>
  </cfloop>
	
	<cfset accept_upload = 1>

	<!--- only allow these mime types. Edit as required --->
    <cffile action="upload" filefield="c_img" destination="#GetTempDirectory()#" nameconflict="overwrite" result="uploadResult" accept="image/jpg, application/msword, image/bmp, image/gif, image/jpeg, application/pdf, application/rtf, text/plain">
    
    <!--- only allow these file extensions. Edit as required --->
    <cfif NOT ListFindNoCase("pdf,doc,rtf,txt,bmp,gif,jpg,jpeg,png", uploadResult.ServerFileExt)>
        <cffile action="delete" file="#getTempDirectory()##uploadResult.serverFile#">
    
        <cfset display_error = 'This file type cannot currently be uploaded. Please contact PQL.'>
        <cfset accept_upload = 0>
    
    </cfif>
    
    <cfif accept_upload eq 1>
	
			<!--- NON IMAGES: move the file. #path# is normally already set in the page header --->
            <cffile action="copy" source="#getTempDirectory()##uploadResult.serverFile#" destination="#path##uploadResult.serverFile#" nameconflict="makeunique">
            
            <cfset sf = uploadResult.serverFile>
            
	</cfif>
          
	<!--- delete the temp file in all cases --->
    <cffile action="delete" file="#getTempDirectory()##uploadResult.serverFile#">


<cfset myex = listLast(sf,'.')>
        
<cfif myex eq "pdf">


<!--- upload new PDF file --->

    <cfset title_now = Replace(FORM.c_title, ' ', '_', 'all')>

    <cfset title_now = Replace(title_now, ' ', '_', 'all')>
    
    
    <cffile action="copy" source="#path##sf#" destination="#main_path##title_now#.pdf" nameconflict="makeunique">


    <cfquery datasource="#application.dsn#">
      INSERT INTO certificates (c_title, c_img, c_pdf) VALUES (
      <cfif IsDefined("FORM.c_title") AND #FORM.c_title# NEQ "">
        '#FORM.c_title#'
          <cfelse>
          ''
      </cfif>
      ,
      <cfif IsDefined("FORM.c_img") AND #FORM.c_img# NEQ "">
        '#title_now#_page_1.jpg'
          <cfelse>
          ''
      </cfif>
      ,'#title_now#.pdf'
    )
    </cfquery>
      
      <cfpdf action="thumbnail" source="#path##sf#" pages="1" destination="../product-images/certificates/tn/" 
         imagePrefix="#title_now#" format="jpg" scale="25" resolution="low" overwrite="yes">
      
      
    <cflocation addtoken="no" url="certificates.cfm">


<!---Upload img--->
<cfelse>

    <cfimage source="#path##sf#" name="myimage">	
    
    <!--- make big image --->
    
        
        <cfset title_now = Replace(FORM.c_title, ' ', '_', 'all')>
    
        <cfset title_now = Replace(title_now, ' ', '_', 'all')>
        
    
    <cfset ImageScaleToFit(myimage,650,910)>
        
        
    <cfimage
        action = "write"
        destination = "#main_path##title_now#.#myex#"
        source = "#myimage#"
        overwrite="yes">
            
    <!--- make thumbnail --->
    <cfset ImageScaleToFit(myimage,150,150)>
    
    <cfimage
        action = "write"
        destination = "#tn_path##title_now#.#myex#"
        source = "#myimage#"
        overwrite="yes">
        
    <cfquery datasource="#application.dsn#">
      INSERT INTO certificates (c_title, c_img) VALUES (
      <cfif IsDefined("FORM.c_title") AND #FORM.c_title# NEQ "">
        "#FORM.c_title#"
          <cfelse>
          ''
      </cfif>
      ,
      <cfif IsDefined("FORM.c_img") AND #FORM.c_img# NEQ "">
        "#title_now#.#myex#"
          <cfelse>
          ''
      </cfif>
    )
    </cfquery>
        
    
    <cflocation addtoken="no" url="certificates.cfm">

</cfif>

</head>

<body>
<div id="container">
	<div id="logo"><img src="../images/HPIL_logo_gif.gif" /></div>
 	<div class="clearline"></div>
	<div id="nav">
			<cfinclude template="nav_admin.cfm">	
	</div>
	<div id="pagecontent">
	  	<div id="pagetitle">
	  	header
	  	</div>
<div class="clearline"></div>
		

	</div>
</div>
<div class="clearline"></div>
<!--- end container div --->
</body>
</html>
