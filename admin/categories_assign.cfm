<cfquery name="topcats" datasource="#application.dsn#">
SELECT id, cat_name
FROM categories
WHERE parentid = 1
ORDER BY display_order ASC
</cfquery>

<!--- get the cats this product is in --->
<cfquery name="getcats" datasource="#application.dsn#">
SELECT A.id, B.cat_name, B.parentid, C.title
FROM (product_cats A INNER JOIN categories B ON A.category_id = B.id) INNER JOIN products C ON A.product_id = C.ID
WHERE A.product_id = #URL.id#
</cfquery>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/templates/HPIL_admin.dwt.cfm" codeOutsideHTMLIsLocked="false" -->
<head>
<cfif NOT IsDefined ("session.admininitials") OR session.admininitials eq 999>
	<cflocation url="index.cfm?passwordcheck=failed" addtoken="no">
</cfif>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<title>Website Admin</title>

<link href="../css/HPIL_admin.css" rel="stylesheet" type="text/css" media="all">




<script src="../javascripts/engine.js"></script>
<script src="../javascripts/prototype.js"></script>
<script src="../javascripts/scriptaculous.js"></script>
<script type="text/JavaScript">
<!--
function selectCat(catid, listid){
	param = new Object();
	param.catid = catid
	param.listid = listid
	http( 'POST'  , 'categories_assign_aj.cfm' , cat_response, param ); 
}
function removeCat(listid, catlistid){
	param = new Object();
	param.catlistid = catlistid
	param.listid = listid
	http( 'POST'  , 'categories_assign_aj.cfm' , cat_response, param ); 
}
function cat_response(obj){ 
	document.getElementById('selectedcats').innerHTML = obj.message
}
//-->
</script>

</head>

<body>
<div id="container">
	<div id="logo"><img src="../images/HPIL_logo_gif.gif" /></div>
 	<div class="clearline"></div>
	<div id="nav">
			<cfinclude template="nav_admin.cfm">	
	</div>
	<div id="pagecontent">
	  	<div id="pagetitle">
	  	
			<h1>Product Categories</h1>
	
	  	</div>
<div class="clearline"></div>
		

	 <h2><cfoutput><a href="edit_product.cfm?id=#URL.id#">Return to #URLDecode(getcats.title)#</a></cfoutput></h2>
<span id="selectedcats">
<cfif getcats.recordcount>
	<p>This listing will appear in the following categories.</p>
	<p>Click remove to unassign a category.</p>
	<p>To add an extra category, click it's name.</p>

	<table cellpadding="6">
	<cfloop query="getcats">
<!--- get the parents name too --->
		<cfquery name="getparent" datasource="#application.dsn#">
		SELECT cat_name
		FROM categories
		WHERE id = #getcats.parentid#
		ORDER BY display_order ASC
		</cfquery>
	
		<cfoutput>
		<tr>
			<td>#getparent.cat_name# &nbsp;&gt;&nbsp; #getcats.cat_name#</td>
			<td><a href="##" onClick="removeCat(#URL.id#, #getcats.id#)">remove</a></td>
		</tr>
		</cfoutput>
	</cfloop>
	</table>
<cfelse>
<p>You now need to click a category name to add this product to it.</p>
<p>You can add this item to multiple appropriate categories.</p>
</cfif>
</span>
<div class="full_line"></div>
<cfloop query="topcats">
<div class="catbox">
	<cfquery name="subcats" datasource="#application.dsn#">
	SELECT id, cat_name
	FROM categories
	WHERE parentid = #topcats.id#
	ORDER BY display_order ASC, cat_name ASC
	</cfquery>
<cfif subcats.recordcount>
	<cfoutput><p><b>#topcats.cat_name#</b></p></cfoutput>
<cfelse>
	<cfoutput><a href="##" onclick="selectCat(#topcats.id#, #URL.id#)"><b>#topcats.cat_name#</b></a></cfoutput>
</cfif>
	<cfloop query="subcats">
        <cfquery name="subsubcats" datasource="#application.dsn#">
        SELECT id, cat_name
        FROM categories
        WHERE parentid = #subcats.id#
        ORDER BY display_order ASC, cat_name ASC
        </cfquery>
	<cfif subsubcats.recordcount>
		<cfoutput><a href="##">#subcats.cat_name#</a><br /></cfoutput>
        <cfloop query="subsubcats">
                <cfoutput><a href="##" onclick="selectCat(#subsubcats.id#, #URL.id#)">&nbsp;&nbsp;&nbsp;&nbsp;-#subsubcats.cat_name#</a><br /></cfoutput>
        </cfloop>
    <cfelse>
		<cfoutput><a href="##" onclick="selectCat(#subcats.id#, #URL.id#)">#subcats.cat_name#</a><br /></cfoutput>
    </cfif>
	</cfloop>
</div>
</cfloop>
	
	</div>
</div>
<div class="clearline"></div>
<!--- end container div --->
</body>
</html>