<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/Templates/HPIL_admin.dwt.cfm" codeOutsideHTMLIsLocked="false" -->
<head>
<cfif NOT IsDefined ("session.admininitials") OR session.admininitials eq 999>
	<cflocation url="index.cfm?passwordcheck=failed" addtoken="no">
</cfif>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<title>Website Admin</title>

<link href="../css/HPIL_admin.css" rel="stylesheet" type="text/css" media="all">



<cfset tn_path = "../product-images/certificates/tn/">


</head>

<body>
<div id="container">
	<div id="logo"><img src="../images/HPIL_logo_gif.gif" /></div>
 	<div class="clearline"></div>
	<div id="nav">
			<cfinclude template="nav_admin.cfm">	
	</div>
	<div id="pagecontent">
	  	<div id="pagetitle">
	  	Certificates
	  	</div>
<div class="clearline"></div>
		


<div style="width:30%; float:left; display:inline;">   
	<cfform name="certificate_add" action="certificate_process.cfm" method="post" enctype="multipart/form-data">
    
        <table cellpadding="4" cellspacing="4">
            <tr>
                <td colspan="2">Add a new certificate</td>
            </tr>
            <tr>
                <td>Certificate Title</td>
                <td><input type="text" name="c_title" id="c_title" /></td>
            </tr>
            <tr>
                <td>Certificate image</td>
                <td><input type="file" name="c_img" id="c_img" /></td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td><input type="submit" value="upload" /></td>
            </tr>
        </table>
            
    </cfform>
</div>
<div class="clearline"></div>
            
            <cfquery datasource="#application.dsn#" name="certs">
                SELECT *
                FROM certificates
            </cfquery>

<div style="width:80%; float:left; display:inline; margin-left:40px;">   
    
        <cfoutput query="certs">
        
            <div class="certs_div">
            
                <img src="#tn_path##c_img#" /><br />
                <p>#c_title# - <a href="cert_delete.cfm?id=#id#"><img src="../images/delete_red.gif" border="0" /></a></p>
                
        	</div>
            
        </cfoutput>
    

</div>

	</div>
</div>
<div class="clearline"></div>
<!--- end container div --->
</body>
</html>
