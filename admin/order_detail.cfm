<cfset CurrentPage=GetFileFromPath(GetTemplatePath())>

<cfif IsDefined("FORM.update_order") AND FORM.update_order EQ "yes">
	<cfquery datasource="#application.dsn#">
	UPDATE ordersummary SET ordernotes = '#URLEncodedFormat(FORM.notes)#'
	WHERE orderid = #FORM.id#
	</cfquery>
</cfif>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/templates/HPIL_admin.dwt.cfm" codeOutsideHTMLIsLocked="false" -->
<head>
<cfif NOT IsDefined ("session.admininitials") OR session.admininitials eq 999>
	<cflocation url="index.cfm?passwordcheck=failed" addtoken="no">
</cfif>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<title>Hearing Aids and Speech Amplification</title>


<link href="../css/HPIL_admin.css" rel="stylesheet" type="text/css" media="all">


<script src="../javascripts/engine.js"></script>
<script src="../javascripts/prototype.js"></script>
<script src="../javascripts/scriptaculous.js"></script>
<script type="text/JavaScript">
<!--
/*update from radio groups*/
function updateRadio(thisvalue, thisfield, thisid, thistable){
	param = new Object();
	param.thisvalue = thisvalue
	param.thisfield = thisfield
	param.thisid = thisid
	param.thistable = thistable
	http( 'POST'  , 'ajax_radio.cfm' , radio_response, param ); 
}
function radio_response(){ 
}
//-->
</script>

<cfquery name="orderinfo" datasource="#application.dsn#">
SELECT A.orderid, A.cartid, A.ordervalue, A.deliverycost, A.orderdate, A.customername, A.ordernotes, A.invoice_date, A.orderstatus, B.*
FROM ordersummary A INNER JOIN customerinfo B ON A.customerid = B.custid
WHERE A.orderid = #url.id#
</cfquery>
<cfquery name="orderlines" datasource="#application.dsn#">
SELECT A.prodname, A.size, A.colour, A.quantity, A.price, A.vat, A.child_product, A.product_option, B.title
FROM orders A INNER JOIN products B ON A.prodid = B.id
WHERE A.cartid = '#orderinfo.cartid#'
ORDER BY A.id ASC
</cfquery>

</head>

<body>
<div id="container">
	<div id="logo"><img src="../images/HPIL_logo_gif.gif" /></div>
 	<div class="clearline"></div>
	<div id="nav">
			<cfinclude template="nav_admin.cfm">	
	</div>
	<div id="pagecontent">
	  	<div id="pagetitle">
	  	
		 Order <cfoutput>#URL.id#</cfoutput>
		
	  	</div>
<div class="clearline"></div>
		

<cfif IsDefined("orderinfo.orderstatus") AND orderinfo.orderstatus eq 1>
	<span class="highlight">This order is UNPAID</span>
<cfelse>
    <div class="gofloat">
            <table width="200">
              <tr>
                <td><label>
                  <input type="radio" name="orderstatus" value="2" onclick="updateRadio(this.value, this.name, <cfoutput>#URL.ID#</cfoutput>, 'ordersummary')" <cfif orderinfo.orderstatus eq 2>checked</cfif> />OPEN</label>
                </td>
                <td><label>
                  <input type="radio" name="orderstatus" value="3"  onclick="updateRadio(this.value, this.name, <cfoutput>#URL.ID#</cfoutput>, 'ordersummary')" <cfif orderinfo.orderstatus eq 3>checked</cfif> />COMPLETED</label>
                </td>
              </tr>
            </table>
    </div>
</cfif>
<div class="gofloat">
<cfoutput>#orderinfo.orderdate#</cfoutput><br />
<cfoutput>#orderinfo.customername#</cfoutput><br />
</div>
<!---<div class="gofloat">
  <p><a href="cust_invoice_email_pdf.cfm?id=<cfoutput>#URL.id#</cfoutput>" target="_blank"><img src="../images/invoice.png" alt="Email PDF Invoice" border="0" /></a></p>
  <cfif IsDefined("orderinfo.invoice_date") AND orderinfo.invoice_date neq ''>
  		<p>Invoice emailed <cfoutput>#orderinfo.invoice_date#</cfoutput></p>
  </cfif>
</div>
--->
<div class="greyline"></div>
<div class="gofloat">
<table width="70%" border="0">
  <tr>
    <td class="label">&nbsp;</td>
    <td class="label">Price(ex VAT)</td>
    <td class="label">Qty</td>
    <td class="label">&nbsp;</td>
  </tr>
<cfset netgoods = 0>
<cfset totvat = 0>
<cfoutput query="orderlines"> 
	<cfif IsDefined("orderlines.child_product") AND orderlines.child_product neq ''>
		<cfquery name="kid" datasource="#application.dsn#">
		SELECT child_title FROM child_products WHERE id = #orderlines.child_product#
		</cfquery>
	</cfif>

  <cfset totvat = totvat + numberformat(vat*quantity, '.00')>
  <cfset netgoods = netgoods + numberformat(price*quantity, '.00')>
  <tr>
    <td class="longtext">
	#URLDecode(title)#
	<cfif IsDefined("kid.child_title") AND kid.child_title neq ''>
    	<br />#kid.child_title#
    </cfif>
	<cfif IsDefined("orderlines.product_option") AND orderlines.product_option neq ''>
    	<br />#orderlines.product_option#
    </cfif>
    </td>
    <td class="longtext">#numberformat(price, '.00')#</td>
    <td class="longtext">#quantity#</td>
    <td class="money">#numberformat(price*quantity, '.00')#</td>
  </tr>
</cfoutput>
  <tr>  
    <td colspan="4">&nbsp;</td>
  </tr>
<tr>  
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>Subtotal</td>
    <td class="money"><cfoutput>#numberformat(netgoods, '.00')#</cfoutput></td>
  </tr>
  <tr>  
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>VAT</td>
    <td class="money"><cfoutput>#numberformat(totvat, '.00')#</cfoutput></td>
  </tr>
  <tr>  
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>Total</td>
    <td class="money"><cfoutput>#numberformat(netgoods+totvat, '.00')#</cfoutput></td>
  </tr>
</table>
</div>
<div class="blueline"></div>
<div class="gofloat">
<p class="label">Billing Address:</p>
<cfoutput>#URLDecode(orderinfo.email)#</cfoutput><br />
<cfoutput>#orderinfo.telephone#</cfoutput><br />
<cfoutput>#URLDecode(orderinfo.add1)#</cfoutput><br />
<cfoutput>#URLDecode(orderinfo.add2)#</cfoutput><br />
<cfoutput>#URLDecode(orderinfo.add3)#</cfoutput><br />
<cfoutput>#URLDecode(orderinfo.county)#</cfoutput><br />
<cfoutput>#URLDecode(orderinfo.postcode)#</cfoutput><br />
<cfoutput>#orderinfo.Country#</cfoutput><br />
</div>
<div class="gofloat">
<p class="label">Delivery Address:</p>
<cfoutput>#URLDecode(orderinfo.delname)#</cfoutput><br />
<cfoutput>#URLDecode(orderinfo.deladd1)#</cfoutput><br />
<cfoutput>#URLDecode(orderinfo.deladd2)#</cfoutput><br />
<cfoutput>#URLDecode(orderinfo.deladd3)#</cfoutput><br />
<cfoutput>#URLDecode(orderinfo.delcounty)#</cfoutput><br />
<cfoutput>#URLDecode(orderinfo.delpostcode)#</cfoutput><br />
<cfoutput>#orderinfo.delCountry#</cfoutput><br />
</div>
<div class="greyline"></div>
<div id="ordernotes">
<form action="<cfoutput>#CurrentPage#?id=#id#</cfoutput>" method="post">
<textarea name="notes" cols="40" rows="8">
<cfoutput>#URLDecode(orderinfo.ordernotes)#</cfoutput>
</textarea>
<input name="id" type="hidden" value="<cfoutput>#URL.id#</cfoutput>" />
<input name="update_order" type="hidden" value="yes" />
<input name="submit" type="submit" value="save notes" />
</form>
</div>

	</div>
</div>
<div class="clearline"></div>
<!--- end container div --->
</body>
</html>
