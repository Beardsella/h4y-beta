<cfsetting requesttimeout="3600">

<cfset path = expandPath("../empty_me/")>

<!--- first a bit of housekeeping --->
<cfdirectory directory="#path#" action="list" name="currentDir">
  <cfloop query="currentDir">
<cftry>
	<cffile action="delete" file="#path##currentDir.Name#">
<cfcatch></cfcatch>
</cftry>
  </cfloop>

<!-- Bootstrap CSS-->
 <link href="../css/bootstrap.css" rel="stylesheet">  


<cfset CurrentPage=GetFileFromPath(GetTemplatePath())>
<cfif IsDefined("FORM.MM_InsertRecord") AND FORM.MM_InsertRecord EQ "form1">


    


 <!--- Upload image and resize, make a thumbnail too --->
  
  	<cfif IsDefined("FORM.newImage") AND #FORM.newImage# NEQ "">
		
			<cfset accept_upload = 1>
        
            <!--- only allow these mime types. Edit as required --->
            <cffile action="upload" filefield="newImage" destination="#GetTempDirectory()#" nameconflict="overwrite" result="uploadResult" accept="image/jpg, image/bmp, image/gif, image/jpeg">
            
            <!--- allow images only if needed --->
            <cfif Not IsImageFile(getTempDirectory() & uploadResult.serverFile)>
                <cffile action="delete" file="#getTempDirectory()##uploadResult.serverFile#">
                <cfset accept_upload = 0>
            </cfif>
            
            <!--- only allow these file extensions. Edit as required --->
            <cfif NOT ListFindNoCase("bmp,gif,jpg,jpeg,png", uploadResult.ServerFileExt)>
                <cffile action="delete" file="#getTempDirectory()##uploadResult.serverFile#">
            
                <cfset display_error = 'This file type cannot currently be uploaded. Please contact PQL.'>
                <cfset accept_upload = 0>
            
            </cfif>
            
            <cfif accept_upload eq 1>


<cfif IsDefined("FORM.image_caption") AND FORM.image_caption NEQ "">



  <cfset new_name_with_no_spaces = #REReplace('#URLDecode(FORM.image_caption)#',"[^0-9A-Za-z &/]","","all")# >

  <cfset new_name_with_no_spaces = Replace('#new_name_with_no_spaces#', ' ', '-', 'All')>

  <cfset new_name_with_no_spaces = Replace('#new_name_with_no_spaces#', '/', '-', 'All')>

  <cfset new_name_with_no_spaces = Replace('#new_name_with_no_spaces#', '--', '', 'All')>

  <cfset new_name_with_no_spaces = Replace('#new_name_with_no_spaces#', '---', '', 'All')>

  <cfset new_name_with_no_spaces = Replace('#new_name_with_no_spaces#', '&', 'and', 'All')>

  <cfset new_name_with_no_spaces = "#new_name_with_no_spaces#" & ".jpg">

  <cfset new_name_with_no_spaces = Replace('#new_name_with_no_spaces#', '-.jpg', '.jpg', 'All')>

  <cfset new_name_with_no_spaces = "#LCase(new_name_with_no_spaces)#">

<cfelse>

  <cfset new_name_with_no_spaces = #REReplace('#URLDecode(uploadResult.serverFile)#',"[^0-9A-Za-z &/]","","all")# >

  <cfset new_name_with_no_spaces = Replace('#new_name_with_no_spaces#', ' ', '-', 'All')>

  <cfset new_name_with_no_spaces = Replace('#new_name_with_no_spaces#', '/', '-', 'All')>

  <cfset new_name_with_no_spaces = Replace('#new_name_with_no_spaces#', '--', '', 'All')>

  <cfset new_name_with_no_spaces = Replace('#new_name_with_no_spaces#', '---', '', 'All')>

  <cfset new_name_with_no_spaces = Replace('#new_name_with_no_spaces#', '&', 'and', 'All')>

  <cfset new_name_with_no_spaces = "#LCase(new_name_with_no_spaces)#">

</cfif>
              
            
                    <!--- IMAGES: move the file. #path# is normally already set in the page header---> 
                    <cfimage action="WRITE" source="#getTempDirectory()##uploadResult.serverFile#" destination="#path##new_name_with_no_spaces#" overwrite="yes">
                            
            </cfif>
                  
            <!--- delete the temp file in all cases --->
            <cffile action="delete" file="#getTempDirectory()##uploadResult.serverFile#">
		
		<cfimage source="#path##New_name_with_no_spaces#" name="lpic">		
		
		<!--- make big image --->
		<cfset ImageScaleToFit(lpic,800,600)>

<cfimage
    action = "write"
    destination = "../product-images/#New_name_with_no_spaces#"
    source = "#lpic#"
    overwrite = "yes">
		
<!--- make thumbnail --->
		<cfset ImageScaleToFit(lpic,250,250)>

<cfimage
    action = "write"
    destination = "../product-images/thumbs/#New_name_with_no_spaces#"
    source = "#lpic#"
    overwrite = "yes">

</cfif>


  <cfquery datasource="#application.dsn#">
    INSERT INTO product_images (product_id, image_name, image_caption) VALUES (#FORM.id#, '#new_name_with_no_spaces#', '#form.image_caption#')
  </cfquery>


<!---  <cflocation url="categories_assign.cfm?id=#form.id#" addtoken="no">
---></cfif>
<cfif IsDefined("form.id") AND #form.id# neq ''>
	<cfset prodid = #form.id#>
<cfelse>
	<cfset prodid = #url.id#>
</cfif>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"><html xmlns="http://www.w3.org/1999/xhtml">

<head>
<cfif NOT IsDefined ("session.admininitials") OR session.admininitials eq 999>
	<cflocation url="index.cfm?passwordcheck=failed" addtoken="no">
</cfif>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<title>Administration</title>

<link href="../css/HPIL_admin.css" rel="stylesheet" type="text/css" media="all">


<!--- get existing images --->
<cfquery name="getpics" datasource="#application.dsn#">
SELECT id, image_name, image_caption FROM product_images WHERE product_id = #prodid#
</cfquery>

<cfquery name="prod_title" datasource="#application.dsn#">
SELECT title FROM products WHERE id = #prodid#
</cfquery>

<script type="text/javascript">
<!--
function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
//-->
</script>

</head>

<body onLoad="MM_preloadImages('../images/delete_red.gif')">
<div id="container">
	<div id="logo"><img src="../images/HPIL_logo_gif.gif" /></div>
 	<div class="clearline"></div>
	<div id="nav">
			<cfinclude template="nav_admin.cfm">	
	</div>
	<div id="pagecontent">
	  	<div id="pagetitle">

		<cfoutput><h1>Manage Aditional Images -> <a href="edit_product.cfm?id=#prodid#">#URLDecode(prod_title.title)#</a> -> <span class="badge">#getpics.recordcount#</span></h1></cfoutput>
	  	</div>
<div class="clearline"></div>
		

<div class="gofloat">
    <cfloop query="getpics">
<div class="gofloat_nowidth">
        	<cfoutput><a href="../product-images/#image_name#" target="_blank"><img src="../product-images/thumbs/#image_name#"></a></cfoutput>
          <br /><cfoutput>#image_caption#</cfoutput>
            <br /><a href="image_delete.cfm?id=<cfoutput>#getpics.id#</cfoutput>"><img src="../images/delete.gif" alt="Delete image" name="deleteme" width="58" height="5" id="deleteme" onMouseOver="MM_swapImage('deleteme','','../images/delete_red.gif',1)" onMouseOut="MM_swapImgRestore()" border="0" /></a></div>
    </cfloop>
</div>
<div class="gofloat">
          <table width="400" border="0">
            <tr>
              <td>
                <cfform action="#CurrentPage#" method="post" enctype="multipart/form-data" name="form1" id="form1">
                  <table>
                    <tr>
                      <td class="formlabel">Upload Photo:</td>
                      <td><input name="newImage" type="file" /></td>
                    </tr>
                    <tr>
                      <td class="formlabel">Photo caption:</td>
                      <td><input name="image_caption" type="text" size="40" /></td>
                    </tr>
                    <tr>
                      <td>&nbsp;</td>
                      <td><input type="submit" value="Upload Image" /></td>
                    </tr>
                  </table>
                  <input type="hidden" name="MM_InsertRecord" value="form1" />
                  <input type="hidden" name="id" value="<cfoutput>#prodid#</cfoutput>" />
                </cfform>
              </td>
            </tr>
          </table>
</div>
<div class="gofloat">
          <a href="categories_assign.cfm?id=<cfoutput>#prodid#</cfoutput>">Assign Categories</a>
</div>

	</div>
</div>
<div class="clearline"></div>
<!--- end container div --->
</body>
</html>