<cfif NOT IsDefined ("session.admininitials")>
	<cflocation url="index.cfm?passwordcheck=failed">
</cfif>
<cfset CurrentPage=GetFileFromPath(GetTemplatePath())>

<cfif IsDefined("FORM.MM_UpdateRecord") AND FORM.MM_UpdateRecord EQ "form1">
	<cfloop index="ListElement" list="#Form.tags#">
	<cfquery datasource="#application.dsn#">
	INSERT INTO tags (tag) VALUES(
  <cfif IsDefined("ListElement") AND #ListElement# NEQ "">
    '#ListElement#'
      <cfelse>
    NULL
  </cfif>
 )
	</cfquery>
	</cfloop>
</cfif>

<!--- get all tags --->
<cfquery name="alltags" datasource="#application.dsn#">
	SELECT id, tag
	FROM tags
	ORDER BY tag ASC
</cfquery> 
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"><html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/templates/HPIL_admin.dwt.cfm" codeOutsideHTMLIsLocked="false" -->
<head>
<cfif NOT IsDefined ("session.admininitials") OR session.admininitials eq 999>
	<cflocation url="index.cfm?passwordcheck=failed" addtoken="no">
</cfif>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<title>Website Admin</title>

<link href="../css/HPIL_admin.css" rel="stylesheet" type="text/css" media="all">


</head>

<body>
<div id="container">
	<div id="logo"><img src="../images/HPIL_logo_gif.gif" /></div>
 	<div class="clearline"></div>
	<div id="nav">
			<cfinclude template="nav_admin.cfm">	
	</div>
	<div id="pagecontent">
	  	<div id="pagetitle">
	  	
Add new tags
		
	  	</div>
<div class="clearline"></div>
		

    <div class="floatleft">
	 <table width="40%" border="0">
          <tr>
            <td colspan="3">&nbsp;</td>
          </tr>
          <tr>
            <td colspan="3">
			Enter a list of new tags, separated by a comma after each but the last.
			</td>
          </tr>
		  <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
          </tr>

<form action="<cfoutput>#CurrentPage#</cfoutput>" method="post" enctype="multipart/form-data" name="form1" id="form1">
			<tr>
            <td colspan="3">
			<textarea name="tags" cols="40" rows="6">
			</textarea>            
			</td>
          	</tr>
		  <tr>
            <td colspan="2"><input name="submit" type="submit" value="Add tags" /></td>
            <td>&nbsp;</td>
          </tr>
            <input type="hidden" name="MM_UpdateRecord" value="form1" />
</form>
        </table>
		</div>
		<div class="floatleft">
		<table width="40%" border="0">
          <tr>
            <td colspan="3">
			CURRENT TAGS
			</td>
          </tr>
          <tr>
            <td colspan="3">&nbsp;</td>
          </tr>
		 <cfoutput query="alltags"> 
		  <tr>
            <td>#tag#</td>
            <td>&nbsp;</td>
            <td><a href="tag_delete_check.cfm?id=#id#">delete</a></td>
          </tr>
       		</cfoutput>
	    </table>
		</div>

  
	</div>
</div>
<div class="clearline"></div>
<!--- end container div --->
</body>
</html>