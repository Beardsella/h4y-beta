<!DOCTYPE html>
<html>
  <head>
    <title>Hear 4 You</title>
    <LINK REL="SHORTCUT ICON" HREF="images/h4y.ico" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    
    <meta name="Keywords" content="best hearing aids, nhs hearing aids, hearing aid, hearing aids prices, digital hearing aids" />
  
    <meta name="Description" content="We Build Websites and online applications. We specialse in developing bussiness managment systems and E Commerce sites but offer a comprehensive range of web services and solutions. We work together with our clients using our experience and help you plan, build and maintain you digital strategy." />


    
    <cfif find("localhost", "#CGI.http_host#")>
      
      <!-- LESS -->
      <link href="css/bootstrap.less" rel="stylesheet">
      <link href="css/custom.less" rel="stylesheet">  
      <link href="css/products-list-grid.less" rel="stylesheet">  

    <cfelse>

      <!-- CSS -->
      <link href="css/bootstrap.css" rel="stylesheet">
      <link href="css/custom.css" rel="stylesheet">  

    </cfif>

  <cfquery datasource="#application.DSN#" name="related_products">
    SELECT product_cats.category_id, product_cats.product_id, products.*
    FROM product_cats
    JOIN products
    ON product_cats.product_id = products.id
    WHERE product_cats.category_id = "14"
  </cfquery>

  </head>
  <body>


        <!-- Marketing messaging and featurettes
    ================================================== -->
    <!-- Wrap the rest of the page in another container to center all the content. -->

    <div class="container col-lg-2 visible-lg">
    <div class="space_90px"></div>
  <div class="bs-sidebar hidden-print affix">
    <ul class="nav bs-sidenav" id="mysidenav">
            <a href="index.cfm" class="list-group-item active"  class="scroll">Home</a>
            <a href="mailto:info@hear4you.co.uk" class="list-group-item"  class="scroll">Contact Us</a>
            <a href="#what-we-do" class="list-group-item"  class="scroll">What We Do</a>
    </ul>
</div>
      </div>
    <div class="container col-lg-10" data-spy="scroll" data-target="#mysidenav" id="main_container">
        <cfinclude template="nav.cfm">

<div class="space_60px"></div>

    <div class="well well-sm">
        <strong>Our Product Catogories</strong>
        <div class="pull-right btn-group">
            <a href="#" id="list" class="btn btn-default btn-sm"><span class="glyphicon glyphicon-th-list">
            </span>List</a> <a href="#" id="grid" class="btn btn-default btn-sm"><span
                class="glyphicon glyphicon-th"></span>Grid</a>
        </div>
    </div>

    <div id="products" class="row list-group">

<cfoutput query="related_products">
  <div class="item col-xs-12 col-sm-6 col-md-4 col-lg-4">
  <div class="thumbnail">
  <div class="caption">

    <cfif len(URLDecode(title)) lt 25>
  <h5 class="group inner list-group-item-heading">
  <a href="#SEO_URL#.cfm" title="#URLDecode(title)#" class="captalize visible-xs">#URLDecode(title)# <span class="label label-primary pull-right"> &pound; #NumberFormat(price, "0.00")#</span><!---  - ID = #id# ---></a></h5> 
  <cfelse>
  <h5 class="group inner list-group-item-heading">
      <a href="#SEO_URL#.cfm" title="#URLDecode(title)#" class="captalize visible-xs">#left(URLDecode(title), 25)#... <span class="label label-primary pull-right"> &pound; #NumberFormat(price, "0.00")#</span><!---  - ID = #id# ---></a></h5> 
    </cfif>

    <cfif len(URLDecode(title)) lt 37>
  <h5 class="group inner list-group-item-heading">
  <a href="#SEO_URL#.cfm" title="#URLDecode(title)#" class="captalize visible-sm">#URLDecode(title)# <span class="label label-primary pull-right"> &pound; #NumberFormat(price, "0.00")#</span><!---  - ID = #id# ---></a></h5> 
  <cfelse>
  <h5 class="group inner list-group-item-heading">
      <a href="#SEO_URL#.cfm" title="#URLDecode(title)#" class="captalize visible-sm">#left(URLDecode(title), 37)#... <span class="label label-primary pull-right"> &pound; #NumberFormat(price, "0.00")#</span><!---  - ID = #id# ---></a></h5> 
    </cfif>

        <cfif len(URLDecode(title)) lt 29>
  <h5 class="group inner list-group-item-heading">
  <a href="#SEO_URL#.cfm" title="#URLDecode(title)#" class="captalize visible-md">#URLDecode(title)# <span class="label label-primary pull-right"> &pound; #NumberFormat(price, "0.00")#</span><!---  - ID = #id# ---></a></h5> 
  <cfelse>
  <h5 class="group inner list-group-item-heading">
      <a href="#SEO_URL#.cfm" title="#URLDecode(title)#" class="captalize visible-md">#left(URLDecode(title), 29)#... <span class="label label-primary pull-right"> &pound; #NumberFormat(price, "0.00")#</span><!---  - ID = #id# ---></a></h5> 
    </cfif>

  <h5 class="group inner list-group-item-heading">
  <a href="#SEO_URL#.cfm" title="#URLDecode(title)#" class="captalize visible-lg">#URLDecode(title)# <span class="label label-primary pull-right"> &pound; #NumberFormat(price, "0.00")#</span><!---  - ID = #id# ---></a></h5> 

<div class="clearfix"></div>
  
  <cfquery datasource="#application.DSN#" name="related_images">
    SELECT *
    FROM product_images
    WHERE product_id = #id#
  </cfquery>

  <cfif related_images.recordcount gt 0>
  <img src="product-images/#related_images.image_name#" class="img-responsive grid-img">
  <cfelse>
  <img data-src="holder.js/200x200" class="img-responsive grid-img">
  </cfif>

</div>
<!---   <div class="no-style"><p>#URLDecode(Description)#</p></div>
 --->  
  <div class="row" class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

      <div class="col-xs-11 col-md-4 col-lg-4 text-center">

      <h4 class="rating-num">4.0</h4>
        <div class="rating">
        <span class="glyphicon glyphicon-star"></span>
        <span class="glyphicon glyphicon-star"></span>
        <span class="glyphicon glyphicon-star"></span>
        <span class="glyphicon glyphicon-star"></span>
        <span class="glyphicon glyphicon-star-empty"></span>
        </div>
      </div>

    <div class="col-xs-11 col-sm-11 col-md-7 col-lg-7 text-center">
      <a href="#SEO_URL#.cfm" class="btn btn-primary btn-block">More Info</a>
      <a class="btn btn-success btn-block" href="##?id=#id#">Add to cart - &pound;#NumberFormat(price, "0")#</a>
    </div>
 
  </div>

  </div><!--- /thumbnail --->
  </div><!--- /Item --->
</cfoutput>

</div>



            <hr class="featurette-divider">


      <!-- /END THE FEATURETTES -->


      <!-- FOOTER -->
      <footer>
        <p class="pull-right"><a href="#home">Back to top</a></p>
        <p>Public Quest Limited &copy; 2014</p>
      </footer>

    </div><!-- /.container -->



<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="https://code.jquery.com/jquery.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="js/bootstrap.min.js"></script>
<!-- Less compiler -->
<script src="js/less.js" type="text/javascript"></script>

<script src="js/holder-js/holder.js"></script>

<script type="text/javascript">
    $(document).ready(function () {
    
    $('#label_html_css').popover('toggle')
    
    // $('#main_container').scrollspy({ target: '#mysidenav' }) 

    $(".navbar-nav li a[href^='#']").on('click', function(e) {

   // prevent default anchor click behavior
   e.preventDefault();

   // store hash
   var hash = this.hash;

   // animate
   $('html, body').animate({
       scrollTop: $(this.hash).offset().top
     }, 500, function(){

       // when done, add hash to url
       // (default click behaviour)
       window.location.hash = hash;
     });

});
    $("#mysidenav a[href^='#']").on('click', function(e) {

   // prevent default anchor click behavior
   e.preventDefault();

   // store hash
   var hash = this.hash;

   // animate
   $('html, body').animate({
       scrollTop: $(this.hash).offset().top
     }, 500, function(){

       // when done, add hash to url
       // (default click behaviour)
       window.location.hash = hash;
     });

}); 
        $(".index_page a[href^='#']").on('click', function(e) {

   // prevent default anchor click behavior
   e.preventDefault();

   // store hash
   var hash = this.hash;

   // animate
   $('html, body').animate({
       scrollTop: $(this.hash).offset().top
     }, 500, function(){

       // when done, add hash to url
       // (default click behaviour)
       window.location.hash = hash;
     });

}); 
     });
</script>
<script type="text/javascript">
$(function(){
    $("#mysidenav").scrollspy();
});
$(document).ready(function() {
    $('#list').click(function(event){event.preventDefault();$('#products .item').addClass('list-group-item');});
    $('#grid').click(function(event){event.preventDefault();$('#products .item').removeClass('list-group-item');$('#products .item').addClass('grid-group-item');});
});
</script>

  </body>
</html>