<cfset CurrentPage=GetFileFromPath(GetTemplatePath())>
<cfif IsDefined("FORM.MM_InsertRecord") AND FORM.MM_InsertRecord EQ "form1">
  <cfquery datasource="#application.dsn#">
    UPDATE news
  	SET nheadline =
  <cfif IsDefined("FORM.nheadline") AND #FORM.nheadline# NEQ "">
    '#FORM.nheadline#'
      <cfelse>
    NULL
  </cfif>
   ,
   ndescription =
  <cfif IsDefined("FORM.ndescription") AND #FORM.ndescription# NEQ "">
    '#URLEncodedformat(PreserveSingleQuotes(FORM.ndescription))#'
      <cfelse>
    NULL
  </cfif>
     ,
	 ncurrent =
  <cfif IsDefined("FORM.ncurrent") AND #FORM.ncurrent# NEQ "">
    #FORM.ncurrent#
      <cfelse>
    0
  </cfif>
     ,
	 nfeatured =
  <cfif IsDefined("FORM.nfeatured") AND #FORM.nfeatured# NEQ "">
    #FORM.nfeatured#
      <cfelse>
    0
  </cfif>
     ,
	 norder =
  <cfif IsDefined("FORM.norder") AND #FORM.norder# NEQ "">
    #FORM.norder#
      <cfelse>
    99
  </cfif>
     ,
	 ndate = #now()#
    WHERE ID = #FORM.ID#
  </cfquery>
  <cflocation url="news.cfm" addtoken="no">
</cfif>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/Templates/HPIL_admin.dwt.cfm" codeOutsideHTMLIsLocked="false" -->
<head>
<cfif NOT IsDefined ("session.admininitials") OR session.admininitials eq 999>
	<cflocation url="index.cfm?passwordcheck=failed" addtoken="no">
</cfif>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<title>Website admin</title>

<link href="../css/HPIL_admin.css" rel="stylesheet" type="text/css" media="all">


<cfquery name="getnews" datasource="#application.dsn#">
SELECT ID, nheadline, ndescription, ncurrent, nfeatured, norder, ndate
FROM news
WHERE ID = #URL.ID#
</cfquery>

</head>

<body>
<div id="container">
	<div id="logo"><img src="../images/HPIL_logo_gif.gif" /></div>
 	<div class="clearline"></div>
	<div id="nav">
			<cfinclude template="nav_admin.cfm">	
	</div>
	<div id="pagecontent">
	  	<div id="pagetitle">
	  	
		<h2>Edit News</h2>	  
		
	  	</div>
<div class="clearline"></div>
		

    <cfform method="post" name="form1" action="#CurrentPage#" enctype="multipart/form-data">
      <table>
        <tr>
          <td class="formlabel">Headline:</td>
          <td><cfinput type="text" name="nheadline" value="#getnews.nheadline#" size="40"></td>
        </tr>
        <tr>
          <td class="formlabel">Display order:</td>
          <td><cfinput type="text" name="norder" value="#getnews.norder#" size="6"></td>
        </tr>
        <tr>
          <td class="formlabel">Current Article:</td>
          <td><input name="ncurrent" type="checkbox" <cfif getnews.ncurrent eq 1>checked</cfif> value="1"> tick if yes</td>
        </tr>
        <tr>
          <td class="formlabel">Featured Article:</td>
          <td><input  name="nfeatured" type="checkbox" <cfif getnews.nfeatured eq 1>checked</cfif> value="1"> tick if yes</td>
        </tr>
        <tr>
          <td class="formlabel">Text:</td>
          <td>
<textarea name="ndescription" richtext="true" toolbar="pql" width="600" height="400" fontsizes="1/1,2/2,3/3,4/4,5/5,6/6,7/7">
<cfoutput>#URLDecode(getnews.ndescription)#</cfoutput>
</textarea>
		</td>
        </tr>
        <tr>
          <td>&nbsp;</td>
          <td><input type="submit" value="Update news"></td>
        </tr>
      </table>
      <cfinput type="hidden" name="ID" value="#URL.ID#">
      <cfinput type="hidden" name="MM_InsertRecord" value="form1">
    </cfform>
    
	</div>
</div>
<div class="clearline"></div>
<!--- end container div --->
</body>
</html>
