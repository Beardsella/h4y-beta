<cfset path = expandPath("../product-images/")>

<cfif NOT IsDefined ("session.admininitials")>
	<cflocation url="index.cfm?passwordcheck=failed" addtoken="no">
</cfif>

<cfquery name="getfile" datasource="#application.dsn#">
SELECT id, image_name, product_id FROM product_images WHERE ID = #url.id#
</cfquery>

<cfquery datasource="#application.dsn#">
DELETE FROM product_images WHERE ID = #url.id#
</cfquery>

<cftry>
	<cffile action="delete" file="#path##getfile.image_name#">
	<cffile action="delete" file="#path#thumbs/#getfile.image_name#">
	
<cfcatch type="any"></cfcatch>
</cftry>

<cflocation url="images_additional.cfm?id=#getfile.product_id#" addtoken="no">

