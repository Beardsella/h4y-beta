<cfsetting showdebugoutput="no" enablecfoutputonly="yes">

<cfif IsDefined("FORM.prodid") AND FORM.prodid neq ''>
<!--- unassign the product if selected --->
	<cfquery datasource="#application.dsn#">
		DELETE
		FROM product_tags
		WHERE tag_id = #FORM.tagid# AND product_id = #FORM.prodid#
	</cfquery> 
</cfif>

<!--- get the tagged products --->
<cfquery name="getprods" datasource="#application.dsn#">
	SELECT A.product_id, B.title
	FROM product_tags A INNER JOIN products B ON A.product_id = B.id
	WHERE A.tag_id = #FORM.tagid#
	ORDER BY B.title ASC
</cfquery> 


<!--- get the NON tagged products --->
<cfquery name="freeprods" datasource="#application.dsn#">
	SELECT id, title
	FROM products
	<cfif getprods.recordcount>
		WHERE id NOT IN(#valuelist(getprods.product_id)#)
	</cfif>
	ORDER BY title ASC
</cfquery> 

<!--- set results--->
<cfset result = structNew()>

<cfset result.tagged = '<table>'>

<cfloop query="getprods">
	<cfset result.tagged = result.tagged & '<tr><td>' & #URLDecode(getprods.title)# & '</td><td><a href="##" onClick="removeProd(' & #FORM.tagid# & ',' & #getprods.product_id# & ')">remove</a></td></tr>'>
</cfloop>

<cfset result.tagged = result.tagged & '</table>'>
<cfset result.taggedcount = #getprods.recordcount#>

<!--- now the untagged products --->
<cfset result.untagged = '<select name="products" size="20" multiple>'>

<cfloop query="freeprods">
		<cfset result.untagged = result.untagged & 
              '<option value="' & #freeprods.id# & '">' & #URLDecode(freeprods.title)# & '</option>'>
</cfloop>

<cfset result.untagged = result.untagged & '</select>' & #freeprods.recordcount#>
<cfset result.untaggedcount = #freeprods.recordcount#>


<cfwddx action="CFML2JS" input="#result#" toplevelvariable="r">	