<cfsetting showdebugoutput="no" enablecfoutputonly="yes">

<cfif IsDefined("FORM.id") AND FORM.id neq ''>
	<!--- get current value --->
	<cfquery name="checksub" datasource="#application.dsn#">
	SELECT eunsubscribed
	FROM mailing_list 
	WHERE ID = #FORM.id#
	</cfquery> 
	<cfif checksub.eunsubscribed eq 1>
			<cfquery datasource="#application.dsn#">
			UPDATE mailing_list
			SET eunsubscribed = 0
			WHERE ID = #FORM.id#
			</cfquery>
			
            <cfelse>
			
            <cfquery datasource="#application.dsn#">
			UPDATE mailing_list
			SET eunsubscribed = 1
			WHERE ID = #FORM.id#
			</cfquery>
			
	</cfif>
</cfif>

<!--- set results--->
<cfset result = structNew()>
	
<cfwddx action="CFML2JS" input="#result#" toplevelvariable="r">	