<cfset CurrentPage=GetFileFromPath(GetTemplatePath())>
<cfif IsDefined("FORM.MM_InsertRecord") AND FORM.MM_InsertRecord EQ "form1">
  <cfquery datasource="#application.dsn#">
    INSERT INTO clients (client_name, contact_name, client_telephone, client_email, client_password, client_add1, client_add2, client_add3, client_county, client_postcode, client_deladd1, client_deladd2, client_deladd3, client_delcounty, client_delpostcode, client_eunsubscribed) VALUES (
  <cfif IsDefined("FORM.client_name") AND #FORM.client_name# NEQ "">
    '#FORM.client_name#'
      <cfelse>
    NULL
  </cfif>
   ,
  <cfif IsDefined("FORM.contact_name") AND #FORM.contact_name# NEQ "">
    '#FORM.contact_name#'
      <cfelse>
    NULL
  </cfif>
     ,
  <cfif IsDefined("FORM.client_telephone") AND #FORM.client_telephone# NEQ "">
    '#FORM.client_telephone#'
      <cfelse>
    NULL
  </cfif>
     ,
  <cfif IsDefined("FORM.client_email") AND #FORM.client_email# NEQ "">
    '#URLEncodedformat(FORM.client_email)#'
      <cfelse>
    NULL
  </cfif>
     ,
  <cfif IsDefined("FORM.client_password") AND #FORM.client_password# NEQ "">
    '#FORM.client_password#'
      <cfelse>
    NULL
  </cfif>
     ,
  <cfif IsDefined("FORM.client_add1") AND #FORM.client_add1# NEQ "">
    '#FORM.client_add1#'
      <cfelse>
    NULL
  </cfif>
      ,
  <cfif IsDefined("FORM.client_add2") AND #FORM.client_add2# NEQ "">
    '#FORM.client_add2#'
      <cfelse>
    NULL
  </cfif>
     ,
  <cfif IsDefined("FORM.client_add3") AND #FORM.client_add3# NEQ "">
    '#FORM.client_add3#'
      <cfelse>
    NULL
  </cfif>
      ,
  <cfif IsDefined("FORM.client_county") AND #FORM.client_county# NEQ "">
    '#FORM.client_county#'
      <cfelse>
    NULL
  </cfif>
      ,
  <cfif IsDefined("FORM.client_postcode") AND #FORM.client_postcode# NEQ "">
    '#FORM.client_postcode#'
      <cfelse>
    NULL
  </cfif>
     ,
  <cfif IsDefined("FORM.client_deladd1") AND #FORM.client_deladd1# NEQ "">
    '#FORM.client_deladd1#'
      <cfelse>
    NULL
  </cfif>
      ,
  <cfif IsDefined("FORM.client_deladd2") AND #FORM.client_deladd2# NEQ "">
    '#FORM.client_deladd2#'
      <cfelse>
    NULL
  </cfif>
     ,
  <cfif IsDefined("FORM.client_deladd3") AND #FORM.client_deladd3# NEQ "">
    '#FORM.client_deladd3#'
      <cfelse>
    NULL
  </cfif>
      ,
  <cfif IsDefined("FORM.client_delcounty") AND #FORM.client_delcounty# NEQ "">
    '#FORM.client_delcounty#'
      <cfelse>
    NULL
  </cfif>
      ,
  <cfif IsDefined("FORM.client_delpostcode") AND #FORM.client_delpostcode# NEQ "">
    '#FORM.client_delpostcode#'
      <cfelse>
    NULL
  </cfif>
      ,
  <cfif IsDefined("FORM.client_eunsubscribed") AND #FORM.client_eunsubscribed# EQ 1>
    1
      <cfelse>
    0
  </cfif>
  )
  </cfquery>
</cfif>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/Templates/HPIL_admin.dwt.cfm" codeOutsideHTMLIsLocked="false" -->
<head>
<cfif NOT IsDefined ("session.admininitials") OR session.admininitials eq 999>
	<cflocation url="index.cfm?passwordcheck=failed" addtoken="no">
</cfif>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<title>Website admin</title>

<link href="../css/HPIL_admin.css" rel="stylesheet" type="text/css" media="all">


<cfquery name="allclients" datasource="#application.dsn#">
SELECT ID, client_name, contact_name, client_telephone, client_email, client_password
FROM clients
ORDER BY client_name ASC
</cfquery>

</head>

<body>
<div id="container">
	<div id="logo"><img src="../images/HPIL_logo_gif.gif" /></div>
 	<div class="clearline"></div>
	<div id="nav">
			<cfinclude template="nav_admin.cfm">	
	</div>
	<div id="pagecontent">
	  	<div id="pagetitle">
	  	
		<h2>Clients</h2>	  
		
	  	</div>
<div class="clearline"></div>
		

    <cfform method="post" name="form1" action="#CurrentPage#" enctype="multipart/form-data">
      <table>
        <tr>
          	<td class="formlabel">Client:</td>
          	<td><cfinput type="text" name="client_name" value="" size="30"></td>
       		<td>&nbsp;</td>
        </tr>
        <tr>
          	<td class="formlabel">Contact Name:</td>
          	<td><cfinput type="text" name="contact_name" value="" size="30"></td>
       		<td>&nbsp;</td>
        </tr>
        <tr>
          	<td class="formlabel">Telephone:</td>
          	<td><cfinput type="text" name="client_telephone" value="" size="30"></td>
       		<td>&nbsp;</td>
        </tr>
        <tr>
          	<td class="formlabel">Email:</td>
          	<td><cfinput type="text" name="client_email" value="" size="30"></td>
       		<td>&nbsp;</td>
        </tr>
        <tr>
          	<td class="formlabel">Password:</td>
         	<td><cfinput type="text" name="client_password" value="" size="20"></td>
       		<td>&nbsp;</td>
        </tr>
        <tr>
          	<td class="formlabel">Address</td>
         	<th>BILLING</th>
         	<th>DELIVERY</th>
        </tr>
        <tr>
          	<td>&nbsp;</td>
          	<td><cfinput type="text" name="client_add1" value="" size="30"></td>
       		<td><cfinput type="text" name="client_deladd1" value="" size="30"></td>
        </tr>
        <tr>
          	<td>&nbsp;</td>
          	<td><cfinput type="text" name="client_add2" value="" size="30"></td>
       		<td><cfinput type="text" name="client_deladd2" value="" size="30"></td>
        </tr>
        <tr>
          	<td>&nbsp;</td>
          	<td><cfinput type="text" name="client_add3" value="" size="30"></td>
       		<td><cfinput type="text" name="client_deladd3" value="" size="30"></td>
        </tr>
        <tr>
          	<td class="formlabel">County</td>
          	<td><cfinput type="text" name="client_county" value="" size="30"></td>
       		<td><cfinput type="text" name="client_delcounty" value="" size="30"></td>
        </tr>
        <tr>
          	<td class="formlabel">Postcode</td>
          	<td><cfinput type="text" name="client_postcode" value="" size="10"></td>
       		<td><cfinput type="text" name="client_delpostcode" value="" size="10"></td>
        </tr>
        <tr>
          	<td class="formlabel">Unsubscribe</td>
          	<td colspan="2"><cfinput name="client_eunsubscribed" type="checkbox" value="1" />&nbsp;Tick here to remove this client from the mailing list</td>
        </tr>
        <tr>
          <td colspan="3">&nbsp;</td>
        </tr>
        <tr>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
          <td align="right"><input type="submit" value="Add Client"></td>
        </tr>
      </table>
      <cfinput type="hidden" name="MM_InsertRecord" value="form1">
    </cfform>
<div class="full_line"></div>
<h2>Existing Clients</h2>
<table cellspacing="10">
<cfoutput query="allclients">
	<tr>
		<td>#client_name#</td>
		<td>#contact_name#</td>
		<td>#client_telephone#</td>
		<td>#URLDecode(client_email)#</td>
		<td class="editlink"><a href="client_edit.cfm?ID=#ID#">edit</a></td>
		<td class="editlink"><a href="client_delete_check.cfm?ID=#ID#">delete</a></td>
	</tr>
</cfoutput>
</table>
    
	</div>
</div>
<div class="clearline"></div>
<!--- end container div --->
</body>
</html>
