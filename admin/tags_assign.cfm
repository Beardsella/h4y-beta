<cfif NOT IsDefined ("session.admininitials")>
	<cflocation url="index.cfm?passwordcheck=failed">
</cfif>
<cfset CurrentPage=GetFileFromPath(GetTemplatePath())>

<cfif IsDefined("FORM.MM_UpdateRecord") AND FORM.MM_UpdateRecord EQ "form1">
	
		<cfloop index="ListTags" list="#Form.tags#">
			<cfquery datasource="#application.dsn#">
			INSERT INTO product_tags (tag_id, product_id) VALUES(
 	 	<cfif IsDefined("ListTags") AND #ListTags# NEQ "">
    		#ListTags#
     	<cfelse>
   			 NULL
  		</cfif>
		,
    		#FORM.product_id#
 )
			</cfquery>
		</cfloop>
</cfif>

<cfquery name="currenttags" datasource="#application.dsn#">
SELECT A.id, A.tag_id, B.tag
FROM product_tags A INNER JOIN tags B ON A.tag_id = B.ID
WHERE A.product_id = #URL.id#
ORDER BY B.tag ASC
</cfquery>

<cfquery name="alltags" datasource="#application.dsn#">
SELECT ID, tag
FROM tags
<cfif currenttags.recordcount gt 0>
	WHERE id NOT IN (#valuelist(currenttags.tag_id)#)
</cfif>
ORDER BY tag ASC
</cfquery>

<cfquery name="getname" datasource="#application.dsn#" maxrows="1">
SELECT title FROM products WHERE id = #URL.id#
</cfquery>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"><html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/templates/HPIL_admin.dwt.cfm" codeOutsideHTMLIsLocked="false" -->
<head>
<cfif NOT IsDefined ("session.admininitials") OR session.admininitials eq 999>
	<cflocation url="index.cfm?passwordcheck=failed" addtoken="no">
</cfif>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<title>Website Admin</title>

<link href="../css/HPIL_admin.css" rel="stylesheet" type="text/css" media="all">


</head>

<body>
<div id="container">
	<div id="logo"><img src="../images/HPIL_logo_gif.gif" /></div>
 	<div class="clearline"></div>
	<div id="nav">
			<cfinclude template="nav_admin.cfm">	
	</div>
	<div id="pagecontent">
	  	<div id="pagetitle">
	  	
Assign tags to <cfoutput>#URLDecode(getname.title)#</cfoutput>
  
	  	</div>
<div class="clearline"></div>
		

<form action="<cfoutput>#CurrentPage#?id=#URL.id#</cfoutput>" method="post" enctype="multipart/form-data" name="form1" id="form1">
     <table width="50%" border="0">
          <tr>
            <td>
			<select name="tags" size="20" multiple="multiple">
			<cfoutput query="alltags">
              <option value="#alltags.ID#">#alltags.tag#</option>
            </cfoutput>
            </select>
			</td>
            <td>&nbsp;</td>
            <td valign="top">
<table width="80%" border="0">
  <tr>
    <td colspan="2" class="label">Currently assigned tags</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
<cfoutput query="currenttags">
  <tr>
    <td>#tag#</td>
    <td><a href="tag_unassign.cfm?id=#id#&prodid=#URL.id#">remove</a></td>
  </tr>
</cfoutput>
</table>
			</td>
          </tr>
		  <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
          </tr>
		<tr>
            <td colspan="3">
			</td>
        </tr>
		  <tr>
            <td colspan="2"><input name="submit" type="submit" value="Assign tags" /></td>
            <td>&nbsp;</td>
          </tr>
            <input type="hidden" name="MM_UpdateRecord" value="form1" />
            <input type="hidden" name="product_id" value="<cfoutput>#URL.id#</cfoutput>" />
      </table>
</form>

  
	</div>
</div>
<div class="clearline"></div>
<!--- end container div --->
</body>
</html>