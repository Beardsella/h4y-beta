<cfsetting requesttimeout="3600">

<cfset path = expandPath("../empty_me/")>

<!--- first a bit of housekeeping --->
<cfdirectory directory="#path#" action="list" name="currentDir">
  <cfloop query="currentDir">
<cftry>
	<cffile action="delete" file="#path##currentDir.Name#">
<cfcatch></cfcatch>
</cftry>
  </cfloop>

<!-- Bootstrap CSS-->
 <link href="../css/bootstrap.css" rel="stylesheet">  


<cfset CurrentPage=GetFileFromPath(GetTemplatePath())>
    
<cfset application.DSN = "hear4">


<cfquery datasource="#application.DSN#" name="SEO_page_update">
  SELECT *
  FROM products  
</cfquery>

<cfset update_count = 0>

<cfoutput query="SEO_page_update">

  <cfset update_count = #update_count# + 1>


  <cfset new_name_with_no_spaces = #REReplace('#URLDecode(title)#',"[^0-9A-Za-z &/]","","all")# >

  <cfset new_name_with_no_spaces = Replace('#new_name_with_no_spaces#', ' ', '-', 'All')>

  <cfset new_name_with_no_spaces = Replace('#new_name_with_no_spaces#', '/', '-', 'All')>

  <cfset new_name_with_no_spaces = Replace('#new_name_with_no_spaces#', '--', '', 'All')>

  <cfset new_name_with_no_spaces = Replace('#new_name_with_no_spaces#', '---', '', 'All')>

  <cfset new_name_with_no_spaces = Replace('#new_name_with_no_spaces#', '&', 'and', 'All')>

  <cfset new_name_with_no_spaces = "#new_name_with_no_spaces#" & ".cfm">

  <cfset new_name_with_no_spaces = Replace('#new_name_with_no_spaces#', '-.cfm', '.cfm', 'All')>

  <cfset new_name_with_no_spaces = "#LCase(new_name_with_no_spaces)#">


  <cfquery datasource="#application.DSN#">
    UPDATE products
    SET SEO_URL = "#new_name_with_no_spaces#"
    WHERE id = #id#

  </cfquery>

  
</cfoutput>

<cfquery datasource="#application.DSN#" name="SEO_page_update">
  SELECT *
  FROM products
</cfquery>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"><html xmlns="http://www.w3.org/1999/xhtml">

<head>
<cfif NOT IsDefined ("session.admininitials") OR session.admininitials eq 999>
	<cflocation url="index.cfm?passwordcheck=failed" addtoken="no">
</cfif>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<title>Administration</title>

<link href="../css/HPIL_admin.css" rel="stylesheet" type="text/css" media="all">


</head>

<body onLoad="MM_preloadImages('../images/delete_red.gif')">
<div id="container">
	<div id="logo"><img src="../images/HPIL_logo_gif.gif" /></div>
 	<div class="clearline"></div>
	<div id="nav">
			<cfinclude template="nav_admin.cfm">	
	</div>
	<div id="pagecontent"><h1><cfoutput>#update_count# #SEO_page_update.recordcount#
 <table>
 
<cfoutput query="SEO_page_update">

  <tr>
    <td>#id#</td>
    <td>#URLdecode(title)#</td>
    <td>#URLDecode(SEO_URL)#</td>
  </tr>

  </cfoutput>
</table>
</cfoutput></h1></div>
</div>
<!--- end container div --->
</body>
</html>