<cfquery name="getuploads" datasource="#application.dsn#">
SELECT ID, filename, filelabel
FROM uploads
WHERE product_id = #URL.ID#
ORDER BY ID ASC
</cfquery>
<cfquery name="prod" datasource="#application.dsn#">
SELECT title
FROM products
WHERE id = #URL.ID#
</cfquery>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/templates/HPIL_admin.dwt.cfm" codeOutsideHTMLIsLocked="false" -->
<head>
<cfif NOT IsDefined ("session.admininitials") OR session.admininitials eq 999>
	<cflocation url="index.cfm?passwordcheck=failed" addtoken="no">
</cfif>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<title>Hearing Aids and Speech Amplification</title>


<link href="../css/HPIL_admin.css" rel="stylesheet" type="text/css" media="all">


<script type="text/JavaScript">
<!--
function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}
//-->
</script>
</head>

<body onload="MM_preloadImages('../images/delete_red.gif')">
<div id="container">
	<div id="logo"><img src="../images/HPIL_logo_gif.gif" /></div>
 	<div class="clearline"></div>
	<div id="nav">
			<cfinclude template="nav_admin.cfm">	
	</div>
	<div id="pagecontent">
	  	<div id="pagetitle">
	  	
<title>Website Admin</title>
		<script type="text/javascript">
<!--
function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
//-->
</script>
		
	  	</div>
<div class="clearline"></div>
		

	 <h2><cfoutput><a href="edit_product.cfm?id=#URL.id#">Return to #URLDecode(prod.title)#</a></cfoutput></h2>
  <table width="80%">
        <tr>
          <td colspan="3" class="longtext">
          <p>Use this page to upload a PDF or other file that can be downloaded from the '<cfoutput>#URLDecode(prod.title)#</cfoutput> page.</p>
		  </td>
        </tr>
        <tr>
          <td colspan="3" class="longtext">&nbsp;</td>
        </tr>
<cfif getuploads.recordcount neq 0>
	<cfoutput query="getuploads">
        <tr>
          <td colspan="2" class="longtext">#URLDecode(fileName)#</td>
          <td colspan="2" class="longtext">#URLDecode(filelabel)#</td>
         <td class="longtext"><a href="upload_delete.cfm?ID=#ID#&prodid=#URL.id#"><img src="../images/delete.gif" alt="Delete file" name="delete_gif" width="58" height="5" border="0" id="delete_gif" onmouseover="MM_swapImage('delete_gif','','../images/delete_red.gif',1)" onmouseout="MM_swapImgRestore()" /></a></td>
        </tr>
	</cfoutput>
</cfif>
        <tr>
          <td colspan="3"><hr color="#CCCCCC" /></td>
        </tr>
        <tr>
          <td colspan="3">
           <form action="do_upload.cfm" method="post" enctype="multipart/form-data" name="form1" id="form1">
		   <table width="100%">
              <tr>
                <td>&nbsp;</td>
                <td colspan="2" class="formtext">&nbsp;</td>
              </tr>
              <tr>
                <td valign="top" class="formlabel">File:</td>
                <td colspan="2" class="formtext"><input name="newUpload" type="file" id="newUpload" size="50" /></td>
              </tr>
              <tr>
                <td valign="top" class="formlabel">Label:</td>
                <td colspan="2" class="formtext"><input name="filelabel" type="text" size="30" /></td>
              </tr>
              <tr>
                <td class="formlabel">&nbsp;</td>
                <td colspan="2" class="formtext"><input name="submit" type="submit" value="Upload file" /></td>
              </tr>
            </table>
            <input type="hidden" name="MM_UpdateRecord1" value="form1" />
            <input type="hidden" name="prodid" value="<cfoutput>#URL.id#</cfoutput>" />
          </form></td>
        </tr>
        <tr>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
        </tr>
      </table>		  
	</div>
</div>
<div class="clearline"></div>
<!--- end container div --->
</body>
</html>			
