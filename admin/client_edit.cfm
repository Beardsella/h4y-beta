<cfset CurrentPage=GetFileFromPath(GetTemplatePath())>
<cfif IsDefined("FORM.MM_InsertRecord") AND FORM.MM_InsertRecord EQ "form1">
  <cfquery datasource="#application.dsn#">
    UPDATE clients
    SET client_name =
  <cfif IsDefined("FORM.client_name") AND #FORM.client_name# NEQ "">
    '#FORM.client_name#'
      <cfelse>
    NULL
  </cfif>
   , contact_name =
  <cfif IsDefined("FORM.contact_name") AND #FORM.contact_name# NEQ "">
    '#FORM.contact_name#'
      <cfelse>
    NULL
  </cfif>
     , client_telephone =
  <cfif IsDefined("FORM.client_telephone") AND #FORM.client_telephone# NEQ "">
    '#FORM.client_telephone#'
      <cfelse>
    NULL
  </cfif>
     , client_email =
  <cfif IsDefined("FORM.client_email") AND #FORM.client_email# NEQ "">
    '#URLEncodedformat(FORM.client_email)#'
      <cfelse>
    NULL
  </cfif>
     , client_password =
  <cfif IsDefined("FORM.client_password") AND #FORM.client_password# NEQ "">
    '#FORM.client_password#'
      <cfelse>
    NULL
  </cfif>
     , client_add1 =
  <cfif IsDefined("FORM.client_add1") AND #FORM.client_add1# NEQ "">
    '#FORM.client_add1#'
      <cfelse>
    NULL
  </cfif>
      , client_add2 =
  <cfif IsDefined("FORM.client_add2") AND #FORM.client_add2# NEQ "">
    '#FORM.client_add2#'
      <cfelse>
    NULL
  </cfif>
     , client_add3 =
  <cfif IsDefined("FORM.client_add3") AND #FORM.client_add3# NEQ "">
    '#FORM.client_add3#'
      <cfelse>
    NULL
  </cfif>
      , client_county =
  <cfif IsDefined("FORM.client_county") AND #FORM.client_county# NEQ "">
    '#FORM.client_county#'
      <cfelse>
    NULL
  </cfif>
      , client_postcode =
  <cfif IsDefined("FORM.") AND #FORM.client_postcode# NEQ "">
    '#FORM.client_postcode#'
      <cfelse>
    NULL
  </cfif>
     , client_deladd1 =
  <cfif IsDefined("FORM.client_deladd1") AND #FORM.client_deladd1# NEQ "">
    '#FORM.client_deladd1#'
      <cfelse>
    NULL
  </cfif>
      , client_deladd2 =
  <cfif IsDefined("FORM.client_deladd2") AND #FORM.client_deladd2# NEQ "">
    '#FORM.client_deladd2#'
      <cfelse>
    NULL
  </cfif>
     , client_deladd3 =
  <cfif IsDefined("FORM.client_deladd3") AND #FORM.client_deladd3# NEQ "">
    '#FORM.client_deladd3#'
      <cfelse>
    NULL
  </cfif>
      , client_delcounty =
  <cfif IsDefined("FORM.client_delcounty") AND #FORM.client_delcounty# NEQ "">
    '#FORM.client_delcounty#'
      <cfelse>
    NULL
  </cfif>
      , client_delpostcode =
  <cfif IsDefined("FORM.client_delpostcode") AND #FORM.client_delpostcode# NEQ "">
    '#FORM.client_delpostcode#'
      <cfelse>
    NULL
  </cfif>
       , client_eunsubscribed =
  <cfif IsDefined("FORM.client_eunsubscribed") AND #FORM.client_eunsubscribed# EQ 1>
    1
      <cfelse>
    0
  </cfif>
     , client_notes =
  <cfif IsDefined("FORM.client_notes") AND #FORM.client_notes# NEQ "">
    '#URLEncodedformat(FORM.client_notes)#'
      <cfelse>
    NULL
  </cfif>
 	WHERE ID = #FORM.ID#
  </cfquery>
  
  <cflocation url="clients.cfm" addtoken="no">
</cfif>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/Templates/HPIL_admin.dwt.cfm" codeOutsideHTMLIsLocked="false" -->
<head>
<cfif NOT IsDefined ("session.admininitials") OR session.admininitials eq 999>
	<cflocation url="index.cfm?passwordcheck=failed" addtoken="no">
</cfif>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<title>Website admin</title>

<link href="../css/HPIL_admin.css" rel="stylesheet" type="text/css" media="all">


<cfquery name="client" datasource="#application.dsn#">
SELECT *
FROM clients
WHERE ID = #URL.ID#
</cfquery>
<cfquery name="allprods" datasource="#application.dsn#">
SELECT id, title
FROM products
ORDER BY title ASC
</cfquery>

</head>

<body>
<div id="container">
	<div id="logo"><img src="../images/HPIL_logo_gif.gif" /></div>
 	<div class="clearline"></div>
	<div id="nav">
			<cfinclude template="nav_admin.cfm">	
	</div>
	<div id="pagecontent">
	  	<div id="pagetitle">
	  	
		<h2><cfoutput>#client.client_name#</cfoutput></h2>	  
		
	  	</div>
<div class="clearline"></div>
		

<div class="gofloat">
    <h2>Select a product to set specific price breaks for this client</h2>
    <cfloop query="allprods">
        <cfquery name="price_breaks_client" datasource="#application.dsn#">
        SELECT bprice, break_qty FROM product_price_breaks WHERE bproduct_id = #allprods.id# AND client_id = #URL.ID#
        </cfquery>
    <div class="small_thumbs">
        <a href="price_breaks_client.cfm?pid=<cfoutput>#allprods.id#</cfoutput>&cid=<cfoutput>#URL.id#</cfoutput>" title="<cfoutput>#URLDecode(allprods.title)#</cfoutput>">
        <img src="../product-images/thumbs/<cfoutput>#ID#</cfoutput>.jpg" height="40" alt="" border="0" />
        </a>
        <br /><cfif price_breaks_client.recordcount><span style="color:#C30">Not Default</span><cfelse>As Default</cfif>
    </div>
    </cfloop>
</div>
<div class="full_line"></div>
    <cfform method="post" name="form1" action="#CurrentPage#" enctype="multipart/form-data">
      <table>
        <tr>
          	<td class="formlabel">Client:</td>
          	<td><cfinput type="text" name="client_name" value="#client.client_name#" size="30"></td>
       		<td>&nbsp;</td>
        </tr>
        <tr>
          	<td class="formlabel">Contact Name:</td>
          	<td><cfinput type="text" name="contact_name" value="#client.contact_name#" size="30"></td>
       		<td>&nbsp;</td>
        </tr>
        <tr>
          	<td class="formlabel">Telephone:</td>
          	<td><cfinput type="text" name="client_telephone" value="#client.client_telephone#" size="30"></td>
       		<td>&nbsp;</td>
        </tr>
        <tr>
          	<td class="formlabel">Email:</td>
          	<td><cfinput type="text" name="client_email" value="#URLDecode(client.client_email)#" size="30"></td>
       		<td>&nbsp;</td>
        </tr>
        <tr>
          	<td class="formlabel">Password:</td>
         	<td><cfinput type="text" name="client_password" value="#client.client_password#" size="20"></td>
       		<td>&nbsp;</td>
        </tr>
        <tr>
          	<td class="formlabel">Address</td>
         	<th>BILLING</th>
         	<th>DELIVERY</th>
        </tr>
        <tr>
          	<td>&nbsp;</td>
          	<td><cfinput type="text" name="client_add1" value="#client.client_add1#" size="30"></td>
       		<td><cfinput type="text" name="client_deladd1" value="#client.client_deladd1#" size="30"></td>
        </tr>
        <tr>
          	<td>&nbsp;</td>
          	<td><cfinput type="text" name="client_add2" value="#client.client_add2#" size="30"></td>
       		<td><cfinput type="text" name="client_deladd2" value="#client.client_deladd2#" size="30"></td>
        </tr>
        <tr>
          	<td>&nbsp;</td>
          	<td><cfinput type="text" name="client_add3" value="#client.client_add3#" size="30"></td>
       		<td><cfinput type="text" name="client_deladd3" value="#client.client_deladd3#" size="30"></td>
        </tr>
        <tr>
          	<td class="formlabel">County</td>
          	<td><cfinput type="text" name="client_county" value="#client.client_county#" size="30"></td>
       		<td><cfinput type="text" name="client_delcounty" value="#client.client_delcounty#" size="30"></td>
        </tr>
        <tr>
          	<td class="formlabel">Postcode</td>
          	<td><cfinput type="text" name="client_postcode" value="#client.client_postcode#" size="10"></td>
       		<td><cfinput type="text" name="client_delpostcode" value="#client.client_delpostcode#" size="10"></td>
        </tr>
        <tr>
          	<td class="formlabel">Unsubscribe</td>
          	<td colspan="2"><input name="client_eunsubscribed" type="checkbox" value="1" <cfif client.client_eunsubscribed eq 1>checked</cfif> /> &nbsp;Tick here to remove this client from the mailing list</td>
        </tr>
        <tr>
          	<td class="formlabel">Notes</td>
          	<td colspan="2">
            <textarea name="client_notes" width="400" height="300" richtext="true" toolbar="Basic">
            <cfoutput>#URLDecode(client.client_notes)#</cfoutput>
            </textarea>
            </td>
        </tr>
        <tr>
          <td colspan="3">&nbsp;</td>
        </tr>
        <tr>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
          <td align="right"><input type="submit" value="Update Client"></td>
        </tr>
      </table>
      <cfinput type="hidden" name="MM_InsertRecord" value="form1">
      <cfinput type="hidden" name="ID" value="#URL.ID#">
    </cfform>
    
	</div>
</div>
<div class="clearline"></div>
<!--- end container div --->
</body>
</html>
