<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/templates/HPIL_admin.dwt.cfm" codeOutsideHTMLIsLocked="false" -->
<head>
<cfif NOT IsDefined ("session.admininitials") OR session.admininitials eq 999>
	<cflocation url="index.cfm?passwordcheck=failed" addtoken="no">
</cfif>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<title>Website Admin</title>

<link href="../css/HPIL_admin.css" rel="stylesheet" type="text/css" media="all">


<cfquery name="listorders" datasource="#application.dsn#">
SELECT orderid
FROM ordersummary
ORDER BY orderid DESC
</cfquery>

</head>

<body>
<div id="container">
	<div id="logo"><img src="../images/HPIL_logo_gif.gif" /></div>
 	<div class="clearline"></div>
	<div id="nav">
			<cfinclude template="nav_admin.cfm">	
	</div>
	<div id="pagecontent">
	  	<div id="pagetitle">
	  	
		Customer Orders
		
	  	</div>
<div class="clearline"></div>
		

<p>Orders that have a line through them are UNPAID</p>
<table width="100%" border="0">
  <tr>
    <td>&nbsp;</td>
    <td class="label">Date</td>
    <td class="label">Name</td>
    <td class="formlabel">Value</td>
    <td>&nbsp;</td>
  </tr>
<cfloop query="listorders"> 
<cfquery name="getdetail" datasource="#application.dsn#">
SELECT A.orderid, A.ordervalue, A.deliverycost, A.orderdate, A.orderstatus, B.firstname, B.lastname
FROM ordersummary A INNER JOIN customerinfo B ON A.customerid = B.custid
WHERE A.orderid = #listorders.orderid#
</cfquery>
  <tr>
    <td class="longtext">
    <cfif IsDefined("getdetail.orderstatus") AND getdetail.orderstatus eq 1>
            <span class="strike">
            <cfoutput>#getdetail.orderid#</cfoutput>
            </span>
    <cfelseif IsDefined("getdetail.orderstatus") AND getdetail.orderstatus eq 3>
            <span class="complete">
            <cfoutput>#getdetail.orderid#</cfoutput>
            </span>
    <cfelse>
            <cfoutput>#getdetail.orderid#</cfoutput>
    </cfif>
	</td>
    <td class="longtext">
    <cfif IsDefined("getdetail.orderstatus") AND getdetail.orderstatus eq 1>
            <span class="strike">
            <cfoutput>#dateformat(getdetail.orderdate, 'dd/mm/yy')#</cfoutput>
            </span>
    <cfelseif IsDefined("getdetail.orderstatus") AND getdetail.orderstatus eq 3>
            <span class="complete">
            <cfoutput>#dateformat(getdetail.orderdate, 'dd/mm/yy')#</cfoutput>
            </span>
    <cfelse>
            <cfoutput>#dateformat(getdetail.orderdate, 'dd/mm/yy')#</cfoutput>
    </cfif>
	</td>
    <td class="longtext">
    <cfif IsDefined("getdetail.orderstatus") AND getdetail.orderstatus eq 1>
            <span class="strike">
            <cfoutput>#getdetail.firstname# #getdetail.lastname#</cfoutput>
            </span>
    <cfelseif IsDefined("getdetail.orderstatus") AND getdetail.orderstatus eq 3>
            <span class="complete">
            <cfoutput>#getdetail.firstname# #getdetail.lastname#</cfoutput>
            </span>
    <cfelse>
            <cfoutput>#getdetail.firstname# #getdetail.lastname#</cfoutput>
    </cfif>
	</td>
    <td class="money">
    <cfif IsDefined("getdetail.orderstatus") AND getdetail.orderstatus eq 1>
            <span class="strike">
            <cfoutput>#numberformat(getdetail.ordervalue + getdetail.deliverycost, '.00')#</cfoutput>
            </span>
    <cfelseif IsDefined("getdetail.orderstatus") AND getdetail.orderstatus eq 3>
            <span class="complete">
            <cfoutput>#numberformat(getdetail.ordervalue + getdetail.deliverycost, '.00')#</cfoutput>
            </span>
    <cfelse>
            <cfoutput>#numberformat(getdetail.ordervalue + getdetail.deliverycost, '.00')#</cfoutput>
    </cfif>
	</td>
    <td class="longtext">
	<a href="order_detail.cfm?id=<cfoutput>#getdetail.orderid#</cfoutput>">view</a>
	</td>
  </tr>
</cfloop>
  <tr>  
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
</table>


	</div>
</div>
<div class="clearline"></div>
<!--- end container div --->
</body>
</html>
