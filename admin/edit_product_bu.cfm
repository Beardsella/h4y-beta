<cfsetting requesttimeout="3600">

<cfset CurrentPage=GetFileFromPath(GetTemplatePath())>
<cfset path = expandPath("../empty_me/")>
<cfset final_path = expandPath("../product-images/")>
<!--- first a bit of housekeeping --->
<cfdirectory directory="#path#" action="list" name="currentDir">
  <cfloop query="currentDir">
<cftry>
	<cffile action="delete" file="#path##currentDir.Name#">
<cfcatch></cfcatch>
</cftry>
  </cfloop>

<cfif IsDefined("FORM.MM_UpdateRecord") AND FORM.MM_UpdateRecord EQ "form1">
<!--- Upload image and resize, make a thumbnail too --->
	<cfif IsDefined("FORM.newImage") AND #FORM.newImage# NEQ "">
		
			<cfset accept_upload = 1>
        
            <!--- only allow these mime types. Edit as required --->
            <cffile action="upload" filefield="newImage" destination="#GetTempDirectory()#" nameconflict="overwrite" result="uploadResult" accept="image/jpg, image/bmp, image/gif, image/jpeg">
            
            <!--- allow images only if needed --->
            <cfif Not IsImageFile(getTempDirectory() & uploadResult.serverFile)>
                <cffile action="delete" file="#getTempDirectory()##uploadResult.serverFile#">
                <cfset accept_upload = 0>
            </cfif>
            
            <!--- only allow these file extensions. Edit as required --->
            <cfif NOT ListFindNoCase("bmp,gif,jpg,jpeg,png", uploadResult.ServerFileExt)>
                <cffile action="delete" file="#getTempDirectory()##uploadResult.serverFile#">
            
                <cfset display_error = 'This file type cannot currently be uploaded. Please contact PQL.'>
                <cfset accept_upload = 0>
            
            </cfif>
            
            <cfif accept_upload eq 1>
            
                    <!--- IMAGES: move the file. #path# is normally already set in the page header---> 
                    <cfimage action="WRITE" source="#getTempDirectory()##uploadResult.serverFile#" destination="#final_path##uploadResult.serverFile#" overwrite="no">
                            
            </cfif>
                  
            <!--- delete the temp file in all cases --->
            <cffile action="delete" file="#getTempDirectory()##uploadResult.serverFile#">
        
		<cfimage source="#path##uploadResult.serverFile#" name="lpic">		

<!--- make thumbnail --->
		<cfset ImageScaleToFit(lpic,200,200)>

<cfimage
    action = "write"
    destination = "#final_path#thumbs/#uploadResult.serverFile#"
    source = "#lpic#"
    overwrite = "no">

<cfquery datasource="#application.DSN#">
INSERT INTO product_images (product_id, image_name)
VALUES ("#URL.id#", "#uploadResult.serverFile#");
</cfquery>


</cfif>

<cfif isDefined("FORM.SEO_URL")>
  
   <cfset formatted_seo_url = Replace('#FORM.SEO_URL#', ' ', '-', 'All')>

<cfquery datasource="#application.dsn#" name="seo_name_test">
  SELECT * 
  FROM products
  WHERE id = #URL.id#
</cfquery>

   <cfset current_seo_url = "#seo_name_test.seo_url#">

<cfif isDefined("seo_name_test.seo_url") AND #seo_name_test.seo_url# neq "#current_seo_url#">
  
<cfoutput query="seo_name_test">
<!---     <cffile action = "rename"
    destination="#seo_url#"
      source = "test.cfm"
      nameconflict="overwrite">
 ---></cfoutput>

</cfif>

</cfif>

  <cfquery datasource="#application.dsn#">
    UPDATE products SET title=
  <cfif IsDefined("FORM.title") AND #FORM.title# NEQ "">
    '#URLEncodedFormat(FORM.title)#'
      <cfelse>
    NULL
  </cfif>
	, supplier_code=
  <cfif IsDefined("FORM.supplier_code") AND #FORM.supplier_code# NEQ "">
    '#FORM.supplier_code#'
      <cfelse>
    NULL
  </cfif>
    , display_order=
  <cfif IsDefined("FORM.display_order") AND #FORM.display_order# NEQ "">
    #FORM.display_order#
      <cfelse>
    NULL
  </cfif>
    , Description=
  <cfif IsDefined("FORM.Description") AND #FORM.Description# NEQ "">
    '#FORM.Description#'
      <cfelse>
    NULL
  </cfif>
    , SEO_URL=
  <cfif IsDefined("FORM.Description") AND #FORM.Description# NEQ "">
    '#formatted_seo_url#'
      <cfelse>
    NULL
  </cfif>
     , technical=
  <cfif IsDefined("FORM.technical") AND #FORM.technical# NEQ "">
    '#FORM.technical#'
      <cfelse>
    NULL
  </cfif>
   , displaythis=
  <cfif IsDefined("FORM.displaythis")>
    1
      <cfelse>
    0
  </cfif>
   , in_stock=
  <cfif IsDefined("FORM.in_stock")>
    1
      <cfelse>
    0
  </cfif>
   , vatexempt=
  <cfif IsDefined("FORM.vatexempt")>
    1
      <cfelse>
    0
  </cfif>
   , featured=
  <cfif IsDefined("FORM.featured")>
    1
      <cfelse>
    0
  </cfif>
    , price=
  <cfif IsDefined("FORM.price") AND #FORM.price# NEQ "">
    #FORM.price#
      <cfelse>
    NULL
  </cfif>
    , price_was=
  <cfif IsDefined("FORM.price_was") AND #FORM.price_was# NEQ "">
    #FORM.price_was#
      <cfelse>
    NULL
  </cfif>
    , delivery=
  <cfif IsDefined("FORM.delivery") AND #FORM.delivery# NEQ "">
    #FORM.delivery#
      <cfelse>
    NULL
  </cfif>
    , product_options=
  <cfif IsDefined("FORM.product_options") AND #FORM.product_options# NEQ "">
    '#URLEncodedFormat(FORM.product_options)#'
      <cfelse>
    NULL
  </cfif>
  , prelated=
  <cfif IsDefined("FORM.prelated") AND #FORM.prelated# NEQ "">
    '#FORM.prelated#'
      <cfelse>
    NULL
  </cfif>
    , thumb_description=
  <cfif IsDefined("FORM.new_thumb_description") AND #FORM.new_thumb_description# NEQ "">
    '#URLEncodedFormat(FORM.new_thumb_description)#'
      <cfelse>
    NULL
  </cfif>
    WHERE ID=#FORM.ID#
  </cfquery>

<cfquery datasource="#application.dsn#" name="seo_name_test">
  SELECT * 
  FROM products
  WHERE id = #URL.id#
</cfquery>

<cfif isDefined("seo_name_test.seo_url") AND #seo_name_test.seo_url# neq "#current_seo_url#">
  
<cflocation url="products.cfm?winner" addtoken="no">

<cfoutput query="seo_name_test">
    <cffile action = "rename"
    destination="../../Hear4You-14/#seo_url#"
      source = "test.cfm"
      nameconflict="overwrite">
</cfoutput>

</cfif>


<cflocation url="products.cfm" addtoken="no">

</cfif>

<cfquery name="getprod" datasource="#application.dsn#">
SELECT * FROM products WHERE ID = #URL.id#
</cfquery>
<!--- get any child products --->
<cfquery name="children" datasource="#application.dsn#">
SELECT id, child_title, child_price FROM child_products WHERE product_id = #URL.id#
</cfquery>

<!--- get the cats this product is in --->
<cfquery name="getcats" datasource="#application.dsn#">
SELECT A.id, B.cat_name, B.parentid
FROM product_cats A INNER JOIN categories B ON A.category_id = B.id
WHERE A.product_id = #URL.id#
ORDER BY A.id ASC
</cfquery>
<!--- get the downloads associated with this product --->
<cfquery name="getuploads" datasource="#application.dsn#">
SELECT ID, filename, filelabel
FROM uploads
WHERE product_id = #URL.id#
ORDER BY ID ASC
</cfquery>
<!--- get the price breaks for social service etc clients --->
<cfquery name="getbreaks" datasource="#application.dsn#">
SELECT ID, bprice, break_qty
FROM product_price_breaks
WHERE bproduct_id = #URL.ID#
ORDER BY break_qty ASC
</cfquery>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/templates/HPIL_admin.dwt.cfm" codeOutsideHTMLIsLocked="false" -->
<head>
<cfif NOT IsDefined ("session.admininitials") OR session.admininitials eq 999>
	<cflocation url="index.cfm?passwordcheck=failed" addtoken="no">
</cfif>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<title>Hearing Aids and Speech Amplification</title>


<link href="../css/HPIL_admin.css" rel="stylesheet" type="text/css" media="all">


<!--- get existing images --->
<cfquery name="getpics" datasource="#application.dsn#">
SELECT id, image_name, image_caption FROM product_images WHERE product_id = #url.id#
</cfquery>



<!-- Place inside the <head> of your HTML -->
<script type="text/javascript" src="../js/tinymce/tinymce.min.js"></script>
<script type="text/javascript">
tinymce.init({
    selector: "textarea"
 });
</script>



</head>

<body>
<div id="container">
	<div id="logo"><img src="../images/HPIL_logo_gif.gif" /></div>
 	<div class="clearline"></div>
	<div id="nav">
			<cfinclude template="nav_admin.cfm">	
	</div>
	<div id="pagecontent">
	  	<div id="pagetitle">
	  	Edit <cfoutput>#URLDecode(getprod.title)#</cfoutput>
	  	</div>
<div class="clearline"></div>
		

<div class="full_line"></div>
<table width="500" border="0">
  <tr>
    <td colspan="2"><a href="categories_assign.cfm?id=<cfoutput>#URL.id#</cfoutput>">UPDATE CATEGORIES</a></td>
  </tr>
<cfoutput query="getcats">
  <tr>
    <td>#cat_name#</td>
    <td>&nbsp;</td>
  </tr>
</cfoutput>
	<tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
</table>
<div class="full_line"></div>
<table width="500" border="0">
  <tr>
    <td colspan="2"><a href="children_edit.cfm?id=<cfoutput>#URL.id#</cfoutput>">UPDATE CHILD PRODUCTS AND SET PRICE BREAKS</a></td>
  </tr>
<cfoutput query="children">
  <tr>
    <td>#child_title#</td>
    <td>#numberformat(child_price, '.00')#</td>
  </tr>
</cfoutput>
	<tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
</table>
<div class="full_line"></div>
<table width="500" border="0">
  <tr>
    <td colspan="2"><a href="uploads_edit.cfm?id=<cfoutput>#URL.id#</cfoutput>">UPLOAD DOCUMENTS</a></td>
  </tr>
<cfoutput query="getuploads">
  <tr>
    <td>#filename#</td>
    <td>#URLDecode(filelabel)#</td>
  </tr>
</cfoutput>
	<tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
</table>
<div class="full_line"></div>
<p><a href="images_additional.cfm?id=<cfoutput>#URL.id#</cfoutput>">UPDATE ADDITIONAL IMAGES</a></p>
    <cfloop query="getpics">
        <div class="gofloat_nowidth">
                    <img src="../product-images/thumbs/<cfoutput>#image_name#</cfoutput>">
                  <br /><cfoutput>#image_caption#</cfoutput>
        </div>
    </cfloop>
<div class="clearline"></div>
<cfif children.recordcount eq 0><!---price breaks for product no kids --->
        <div class="full_line"></div>
        <table width="500" border="0">
          <tr>
            <td colspan="2"><a href="price_breaks.cfm?id=<cfoutput>#URL.id#</cfoutput>">DEFAULT PRODUCT PRICE BREAKS</a></td>
          </tr>
        <cfoutput query="getbreaks">
          <tr>
            <td>#break_qty#</td>
            <td>#numberformat(bprice, '.00')#</td>
          </tr>
        </cfoutput>
            <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
          </tr>
        </table>
</cfif>
<div class="full_line"></div>
<div class="gofloat">
<form method="post" name="form1" action="#CurrentPage#?id=#URL.id#" enctype="multipart/form-data">
  <table>
     <tr>
      <td colspan="2">&nbsp;</td>
    </tr>
   <tr>
      <td class="formlabel">In Stock:</td>
      <td><input type="checkbox" name="in_stock" value=""  <cfif (getprod.in_stock EQ 1)>checked</cfif>></td>
    </tr>
   <tr>
      <td class="formlabel">Display this:</td>
      <td><input type="checkbox" name="displaythis" value=""  <cfif (getprod.displaythis EQ 1)>checked</cfif>></td>
    </tr>
   <tr>
      <td class="formlabel">Vat exempt ?</td>
      <td>
      <input type="checkbox" name="vatexempt" value=""  <cfif (getprod.vatexempt EQ 1)>checked</cfif>>
      tick if this can be vat exempt
      </td>
    </tr>
   <tr>
      <td class="formlabel">Feature this:</td>
      <td><input type="checkbox" name="featured" value=""  <cfif (getprod.featured EQ 1)>checked</cfif>></td>
    </tr>
    <tr>
      <td class="formlabel">Display order:</td>
      <td><input type="text" name="display_order" value="<cfoutput>#getprod.display_order#</cfoutput>" size="6"></td>
    </tr>
     <tr>
      <td class="formlabel">Supplier code:</td>
      <td><input type="text" name="supplier_code" value="<cfoutput>#getprod.supplier_code#</cfoutput>" size="40"></td>
    </tr>
   <tr>
      <td class="formlabel">Title:</td>
      <td><input type="text" name="title" value="<cfoutput>#URLDecode(getprod.title)#</cfoutput>" size="70"></td>
    </tr>
   <tr>
      <td class="formlabel">SEO URL:</td>
      <td><input type="text" name="seo_url" value="<cfoutput>#URLDecode(getprod.seo_url)#</cfoutput>" size="80">.cfm</td>
    </tr>
    <tr>
      <td class="formlabel">Description:</td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td colspan="2"><textarea name="Description" width="600" height="400" richtext="true" toolbar="pql" fontsizes="1/1,2/2,3/3,4/4,5/5,6/6,7/7"><cfoutput>#getprod.Description#</cfoutput></textarea>
      </td>
    </tr>
    <tr>
      <td class="formlabel">Technical spec:</td>
      <td>Enter the item label, then a pipe (|), then the text. One item per line.</td>
    </tr>
    <tr>
      <td colspan="2"><textarea name="technical" width="500" height="400" richtext="true" toolbar="pql" fontsizes="1/1,2/2,3/3,4/4"><cfoutput>#getprod.technical#</cfoutput></textarea>
      </td>
    </tr>
    <tr>
      <td class="formlabel">Price:</td>
      <td><input type="text" name="price" value="<cfoutput>#getprod.price#</cfoutput>" size="6"></td>
    </tr>
    <tr>
      <td class="formlabel">Price was:</td>
      <td><input type="text" name="price_was" value="<cfoutput>#getprod.price_was#</cfoutput>" size="6">&nbsp;optional field, displays in red next to price</td>
    </tr>
    <tr>
      <td class="formlabel">UK Delivery:</td>
      <td><input type="text" name="delivery" value="<cfoutput>#getprod.delivery#</cfoutput>" size="6"></td>
    </tr>
    <tr>
      <td class="formlabel">Choices:<br />With a comma after<br />all but the last</td>
      <td><textarea name="product_options" cols="50" rows="3"><cfoutput>#URLDecode(getprod.product_options)#</cfoutput></textarea></td>
    </tr>
    <tr>
        <cfquery datasource="#application.DSN#" name="prod_pictures">
    SELECT *
    FROM product_images
    WHERE product_id = #URL.id#
  </cfquery>
<cfoutput>
      <td colspan="2"><img src="../product-images/thumbs/#prod_pictures.image_name#" height="90" alt="Current Image" /></cfoutput></td>
    </tr>
    <tr>
      <td class="formlabel">Upload Photo:</td>
      <td><input name="newImage" type="file" /></td>
    </tr>
    <tr>
      <td colspan="2">&nbsp;</td>
    </tr>
    <tr>
     <td class="formlabel">Related product IDs:</td>
     <td><input type="text" name="prelated" value="<cfoutput>#getprod.prelated#</cfoutput>" size="30" />&nbsp;Comma separated</td>
    </tr>
    <tr>
     <td class="formlabel">Thumbnail description:</td>
     <td><textarea type="text" name="new_thumb_description" id="new_thumb_description" cols="45" row="100"><cfoutput>#URLDecode(getprod.thumb_description)#</cfoutput></textarea></td>
    </tr>
    <tr>
      <td colspan="2">&nbsp;</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td><input type="submit" value="Update product"></td>
    </tr>
  </table>
  <input type="hidden" name="ID" value="<cfoutput>#getprod.ID#</cfoutput>">
  <input type="hidden" name="MM_UpdateRecord" value="form1">
</form>
</div>

	</div>
</div>
<div class="clearline"></div>
<!--- end container div --->
</body>
</html>
