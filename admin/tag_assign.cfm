<cfsetting requesttimeout="3600">

<cfset CurrentPage=GetFileFromPath(GetTemplatePath())>
<cfset path = expandPath("../empty_me/")>
<cfset final_path = expandPath("../product-images/")>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/templates/HPIL_admin.dwt.cfm" codeOutsideHTMLIsLocked="false" -->
<head>
<cfif NOT IsDefined ("session.admininitials") OR session.admininitials eq 999>
	<cflocation url="index.cfm?passwordcheck=failed" addtoken="no">
</cfif>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<title>Hearing Aids and Speech Amplification</title>


<link href="../css/HPIL_admin.css" rel="stylesheet" type="text/css" media="all">

<!-- Bootstrap CSS-->
 <link href="../css/bootstrap.css" rel="stylesheet">  

<script type="text/javascript" src="../js/tinymce/tinymce.min.js"></script>
<script type="text/javascript">
tinyMCE.init({
    mode : "textareas",
    editor_selector : "mytextarea"
});
</script>

<cfif isDefined("form.update") AND FORM.update NEQ "">
  
  <cfquery datasource="#application.DSN#">
    INSERT INTO tags (tag)
    VALUES ("#urlEncodedFormat(FORM.new_tag)#")
  </cfquery>

  <cfquery datasource="#application.dsn#" Name="check_new_tag">
    SELECT MAX(id) AS last_id
    FROM tags
  </cfquery>

  <cfif isDefined("URL.id") AND URL.ID NEQ "">

  <cfquery datasource="#application.DSN#">
    INSERT INTO products_vs_tags (tag_id, prod_id)
    VALUES ("#check_new_tag.last_id#", "#FORM.prod_ID#")
  </cfquery>

  </cfif>

<cftry><cfset this_page_prod_id = #FORM.prod_ID#>
<cfcatch></cfcatch></cftry>

<cfelseif isDefined("form.update2") AND FORM.update2 NEQ "">



  <cfquery datasource="#application.dsn#" Name="check_new_tag">
    SELECT *
    FROM tags
    WHERE id 
  </cfquery>

  <cfif isDefined("URL.id") AND URL.ID NEQ "">

  <cfquery datasource="#application.DSN#">
    INSERT INTO products_vs_tags (tag_id, prod_id)
    VALUES ("#check_new_tag.last_id#", "#FORM.prod_ID#")
  </cfquery>
  <cfset this_page_prod_id = #FORM.prod_ID#>

<cfelse>


</cfif>
</cfif>

  <cfif isDefined("URL.id") AND URL.ID NEQ "">

    <cfquery datasource="#application.dsn#" Name="find_tags">
      SELECT *
      FROM tags
      JOIN products_vs_tags
      ON products_vs_tags.tag_id = tags.id
      WHERE products_vs_tags.prod_id = #this_page_prod_id#
    </cfquery>

    <cfquery datasource="#application.dsn#" Name="prod_title">
      SELECT ID, title
      FROM products
      WHERE id = #this_page_prod_id#
    </cfquery>

</cfif>

<cfquery datasource="#application.dsn#" Name="all_tags">
  SELECT *
  FROM tags
  ORDER BY tag
</cfquery>

</head>

<body>
<div id="container">
	<div id="logo"><img src="../images/HPIL_logo_gif.gif" /></div>
 	<div class="clearline"></div>
	<div id="nav">
			<cfinclude template="nav_admin.cfm">	
	</div>
	<div id="pagecontent">

  <cfif isDefined("URL.id") AND URL.ID NEQ "">

  <cfoutput>
      <h3><span class="badge">#find_tags.recordcount#</span> tags assigned to "<a href="edit_product.cfm?id=#prod_title.id#" alt="back to editing page">#URLDecode(prod_title.title)#</a>"</h3>
  </cfoutput>
</cfif>

  <div class="">

    <cfif isDefined("URL.id") AND URL.ID NEQ "">

      <cfoutput><form action="tag_assign.cfm?id=#URL.id#" method="post" name="new_tag_submit" enctype="multipart/form-data"></cfoutput>
    
    <cfelse>

      <cfoutput><form action="tag_assign.cfm" method="post" name="new_tag_submit" enctype="multipart/form-data"></cfoutput>

    </cfif>


    <input type="text" name="new_tag" Placeholder="New Tag">

    <input type="submit" class="btn btn-primary" value="Create New Tag">
      <cfif isDefined("URL.id") AND URL.ID NEQ "">

  <input type="hidden" name="prod_ID" value="<cfoutput>#this_page_prod_id#</cfoutput>">
  </cfif>
  <input type="hidden" name="update" value="new_tag_submit">

    </form>

  </div>

<div class="clearline full_line"></div>

  <cfif isDefined("URL.id") AND URL.ID NEQ "">

  <div class="width50"><cfoutput>
    <form action="tag_assign.cfm?id=#URL.id#" method="post" name="tag_link" enctype="multipart/form-data"></cfoutput>

    <cfset acutal_tag_count = 0 >

      <cfoutput query="all_tags">

      <cfset acutal_tag_count =  #acutal_tag_count# +1 >

        <cfquery datasource="#application.dsn#" Name="this_tag">
          SELECT *
          FROM products_vs_tags
          WHERE products_vs_tags.tag_id = #id# AND products_vs_tags.prod_id = #this_page_prod_id#
        </cfquery>

        <cfif isDefined("this_tag.recordcount") AND #this_tag.recordcount# GTE 1>
                      
          <p><input type="checkbox" name="#acutal_tag_count#" value="1" checked> #URLDecode(tag)#  <a href="tag_delete.cfm?id=#id#"><span class="glyphicon glyphicon-remove"></span></a></p><br>

          <cfelse>

          <p><input type="checkbox" name="#acutal_tag_count#" value="0"> #URLDecode(tag)#  <a href="tag_delete.cfm?id=#id#"><span class="glyphicon glyphicon-remove"></a></span></p><br>

        </cfif>

      </cfoutput>
      
      <cfoutput>
        <input type="hidden" name="nunber_of_tags" value="#acutal_tag_count#">
        <input type="hidden" name="prod_ID" value="#this_page_prod_id#">
        <input type="hidden" name="update2" value="new_tag_submit">
        <input type="submit" class="btn btn-primary btn-block" value="Update linked Tags">
      </cfoutput>

    </form>

  </div>

  <cfelse>

    <div class="width50">

    <cfoutput><h2>All Tags <span class="badge">#all_tags.recordcount#</span></h2></a></cfoutput>

          <cfoutput query="all_tags">

              <p>#URLDecode(tag)#  <a href="tag_delete.cfm?id=#id#"><span class="glyphicon glyphicon-remove"></span></a></p><br>

          </cfoutput>
  </div>

  </cfif>

</div>
<div class="clearline"></div>
<!--- end container div --->
</div>

</body>
</html>
