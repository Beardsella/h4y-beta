<cfif NOT IsDefined ("session.admininitials")>
<cflocation url="index.cfm?passwordcheck=failed">
</cfif>

<cfquery name="gettag" datasource="#application.dsn#">
SELECT B.title, C.tag
FROM (product_tags A INNER JOIN products B ON A.product_id = B.id) INNER JOIN tags C ON A.tag_id = C.id
WHERE A.tag_id = #URL.id#
</cfquery>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/templates/HPIL_admin.dwt.cfm" codeOutsideHTMLIsLocked="false" -->
<head>
<cfif NOT IsDefined ("session.admininitials") OR session.admininitials eq 999>
	<cflocation url="index.cfm?passwordcheck=failed" addtoken="no">
</cfif>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<title>Website Admin</title>

<link href="../css/HPIL_admin.css" rel="stylesheet" type="text/css" media="all">


</head>

<body>
<div id="container">
	<div id="logo"><img src="../images/HPIL_logo_gif.gif" /></div>
 	<div class="clearline"></div>
	<div id="nav">
			<cfinclude template="nav_admin.cfm">	
	</div>
	<div id="pagecontent">
	  	<div id="pagetitle">
	  	Deleting a
  TAG cannot be undone 
	  	</div>
<div class="clearline"></div>
		

    <table width="90%" border="0">
      <tr>
        <td width="60%">You are about to delete the tag <cfoutput>#gettag.tag#</cfoutput> from the database.<br /><br />There are <cfoutput>#gettag.recordcount#</cfoutput> products assigned to that tag.
		<br /><br />Click delete to confirm, or cancel to return to the tag page.
		</td>
        <td width="8%"><a href="tags_add.cfm">cancel</a></td>
        <td width="9%"><a href="tag_delete.cfm?tagid=<cfoutput>#URL.id#</cfoutput>">delete</a></td>
        <td width="23%">&nbsp;</td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
      </tr>
    </table>
  
	</div>
</div>
<div class="clearline"></div>
<!--- end container div --->
</body>
</html>
