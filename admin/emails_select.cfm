<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/Templates/HPIL_admin.dwt.cfm" codeOutsideHTMLIsLocked="false" -->
<head>
<cfif NOT IsDefined ("session.admininitials") OR session.admininitials eq 999>
	<cflocation url="index.cfm?passwordcheck=failed" addtoken="no">
</cfif>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<title>Web admin</title>

<link href="../css/HPIL_admin.css" rel="stylesheet" type="text/css" media="all">


<!--- reset all tables to yes --->
<cfquery datasource="#application.dsn#">
UPDATE customerinfo
SET eselect = 1
</cfquery>
<cfquery datasource="#application.dsn#">
UPDATE mailing_list
SET eselect = 1
</cfquery>
<cfquery datasource="#application.dsn#">
UPDATE clients
SET eselect = 1
</cfquery>

<!--- get customers who have bought --->
<cfquery name="getcustomers" datasource="#application.dsn#">
SELECT custid, email
FROM customerinfo
WHERE eunsubscribed = 0
ORDER BY custid ASC
</cfquery>
<!--- get mailing list subscribers --->
<cfquery name="getmailers" datasource="#application.dsn#">
SELECT ID, memail
FROM mailing_list
WHERE eunsubscribed = 0
ORDER BY ID ASC
</cfquery>
<!--- get clients (local authorities etc) --->
<cfquery name="getclients" datasource="#application.dsn#">
SELECT ID, client_email
FROM clients
WHERE client_eunsubscribed = 0
ORDER BY ID ASC
</cfquery>
<cfquery name="allmessages" datasource="#application.dsn#">
SELECT ID, esummary
FROM email_messages
ORDER BY esummary ASC
</cfquery>
<script language="javascript" src="../javascripts/engine.js"></script>
<script language="javascript" src="../javascripts/prototype.js"></script>
<script language="javascript" src="../javascripts/scriptaculous.js"></script>
<script language="javascript" src="../javascripts/library.js"></script>
<script type="text/JavaScript">
<!--
function updateSelection(thistable){
	param = new Object();
	param.thistable = thistable
	http( 'POST'  , 'aj_emails_select.cfm' , email_response, param ); 
}
function email_response(obj){ 
	var showbutton = obj.showbutton
	if (showbutton == 1) {
		document.getElementById('ebutton').style.display = ''
	}
	else {
		document.getElementById('ebutton').style.display = 'none'
	}
	document.getElementById('ecount').innerHTML = obj.ecount
}
function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
//-->
</script>

</head>

<body onload="MM_preloadImages('../images/send_emails_over.gif')">
<div id="container">
	<div id="logo"><img src="../images/HPIL_logo_gif.gif" /></div>
 	<div class="clearline"></div>
	<div id="nav">
			<cfinclude template="nav_admin.cfm">	
	</div>
	<div id="pagecontent">
	  	<div id="pagetitle">
	  	
		<h2>Prepare email broadcast</h2>	  
	  
	  	</div>
<div class="clearline"></div>
		

<div class="gofloat">
<form action="emails_send_now.cfm" method="post" enctype="multipart/form-data" id="emails_send" name="emails_send">
<table cellspacing="20">
	<tr>
		<td>
<h3>Select message to send</h3>
		</td>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td>

<select name="message" id="message">
    <option value="">Select ...</option>
  <cfoutput query="allmessages">
    <option value="#ID#">#esummary#</option>
  </cfoutput>
</select>
		</td>
		<td>
<span id="ecount"><cfoutput>#evaluate(getcustomers.recordcount + getclients.recordcount + getmailers.recordcount)#</cfoutput></span>
		</td>
		<td>
<span id="ebutton"><a href="#" onclick="validate_message('message','emails_send')"><img src="../images/send_emails_up.gif" alt="Send now" name="sendnow" id="sendnow" onmouseover="MM_swapImage('sendnow','','../images/send_emails_over.gif',1)" onmouseout="MM_swapImgRestore()" border="0" /></a></span>
		</td>
	</tr>
</table>
</form>
</div>
<div class="greyline"></div>
<div class="gofloat">
<h3>Select groups to send to</h3>
<table width="30%" border="0" cellspacing="8">
  <tr>
    <td>Ecommerce customers</td>
    <td><cfoutput>#getcustomers.recordcount#</cfoutput></td>
    <td><input name="customers" type="checkbox" value="1" checked onclick="updateSelection('customerinfo')" /></td>
  </tr>
  <tr>
    <td>Business clients</td>
    <td><cfoutput>#getclients.recordcount#</cfoutput></td>
    <td><input name="clients" type="checkbox" value="1" checked onclick="updateSelection('clients')" /></td>
  </tr>
  <tr>
    <td>Mailing list subscribers</td>
    <td><cfoutput>#getmailers.recordcount#</cfoutput></td>
    <td><input name="mailers" type="checkbox" value="1" checked onclick="updateSelection('mailing_list')" /></td>
  </tr>
</table>

</div>

    
	</div>
</div>
<div class="clearline"></div>
<!--- end container div --->
</body>
</html>
