<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/Templates/HPIL_admin.dwt.cfm" codeOutsideHTMLIsLocked="false" -->
<head>
<cfif NOT IsDefined ("session.admininitials") OR session.admininitials eq 999>
	<cflocation url="index.cfm?passwordcheck=failed" addtoken="no">
</cfif>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<title>Website Admin</title>

<link href="../css/HPIL_admin.css" rel="stylesheet" type="text/css" media="all">



<cfquery datasource="#application.dsn#" name="csq">
	SELECT	*
    FROM questionare_cs
    ORDER BY date DESC
</cfquery>

<cfquery datasource="#application.dsn#" name="csq_refrence">
	SELECT	*
    FROM questionare_cs
    WHERE cs_q14 IS NOT NULL
</cfquery>


</head>

<body>
<div id="container">
	<div id="logo"><img src="../images/HPIL_logo_gif.gif" /></div>
 	<div class="clearline"></div>
	<div id="nav">
			<cfinclude template="nav_admin.cfm">	
	</div>
	<div id="pagecontent">
	  	<div id="pagetitle">
	  	Customer Service Stats
	  	</div>
<div class="clearline"></div>
		

<cfif csq.recordcount gt 0>

<cfset cs_q1_total = 0>
<cfset cs_q1_divide_count = 0>

<cfset cs_q2_total = 0>
<cfset cs_q2_divide_count = 0>

<cfset cs_q3_total = 0>
<cfset cs_q3_divide_count = 0>

<cfset cs_q4_total = 0>
<cfset cs_q4_divide_count = 0>

<!--- Taken out by request<cfset cs_q5_total = 0>
<cfset cs_q5_divide_count = 0>--->

<cfset cs_q6_total = 0>
<cfset cs_q6_divide_count = 0>

<cfset cs_q7_total = 0>
<cfset cs_q7_divide_count = 0>

<cfset cs_q8_total = 0>
<cfset cs_q8_divide_count = 0>


<cfoutput query="csq">
	
    <cfif isDefined ("cs_q1") AND cs_q1 NEQ ''>
		<cfset cs_q1_total = cs_q1 + cs_q1_total>
        <cfset cs_q1_divide_count = cs_q1_divide_count + 1>
    </cfif>
    
    <cfif isDefined ("cs_q2") AND cs_q2 NEQ ''>
		<cfset cs_q2_total = cs_q2 + cs_q2_total>
        <cfset cs_q2_divide_count = cs_q2_divide_count + 1>
    </cfif>
    
    <cfif isDefined ("cs_q3") AND cs_q3 NEQ ''>
		<cfset cs_q3_total = cs_q3 + cs_q3_total>
        <cfset cs_q3_divide_count = cs_q3_divide_count + 1>
    </cfif>
    
    <cfif isDefined ("cs_q4") AND cs_q4 NEQ ''>
		<cfset cs_q4_total = cs_q4 + cs_q4_total>
        <cfset cs_q4_divide_count = cs_q4_divide_count + 1>
    </cfif>
    
<!--- Taken out by request    <cfif isDefined ("cs_q5") AND cs_q5 NEQ ''>
		<cfset cs_q5_total = cs_q5 + cs_q5_total>
        <cfset cs_q5_divide_count = cs_q5_divide_count + 1>
    </cfif>--->
    
    <cfif isDefined ("cs_q6") AND cs_q6 NEQ ''>
		<cfset cs_q6_total = cs_q6 + cs_q6_total>
        <cfset cs_q6_divide_count = cs_q6_divide_count + 1>
    </cfif>
    
    <cfif isDefined ("cs_q7") AND cs_q7 NEQ ''>
		<cfset cs_q7_total = cs_q7 + cs_q7_total>
        <cfset cs_q7_divide_count = cs_q7_divide_count + 1>
    </cfif>
    
    <cfif isDefined ("cs_q8") AND cs_q8 NEQ ''>
		<cfset cs_q8_total = cs_q8 + cs_q8_total>
        <cfset cs_q8_divide_count = cs_q8_divide_count + 1>
	</cfif>

</cfoutput>

<cfoutput>


<cfif isDefined ("cs_q1_total") AND cs_q1_total NEQ 0>
<cfset cs_q1_avg = cs_q1_total / cs_q1_divide_count>
<cfelse>
<cfset cs_q1_avg = 0>
</cfif>

<cfif isDefined ("cs_q2_total") AND cs_q2_total NEQ 0>
<cfset cs_q2_avg = cs_q2_total / cs_q2_divide_count>
<cfelse>
<cfset cs_q2_avg = 0>
</cfif>

<cfif isDefined ("cs_q3_total") AND cs_q3_total NEQ 0>
<cfset cs_q3_avg = cs_q3_total / cs_q3_divide_count>
<cfelse>
<cfset cs_q3_avg = 0>
</cfif>

<cfif isDefined ("cs_q4_total") AND cs_q4_total NEQ 0>
<cfset cs_q4_avg = cs_q4_total / cs_q4_divide_count>
<cfelse>
<cfset cs_q4_avg = 0>
</cfif>

<!--- Taken out by request
<cfif isDefined ("cs_q5_total") AND cs_q5_total NEQ 0>
<cfset cs_q5_avg = cs_q5_total / cs_q5_divide_count>
<cfelse>
<cfset cs_q5_avg = 0>
</cfif>--->

<cfif isDefined ("cs_q6_total") AND cs_q6_total NEQ 0>
<cfset cs_q6_avg = cs_q6_total / cs_q6_divide_count>
<cfelse>
<cfset cs_q6_avg = 0>
</cfif>

<cfif isDefined ("cs_q7_total") AND cs_q7_total NEQ 0>
<cfset cs_q7_avg = cs_q7_total / cs_q7_divide_count>
<cfelse>
<cfset cs_q7_avg = 0>
</cfif>

<cfif isDefined ("cs_q8_total") AND cs_q8_total NEQ 0>
<cfset cs_q8_avg = cs_q8_total / cs_q8_divide_count>
<cfelse>
<cfset cs_q8_avg = 0>
</cfif>


<table id="stats_table" cellpadding="4" cellspacing="4">

	<tr>
        <td>Question</td>
    	<td>Score out of 4</td>
        <td>Number of responses</td>
    </tr>
	<tr>
        <th>RESPONSIVENESS: How do you rate our responsiveness in dealing with you?</th>
    	<td>#NumberFormat(cs_q1_avg,'.0')# / 4</td>
        <td>#cs_q1_divide_count#</td>
    </tr>
	<tr>
        <th>PROFESSIONALISM: How do you rate our professionalism in dealing with you?</th>
    	<td>#NumberFormat(cs_q2_avg,'.0')# / 4</td>
        <td>#cs_q2_divide_count#</td>
    </tr>
	<tr>
        <th>TECHNICAL SUPPORT: If you received any technical support, how do you rate the technical competence of our employees?</th>
    	<td>#NumberFormat(cs_q3_avg,'.0')# / 4</td>
        <td>#cs_q3_divide_count#</td>
    </tr>
	<tr>
        <th>PRODUCT QUALITY: How do you rate our products and services and did they meet your needs and expectations regarding quality and performance?</th>
    	<td>#NumberFormat(cs_q4_avg,'.0')# / 4</td>
        <td>#cs_q4_divide_count#</td>
    </tr>
	<!--- Taken out by request<tr>
        <th>DELIVERY: How do you rate our delivery on time performance and our commitment to meet your delivery expectations?</th>
    	<td>#NumberFormat(cs_q5_avg,'.0')# / 4</td>
        <td>#cs_q5_divide_count#</td>
    </tr>--->
	<tr>
        <th>COMPETITIVENESS: How do you rate the competitiveness of our products and do they represent value for money?</th>
    	<td>#NumberFormat(cs_q6_avg,'.0')# / 4</td>
        <td>#cs_q6_divide_count#</td>
    </tr>
	<tr>
        <th>QUALITY: How do you rate our approach to quality management to ensure complete customer satisfaction?</th>
    	<td>#NumberFormat(cs_q7_avg,'.0')# / 4</td>
        <td>#cs_q7_divide_count#</td>
    </tr>
	<tr>
        <th>Overall: How do you rate HEARING PRODUCTS INTERNATIONAL LIMITED?</th>
    	<td>#NumberFormat(cs_q8_avg,'.0')# / 4</td>
        <td>#cs_q8_divide_count#</td>
    </tr>

</cfoutput>

        	<cfset ref_web = 0>
        	<cfset ref_cat = 0>
        	<cfset ref_rec = 0>
        	<cfset ref_add = 0>
        	<cfset ref_cust = 0>
        	<cfset ref_other = 0>


<cfoutput query="csq_refrence">
	<cfif isDefined ("cs_q14") AND cs_q14 NEQ ''>
    
		<cfif #URLDecode(cs_q14)# eq "Internet">
        	<cfset ref_web = ref_web + 1>
        <cfelseif #URLDecode(cs_q14)# eq "Catalogue">
        	<cfset ref_cat = ref_cat + 1>
        <cfelseif #URLDecode(cs_q14)# eq "Recommendation">
        	<cfset ref_rec = ref_rec + 1>
        <cfelseif #URLDecode(cs_q14)# eq "Media / Advertisement">
        	<cfset ref_add = ref_add + 1>
        <cfelseif #URLDecode(cs_q14)# eq "I am a Previous Customer">
        	<cfset ref_cust = ref_cust + 1>
        <cfelseif #URLDecode(cs_q14)# eq "Other">
        	<cfset ref_other = ref_other + 1>
		</cfif>
    </cfif>
</cfoutput>

    <tr>
    <th>How did you hear about Hearing Products International Limited? (Please select one)</th>
        <td colspan="2">
            <cfchart
                     format="png"
                     scalefrom="0"
                     pieslicestyle="solid"
                     chartheight="300"
                     chartwidth="500">
                <cfchartseries
                             type="pie"
                             serieslabel="Website Referals Locations"
                             seriescolor="blue">
                    <cfchartdata item="Internet" value="#ref_web#">
                    <cfchartdata item="Catalogue" value="#ref_cat#">
                    <cfchartdata item="Recommendation" value="#ref_rec#">
                    <cfchartdata item="Media / Advertisement" value="#ref_add#">
                    <cfchartdata item="I am a Previous Customer" value="#ref_cust#">
                    <cfchartdata item="Other" value="#ref_other#">
                </cfchartseries>
            </cfchart>
        </td>
    </tr>
</table>

<table id="stats_table" cellpadding="4" cellspacing="4">
	<tr>
    	<th>Date Submitted</th>
        <th>Customer</th>
        <th>Overall score</th>
    </tr>
    
    <cfoutput query="csq">
    	<tr>
        	<td><a href="questionnaire_detail.cfm?id=#id#">#date#</a></td>
            <td>#URLDecode(cust_name)#</td>
            <td>#cs_q8#</td>
        </tr>
    </cfoutput>

</table>
<cfelse>

<p>No Records Found</p>

</cfif>


	</div>
</div>
<div class="clearline"></div>
<!--- end container div --->
</body>
</html>
