<cfset CurrentPage=GetFileFromPath(GetTemplatePath())>


<!--- get any child products --->
<cfquery name="children" datasource="#application.dsn#">
SELECT id, child_title, child_price, child_supplier_code FROM child_products WHERE product_id = #URL.id# ORDER BY child_price ASC
</cfquery>
<!--- get the product title --->
<cfquery name="getprod" datasource="#application.dsn#">
SELECT title FROM products WHERE id = #URL.id#
</cfquery>
<!--- get client name --->
<cfquery name="client" datasource="#application.dsn#">
SELECT client_name FROM clients WHERE id = #URL.clientid#
</cfquery>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/templates/HPIL_admin.dwt.cfm" codeOutsideHTMLIsLocked="false" -->
<head>
<cfif NOT IsDefined ("session.admininitials") OR session.admininitials eq 999>
	<cflocation url="index.cfm?passwordcheck=failed" addtoken="no">
</cfif>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<title>Hearing Aids and Speech Amplification</title>


<link href="../css/HPIL_admin.css" rel="stylesheet" type="text/css" media="all">




</head>

<body>
<div id="container">
	<div id="logo"><img src="../images/HPIL_logo_gif.gif" /></div>
 	<div class="clearline"></div>
	<div id="nav">
			<cfinclude template="nav_admin.cfm">	
	</div>
	<div id="pagecontent">
	  	<div id="pagetitle">
	  	
        <cfoutput>#getprod.title# child products for #client.client_name#.</cfoutput>
		
	  	</div>
<div class="clearline"></div>
		

	 <h2><cfoutput><a href="client_edit.cfm?id=#URL.clientid#">&lt; Return to #client.client_name#</a></cfoutput> main page</h2>
<div class="full_line"></div>
<table width="700" border="0">
  <tr>
    <td colspan="5">Select a child product</td>
  </tr>
  <tr>
    <td colspan="5">&nbsp;</td>
  </tr>
<cfoutput query="children">
  <tr>
    <td>#child_supplier_code#</td>
    <td>#child_title#</td>
    <td>#numberformat(child_price, '.00')#</td>
    <td>&nbsp;</td>
    <td><a href="price_breaks_client_child.cfm?id=#ID#&clientid=#URL.clientid#">Price Breaks</a></td>
  </tr>
</cfoutput>
	<tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
</table>

	</div>
</div>
<div class="clearline"></div>
<!--- end container div --->
</body>
</html>
