<cfset CurrentPage=GetFileFromPath(GetTemplatePath())>
<cfif IsDefined("FORM.MM_InsertRecord") AND FORM.MM_InsertRecord EQ "form1">
  <cfquery datasource="#application.dsn#">
    INSERT INTO faqs (fquestion, fanswer, forder) VALUES (
  <cfif IsDefined("FORM.fquestion") AND #FORM.fquestion# NEQ "">
    '#FORM.fquestion#'
      <cfelse>
    NULL
  </cfif>
   ,
  <cfif IsDefined("FORM.fanswer") AND #FORM.fanswer# NEQ "">
    '#URLEncodedformat(FORM.fanswer)#'
      <cfelse>
    NULL
  </cfif>
     ,
  <cfif IsDefined("FORM.forder") AND #FORM.forder# NEQ "">
    #FORM.forder#
      <cfelse>
    null
  </cfif>
    )
  </cfquery>
</cfif>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/Templates/HPIL_admin.dwt.cfm" codeOutsideHTMLIsLocked="false" -->
<head>
<cfif NOT IsDefined ("session.admininitials") OR session.admininitials eq 999>
	<cflocation url="index.cfm?passwordcheck=failed" addtoken="no">
</cfif>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<title>Website admin</title>

<link href="../css/HPIL_admin.css" rel="stylesheet" type="text/css" media="all">


<cfquery name="allfaqs" datasource="#application.dsn#">
SELECT ID, fquestion
FROM faqs
ORDER BY forder ASC
</cfquery>

</head>

<body>
<div id="container">
	<div id="logo"><img src="../images/HPIL_logo_gif.gif" /></div>
 	<div class="clearline"></div>
	<div id="nav">
			<cfinclude template="nav_admin.cfm">	
	</div>
	<div id="pagecontent">
	  	<div id="pagetitle">
	  	
<h2>Frequently Asked Questions</h2>
		
	  	</div>
<div class="clearline"></div>
		

    <cfform method="post" name="form1" action="#CurrentPage#" enctype="multipart/form-data">
      <table>
        <tr>
          <td class="formlabel">Display order:</td>
          <td><cfinput type="text" name="forder" value="20" size="6"></td>
        </tr>
         <tr>
          <td class="formlabel">Question:</td>
          <td>
<textarea name="fquestion" richtext="true" toolbar="pql" width="600" height="200" fontsizes="1/1,2/2,3/3,4/4,5/5,6/6,7/7"></textarea>		  </td>
        </tr>
       <tr>
          <td class="formlabel">Answer:</td>
          <td>
<textarea name="fanswer" richtext="true" toolbar="pql" width="600" height="340" fontsizes="1/1,2/2,3/3,4/4,5/5,6/6,7/7"></textarea>		  </td>
        </tr>
        <tr>
          <td>&nbsp;</td>
          <td><input type="submit" value="Add FAQ"></td>
        </tr>
      </table>
      <cfinput type="hidden" name="MM_InsertRecord" value="form1">
    </cfform>
<div class="greyline"></div>
<h2>Existing FAQs</h2>
<table>
<cfoutput query="allfaqs">
	<tr>
		<td>#URLDecode(fquestion)#</td>
		<td class="editlink"><a href="faqs_edit.cfm?ID=#ID#">edit</a></td>
		<td class="editlink"><a href="faqs_delete.cfm?ID=#ID#">delete</a></td>
	</tr>
</cfoutput>
</table>
    
	</div>
</div>
<div class="clearline"></div>
<!--- end container div --->
</body>
</html>
