<cfsetting showdebugoutput="no" enablecfoutputonly="yes">
<cfparam name="FORM.thisvalue" default="0">

<cfif IsDefined("FORM.thisvalue") AND FORM.thisvalue eq 1>
    <cfquery datasource="#application.dsn#">
    UPDATE #FORM.thistable#
    SET eselect = 1
    </cfquery>
<cfelse>
    <cfquery datasource="#application.dsn#">
    UPDATE #FORM.thistable#
    SET eselect = 0
    </cfquery>
</cfif>

<!--- get the client count --->
<cfquery name="clientcount" datasource="#application.dsn#">
SELECT ID
FROM clients
WHERE eselect = 1 AND client_eunsubscribed = 0
</cfquery>

<!--- get the customer count --->
<cfquery name="customercount" datasource="#application.dsn#">
SELECT custid
FROM customerinfo
WHERE eselect = 1 AND eunsubscribed = 0
</cfquery>

<!--- get the subscribers count --->
<cfquery name="mailercount" datasource="#application.dsn#">
SELECT ID
FROM mailing_list
WHERE eselect = 1 AND eunsubscribed = 0
</cfquery>

<cfset totalcount = evaluate(clientcount.recordcount + customercount.recordcount + mailercount.recordcount)>

<!--- set results--->
<cfset result = structNew()>

<cfif IsDefined("totalcount") AND totalcount gt 0>
	<cfset result.ecount = totalcount>
	<cfset result.showbutton = 1>
<cfelse>
	<cfset result.ecount = 0>
	<cfset result.showbutton = 0>
</cfif>	
	
	
<cfwddx action="CFML2JS" input="#result#" toplevelvariable="r">	