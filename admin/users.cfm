<cfset CurrentPage=GetFileFromPath(GetTemplatePath())>
<cfif IsDefined("FORM.MM_InsertRecord") AND FORM.MM_InsertRecord EQ "form1">
  <cfquery datasource="#application.dsn#">
    INSERT INTO adminusers (username, password, initials, slevel) VALUES (
  <cfif IsDefined("FORM.username") AND #FORM.username# NEQ "">
    '#FORM.username#'
      <cfelse>
    NULL
  </cfif>
    ,
  <cfif IsDefined("FORM.password") AND #FORM.password# NEQ "">
    '#FORM.password#'
      <cfelse>
    NULL
  </cfif>
    ,
  <cfif IsDefined("FORM.initials") AND #FORM.initials# NEQ "">
    '#FORM.initials#'
      <cfelse>
    NULL
  </cfif>
    ,
  <cfif IsDefined("FORM.slevel") AND #FORM.slevel# NEQ "">
    #FORM.slevel#
      <cfelse>
    NULL
  </cfif>
    )
  </cfquery>
  <cflocation url="users.cfm">
</cfif>
<cfquery name="getusers" datasource="#application.dsn#">
SELECT userID, username, password, initials, slevel
FROM adminusers
ORDER BY username ASC
</cfquery>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/Templates/HPIL_admin.dwt.cfm" codeOutsideHTMLIsLocked="false" -->
<head>
<cfif NOT IsDefined ("session.admininitials") OR session.admininitials eq 999>
	<cflocation url="index.cfm?passwordcheck=failed" addtoken="no">
</cfif>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<title>Website admin</title>

<link href="../css/HPIL_admin.css" rel="stylesheet" type="text/css" media="all">


<script src="../javascripts/engine.js"></script>
<script src="../javascripts/prototype.js"></script>
<script src="../javascripts/scriptaculous.js"></script>
<script type="text/javascript">
<!--
function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_validateForm() { //v4.0
  var i,p,q,nm,test,num,min,max,errors='',args=MM_validateForm.arguments;
  for (i=0; i<(args.length-2); i+=3) { test=args[i+2]; val=MM_findObj(args[i]);
    if (val) { nm=val.name; if ((val=val.value)!="") {
      if (test.indexOf('isEmail')!=-1) { p=val.indexOf('@');
        if (p<1 || p==(val.length-1)) errors+='- '+nm+' must contain an e-mail address.\n';
      } else if (test!='R') { num = parseFloat(val);
        if (isNaN(val)) errors+='- '+nm+' must contain a number.\n';
        if (test.indexOf('inRange') != -1) { p=test.indexOf(':');
          min=test.substring(8,p); max=test.substring(p+1);
          if (num<min || max<num) errors+='- '+nm+' must contain a number between '+min+' and '+max+'.\n';
    } } } else if (test.charAt(0) == 'R') errors += '- '+nm+' is required.\n'; }
  } if (errors) alert('The following error(s) occurred:\n'+errors);
  document.MM_returnValue = (errors == '');
}
function pwShow(userid){
	param = new Object();
	param.userid = userid
	http( 'POST'  , 'ajax_showpw.cfm' , pwshow_response, param ); 
}
function pwshow_response(obj){ 
	var showpw = obj.showpw
	var userid = obj.userid
	document.getElementById('pword'+userid).innerHTML = obj.showpw
}
function updatePW(thisvalue, userid){
	param = new Object();
	param.thisvalue = thisvalue
	param.userid = userid
	http( 'POST'  , 'ajax_updatepw.cfm' , pwupdate_response, param ); 
}
function pwupdate_response(obj){ 
	var userid = obj.userid
	document.getElementById('pword'+userid).innerHTML = '******'
}
//-->
</script>

</head>

<body>
<div id="container">
	<div id="logo"><img src="../images/HPIL_logo_gif.gif" /></div>
 	<div class="clearline"></div>
	<div id="nav">
			<cfinclude template="nav_admin.cfm">	
	</div>
	<div id="pagecontent">
	  	<div id="pagetitle">
	  	
       <h2>Maintain users</h2>
		
	  	</div>
<div class="clearline"></div>
		

<table width="96%" border="0">
  <tr>
    <td>&nbsp;
      <form action="<cfoutput>#CurrentPage#</cfoutput>" method="post" name="form1" id="form1">
        <table>
          <tr>
            <td class="formlabel">User name:</td>
            <td class="longtext"><input type="text" name="username" value="" size="32" /></td>
          </tr>
          <tr>
            <td class="formlabel">Password:</td>
            <td class="longtext"><input type="text" name="password" value="" size="32" /></td>
          </tr>
          <tr>
            <td class="formlabel">Initials:</td>
            <td class="longtext"><input type="text" name="initials" value="" size="32" /></td>
          </tr>
          <tr>
            <td class="formlabel">Security level:</td>
            <td class="longtext"><select name="slevel">
                <option value="2" >User (2)</option>
                <option value="1" >Administrator (1)</option>
              </select>
            </td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td class="longtext"><input type="submit" onclick="MM_validateForm('username','','R','password','','R','initials','','R');return document.MM_returnValue" value="Add user" /></td>
          </tr>
        </table>
        <input type="hidden" name="MM_InsertRecord" value="form1" />
      </form>
    </td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>
	<table width="669" border="0">
      <tr>
        <td width="114" class="label">Username</td>
        <td width="112" class="label">Password</td>
        <td width="74" class="label">Initials</td>
        <td width="91" class="label">Security</td>
        <td width="138">&nbsp;</td>
        <td width="114">&nbsp;</td>
      </tr>
<cfoutput query="getusers">
      <tr>
        <td>#username#</td>
        <td><span id="pword#userID#">*******</span></td>
        <td>#initials#</td>
        <td>#slevel#</td>
        <td><a href="##" onclick="pwShow(#userID#)">change p/w</a></td>
        <td><a href="user_delete.cfm?id=#userID#">delete</a></td>
      </tr>
</cfoutput>
    </table></td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
</table>

	</div>
</div>
<div class="clearline"></div>
<!--- end container div --->
</body>
</html>
