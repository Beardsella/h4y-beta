<cfif IsDefined("URL.id") AND URL.id neq ''>
	<cfquery name="thiscat" datasource="#application.dsn#">
	SELECT ID, cat_name
	FROM categories
	WHERE id = #URL.id#
	</cfquery>
	
	<cfquery name="adcount" datasource="#application.dsn#">
	SELECT ID
	FROM product_cats
	WHERE category_id = #URL.id#
	</cfquery>
<cfelseif IsDefined("URL.tid") AND URL.tid neq ''>
	<cfquery name="countsubs" datasource="#application.dsn#">
	SELECT ID, cat_name
	FROM categories
	WHERE parentid = #URL.tid#
	</cfquery>
	<cfquery name="thiscat" datasource="#application.dsn#">
	SELECT ID, cat_name
	FROM categories
	WHERE id = #URL.tid#
	</cfquery>
</cfif>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/templates/HPIL_admin.dwt.cfm" codeOutsideHTMLIsLocked="false" -->
<head>
<cfif NOT IsDefined ("session.admininitials") OR session.admininitials eq 999>
	<cflocation url="index.cfm?passwordcheck=failed" addtoken="no">
</cfif>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<title>Website Admin</title>
<link href="../css/HPIL_admin.css" rel="stylesheet" type="text/css" media="all">




</head>

<body>
<div id="container">
	<div id="logo"><img src="../images/HPIL_logo_gif.gif" /></div>
 	<div class="clearline"></div>
	<div id="nav">
			<cfinclude template="nav_admin.cfm">	
	</div>
	<div id="pagecontent">
	  	<div id="pagetitle">
	  	
		<h1>Delete <cfoutput>#thiscat.cat_name#</cfoutput> Category ?</h1>
  
	  	</div>
<div class="clearline"></div>
		

<p>
<cfif IsDefined("URL.id") AND URL.id neq ''>
	There are <cfoutput>#adcount.recordcount#</cfoutput> products in this category.
	<a href="category_delete_do.cfm?id=<cfoutput>#URL.id#</cfoutput>">delete</a>&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;<a href="categories.cfm">cancel delete</a>
<cfelseif IsDefined("URL.tid") AND URL.tid neq ''>
	<cfif countsubs.recordcount>
	There are <cfoutput>#countsubs.recordcount#</cfoutput> sub categories in this category. <br /><br />You need to delete these before you can delete this top category.
	<cfelse>
	<a href="category_delete_do.cfm?id=<cfoutput>#URL.tid#</cfoutput>">delete</a>&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;<a href="categories.cfm">cancel delete</a>
	</cfif>
</cfif>
</p>

	</div>
</div>
<div class="clearline"></div>
<!--- end container div --->
</body>
</html>