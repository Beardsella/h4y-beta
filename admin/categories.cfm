<cfsetting requesttimeout="3600">

<cfset path = expandPath("../empty_me/")>
<cfset CurrentPage=GetFileFromPath(GetTemplatePath())>
<cfif IsDefined("FORM.MM_InsertRecord") AND FORM.MM_InsertRecord EQ "form1">
  <cfquery datasource="#application.dsn#">
    INSERT INTO categories (cat_name, parentid, display_order, cat_description, gallery) VALUES (
  <cfif IsDefined("FORM.cat_name") AND #FORM.cat_name# NEQ "">
    '#FORM.cat_name#'
      <cfelse>
    NULL
  </cfif>
    ,
  <cfif IsDefined("FORM.parentid") AND #FORM.parentid# NEQ "">
    #FORM.parentid#
      <cfelse>
    NULL
  </cfif>
    ,
  <cfif IsDefined("FORM.display_order") AND #FORM.display_order# NEQ "">
    #FORM.display_order#
      <cfelse>
    99
  </cfif>
    ,
  <cfif IsDefined("FORM.cat_description") AND #FORM.cat_description# NEQ "">
    '#FORM.cat_description#'
      <cfelse>
    NULL
  </cfif>
    ,
  <cfif IsDefined("FORM.gallery") AND #FORM.gallery# EQ 1>
    #FORM.gallery#
      <cfelse>
   	0
  </cfif>
    )
  </cfquery>
	<cfquery name="getid" datasource="#application.dsn#">
    SELECT max(id) AS thisid FROM categories
    </cfquery>
 <!--- Upload image and resize, make a thumbnail too --->
  
  	<cfif IsDefined("FORM.newImage") AND #FORM.newImage# NEQ "">
		
			<cfset accept_upload = 1>
        
            <!--- only allow these mime types. Edit as required --->
            <cffile action="upload" filefield="newImage" destination="#GetTempDirectory()#" nameconflict="overwrite" result="uploadResult" accept="image/jpg, image/bmp, image/gif, image/jpeg">
            
            <!--- allow images only if needed --->
            <cfif Not IsImageFile(getTempDirectory() & uploadResult.serverFile)>
                <cffile action="delete" file="#getTempDirectory()##uploadResult.serverFile#">
                <cfset accept_upload = 0>
            </cfif>
            
            <!--- only allow these file extensions. Edit as required --->
            <cfif NOT ListFindNoCase("bmp,gif,jpg,jpeg,png", uploadResult.ServerFileExt)>
                <cffile action="delete" file="#getTempDirectory()##uploadResult.serverFile#">
            
                <cfset display_error = 'This file type cannot currently be uploaded. Please contact PQL.'>
                <cfset accept_upload = 0>
            
            </cfif>
            
            <cfif accept_upload eq 1>
            
                    <!--- IMAGES: move the file. #path# is normally already set in the page header---> 
                    <cfimage action="WRITE" source="#getTempDirectory()##uploadResult.serverFile#" destination="#path##getid.thisid#.jpg" overwrite="yes">
                            
            </cfif>
                  
            <!--- delete the temp file in all cases --->
            <cffile action="delete" file="#getTempDirectory()##uploadResult.serverFile#">
		
		<cfimage source="#path##getid.thisid#.jpg" name="lpic">		
<!--- make big image --->
<!---		<cfset ImageScaleToFit(lpic,800,600)>

<cfimage
    action = "write"
    destination = "../category_images/#getid.thisid#.jpg"
    source = "#lpic#"
    overwrite = "yes">
--->		
<!--- make thumbnail --->
		<cfset ImageScaleToFit(lpic,150,150)>

<cfimage
    action = "write"
    destination = "../category_images/#getid.thisid#.jpg"
    source = "#lpic#"
    overwrite = "yes">

	</cfif>
</cfif>

<cfquery name="topcats" datasource="#application.dsn#">
SELECT id, cat_name
FROM categories
WHERE parentid = 1
ORDER BY display_order ASC
</cfquery>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/templates/HPIL_admin.dwt.cfm" codeOutsideHTMLIsLocked="false" -->
<head>
<cfif NOT IsDefined ("session.admininitials") OR session.admininitials eq 999>
	<cflocation url="index.cfm?passwordcheck=failed" addtoken="no">
</cfif>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<title>Website Admin</title>
<link href="../css/HPIL_admin.css" rel="stylesheet" type="text/css" media="all">




</head>

<body>
<div id="container">
	<div id="logo"><img src="../images/HPIL_logo_gif.gif" /></div>
 	<div class="clearline"></div>
	<div id="nav">
			<cfinclude template="nav_admin.cfm">	
	</div>
	<div id="pagecontent">
	  	<div id="pagetitle">
	  	
 			<h1>Product Categories</h1>

		  
	  	</div>
<div class="clearline"></div>
		

         <form method="post" name="form1" action="<cfoutput>#CurrentPage#</cfoutput>" enctype="multipart/form-data">
            <table>
              <tr>
                <td class="formlabel">Category name:</td>
                <td class="longtext"><input type="text" name="cat_name" value="" size="32"></td>
              </tr>
              <tr>
                <td class="formlabel">Parent category :</td>
                <td class="longtext"><select name="parentid">
                      <option value="1">None (Top level)</option>
                    <cfloop query="topcats">
                      	<cfquery name="subcats" datasource="#application.dsn#">
                        SELECT id, cat_name
                        FROM categories
                        WHERE parentid = #topcats.id#
                        ORDER BY display_order ASC
                        </cfquery>

                      <option value="<cfoutput>#topcats.id#</cfoutput>">
					 <span style="font-weight:bold"><cfoutput>#URLDecode(topcats.cat_name)#</cfoutput></span>
					  </option>
                      <cfloop query="subcats">
                          <option value="<cfoutput>#subcats.id#</cfoutput>">
                         <cfoutput>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;#URLDecode(subcats.cat_name)#</cfoutput>
                          </option>
                      </cfloop>
                    </cfloop>
                  </select>
                </td>
              </tr>
              <tr>
                <td class="formlabel">Display order:</td>
                <td class="longtext"><input type="text" name="display_order" value="99" size="3"></td>
              </tr>
              <tr>
                <td class="formlabel">Description
                <p>(optional):</p></td>
                <td class="longtext"><textarea name="cat_description" cols="40" rows="5"></textarea>
                </td>
              </tr>
              <tr>
                 <td class="formlabel">Upload Photo:</td>
                 <td><input name="newImage" type="file" /></td>
              </tr>
    <tr>
       <td class="formlabel">Show gallery:</td>
        <td><input name="gallery" type="checkbox" value="1" />&nbsp;&nbsp;Displays gallery link if checked</td>
    </tr>
              <tr>
                <td class="formlabel">&nbsp;</td>
                <td><input type="submit" value="Add Category"></td>
              </tr>
            </table>
            <input type="hidden" name="MM_InsertRecord" value="form1">
          </form>
<div class="clearline"></div>
<cfloop query="topcats">
<div class="catbox">
	<ul>
	<cfoutput>
	<b><a href="category_edit.cfm?id=#topcats.id#" title="edit">#URLDecode(cat_name)#</a></b>
<span class="goright"><a href="category_delete_check.cfm?tid=#topcats.id#"><img src="../images/delete.gif" alt="delete" border="0" /></a></span>	</cfoutput>
	<cfquery name="subcats" datasource="#application.dsn#">
	SELECT id, cat_name
	FROM categories
	WHERE parentid = #topcats.id#
	ORDER BY display_order ASC
	</cfquery>
	<cfloop query="subcats">
        <cfquery name="subsubcats" datasource="#application.dsn#">
        SELECT id, cat_name
        FROM categories
        WHERE parentid = #subcats.id#
        ORDER BY display_order ASC
        </cfquery>
		<li class="subs"><a href="category_edit.cfm?id=<cfoutput>#subcats.id#</cfoutput>" title="edit"><cfoutput>#URLDecode(subcats.cat_name)#</cfoutput></a></li>
		<span class="goright"><a href="category_delete_check.cfm?id=<cfoutput>#subcats.id#</cfoutput>"><img src="../images/delete.gif" alt="delete" border="0" /></a></span>
     		<cfloop query="subsubcats">
                <li class="subsub"><a href="category_edit.cfm?id=<cfoutput>#subsubcats.id#</cfoutput>" title="edit"><cfoutput>#URLDecode(subsubcats.cat_name)#</cfoutput></a></li>
                <span class="goright"><a href="category_delete_check.cfm?id=<cfoutput>#subsubcats.id#</cfoutput>"><img src="../images/delete.gif" alt="delete" border="0" /></a></span>
            </cfloop>   	
	</cfloop>
	</ul>
</div>
</cfloop>

	</div>
</div>
<div class="clearline"></div>
<!--- end container div --->
</body>
</html>