<cfset CurrentPage=GetFileFromPath(GetTemplatePath())>

<cfif IsDefined("FORM.MM_UpdateRecord") AND FORM.MM_UpdateRecord EQ "form1">
  <cfquery datasource="#application.dsn#">
    INSERT INTO child_products (product_id, child_supplier_code, child_title, child_price)
    VALUES(
  <cfif IsDefined("FORM.id") AND FORM.id NEQ "">
    #FORM.id#
      <cfelse>
    NULL
  </cfif>
    , 
  <cfif IsDefined("FORM.child_supplier_code") AND #FORM.child_supplier_code# NEQ "">
    '#FORM.child_supplier_code#'
      <cfelse>
    NULL
  </cfif>
    , 
  <cfif IsDefined("FORM.child_title") AND #FORM.child_title# NEQ "">
    '#FORM.child_title#'
      <cfelse>
    NULL
  </cfif>
    , 
  <cfif IsDefined("FORM.child_price") AND #FORM.child_price# NEQ "">
    #FORM.child_price#
      <cfelse>
    NULL
  </cfif>
  )
  </cfquery>

</cfif>

<cfif IsDefined("URL.id") AND URL.id neq ''>
	<cfset prodid = URL.id>
<cfelse>
	<cfset prodid = FORM.id>
</cfif>

<!--- get any child products --->
<cfquery name="children" datasource="#application.dsn#">
SELECT id, child_title, child_price, child_supplier_code FROM child_products WHERE product_id = #prodid# ORDER BY child_price ASC
</cfquery>
<!--- get the product title --->
<cfquery name="getprod" datasource="#application.dsn#">
SELECT title FROM products WHERE id = #prodid#
</cfquery>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/templates/HPIL_admin.dwt.cfm" codeOutsideHTMLIsLocked="false" -->
<head>
<cfif NOT IsDefined ("session.admininitials") OR session.admininitials eq 999>
	<cflocation url="index.cfm?passwordcheck=failed" addtoken="no">
</cfif>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<title>Hearing Aids and Speech Amplification</title>


<link href="../css/HPIL_admin.css" rel="stylesheet" type="text/css" media="all">




</head>

<body>
<div id="container">
	<div id="logo"><img src="../images/HPIL_logo_gif.gif" /></div>
 	<div class="clearline"></div>
	<div id="nav">
			<cfinclude template="nav_admin.cfm">	
	</div>
	<div id="pagecontent">
	  	<div id="pagetitle">
	  	Child products for <cfoutput><a href="edit_product.cfm?id=#prodid#">#URLDecode(getprod.title)#</a></cfoutput>
	  	</div>
<div class="clearline"></div>
		

<div class="full_line"></div>
<table width="700" border="0">
  <tr>
    <td colspan="6">Existing child products</td>
  </tr>
  <tr>
    <td colspan="6">&nbsp;</td>
  </tr>
<cfoutput query="children">
  <tr>
    <td>#child_supplier_code#</td>
    <td>#child_title#</td>
    <td>#numberformat(child_price, '.00')#</td>
    <td><a href="child_delete.cfm?id=#id#&prodid=#prodid#">Delete</a></td>
    <td>&nbsp;</td>
    <td><a href="price_breaks_child.cfm?id=#ID#">Price Breaks</a></td>
  </tr>
</cfoutput>
	<tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
</table>
<div class="full_line"></div>

<cfform method="post" name="form1" action="#CurrentPage#" enctype="multipart/form-data">
  <table>
      <tr>
      <td colspan="6">Add a new child product with this form</td>
    </tr>
    <tr>
      <td colspan="6">&nbsp;</td>
    </tr>
    <tr>
       <td class="formlabel">Supplier Code:</td>
      <td><input type="text" name="child_supplier_code" value="" size="20"></td>
     <td class="formlabel">Title:</td>
      <td><input type="text" name="child_title" value="" size="50"></td>
      <td class="formlabel">Price:</td>
      <td><input type="text" name="child_price" value="" size="6"></td>
    </tr>
    <tr>
      <td colspan="5">&nbsp;</td>
      <td><input type="submit" value="Add child product"></td>
    </tr>
  </table>
  <input type="hidden" name="ID" value="<cfoutput>#prodid#</cfoutput>">
  <input type="hidden" name="MM_UpdateRecord" value="form1">
</cfform>

	</div>
</div>
<div class="clearline"></div>
<!--- end container div --->
</body>
</html>
