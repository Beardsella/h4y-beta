<div class="clearfix"></div>

<footer id="footer">

<cfquery name="footer_carousel" datasource="#application.DSN#">
    SELECT *
    FROM product_images
</cfquery>

<div class="container hidden-xs">
	<div class="row">
		<div class="col-sm-12 col-md-12 col-lg-12">
                <div id="myFooterCarousel" class="carousel slide">
                 
<!---                 <ol class="carousel-indicators">
                    <li data-target="#myFooterCarousel" data-slide-to="0" class="active"></li>
                    <li data-target="#myFooterCarousel" data-slide-to="1"></li>
                    <li data-target="#myFooterCarousel" data-slide-to="2"></li>
                </ol>
 --->                 
                <!-- Carousel items -->
                <div class="carousel-inner">
<div class="item active">
    <div class="row-fluid">

        <cfoutput query="footer_carousel" maxrows="4">

    <cfquery datasource="#application.DSN#" name="getpage">
        SELECT * 
        FROM Products
        WHERE id = #footer_carousel.product_id#
    </cfquery>    
            
    	  <div class="col-sm-3 col-md-3 col-lg-3"><a href="#getpage.SEO_url#" class="thumbnail"><img src="product-images/thumbs/#image_name#" alt="#image_caption#" title="#image_caption#" style="max-width:100%;" /></a></div>
         </cfoutput>

	</div><!--/row-fluid-->
 </div><!--/item-->

                <div class="item">
                	<div class="row-fluid">

                        <cfset count = 0>
<cfoutput query="footer_carousel" startrow="5">

    <cfif count eq 4>   
        </div><!--/row-fluid-->
    </div><!--/item-->
    <div class="item">
        <div class="row-fluid">
            <cfset count = 0>
    </cfif>    
    <cfset count = #count# + 1>

    <cfquery datasource="#application.DSN#" name="getpage">
        SELECT * 
        FROM Products
        WHERE id = #footer_carousel.product_id#
    </cfquery>

	<div class="col-sm-3 col-md-3 col-lg-3"><a href="#getpage.SEO_url#" class="thumbnail"><img src="product-images/thumbs/#image_name#" alt="#image_caption#" title="#image_caption#" style="max-width:100%;" /></a></div>



 </cfoutput>                

    	</div><!--/row-fluid-->
    </div><!--/item-->
     
    </div><!--/carousel-inner-->
                 
                <a class="left carousel-control" href="#myFooterCarousel" data-slide="prev"> <span class="glyphicon glyphicon-chevron-left my_bg_circle"></span></a>
                <a class="right carousel-control" href="#myFooterCarousel" data-slide="next"> <span class="glyphicon glyphicon-chevron-right my_bg_circle"></span></a>
                </div><!--/myFooterCarousel-->
    </div>
        </div>
<div>
    <p class="pull-right" style="margin:20px;"><a href="#top">Back to top</a></p>
    <p style="margin:20px;">Hearing Products International &copy; 2014</p>
</div>                 
</div>





</footer>
