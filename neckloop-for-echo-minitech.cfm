<!DOCTYPE html>
<html>

<!--- product_detail_template.cfm - V 1.11  --->    
<head>

<cfinclude template="global_header.cfm">

</head>

<body><!--- START OF CONTENT --->

<!--- SIDE NAV --->
<div class="container col-lg-2 visible-lg">
  <cfinclude template="side_nav.cfm">
</div>

<div class="container col-lg-10" data-spy="scroll" data-target="#mysidenav" id="main_container">

  <cfinclude template="nav.cfm">

  <div class="space_100px"></div>

  <div class="row">
  
    <!--- CONTENT HERE --->
          <cfoutput query="this_product">
     
          <div class="clearfix"></div>
          
  <cfquery datasource="#application.DSN#" name="related_images">
    SELECT *
    FROM product_images
    WHERE product_id = #this_product.id#  
  </cfquery>
<!--- 
  <cfloop query="related_images">
      <img src="product-images/#image_name#" alt="#URLDecode(image_caption)#">
</cfloop> --->
    
<div class="col-lg-10">

    <div id="myCarousel" class="carousel slide hidden-xs col-lg-6" data-ride="carousel">
  <!-- Indicators -->
  <ol class="carousel-indicators">
  <cfloop query="related_images" endrow="1">
    <li data-target="##myCarousel" data-slide-to="#related_images.currentrow#" class="active"></li>
  </cfloop>  
    <cfloop query="related_images" startrow="2">
    <li data-target="##myCarousel" data-slide-to="#related_images.currentrow#"></li>
  </cfloop>  

  </ol>

  <!-- Wrapper for slides -->
  <div class="carousel-inner">

<cfloop query="related_images" endrow="1">
  <div class="item active mycarousel-item">
    <a href="product-images/#image_name#" data-lightbox="carousel" alt="#URLDecode(image_caption)#" title="#URLDecode(image_caption)#"><img src="product-images/#image_name#" alt="#URLDecode(image_caption)#"></a>

    <div class="carousel-caption">
      #URLDecode(image_caption)#
    </div>
  </div>
</cfloop> 

<cfloop query="related_images" startrow="2">
  <div class="item mycarousel-item">
    <a href="product-images/#image_name#" data-lightbox="carousel" alt="#URLDecode(image_caption)#" title="#URLDecode(image_caption)#">
    <img src="product-images/#image_name#" alt="#URLDecode(image_caption)#"></a>

    <div class="carousel-caption">
      #URLDecode(image_caption)#
    </div>
  </div>
</cfloop> 
  
  </div>

  <!-- Controls -->
  <a class="left carousel-control" href="##myCarousel" data-slide="prev">
    <span class="glyphicon glyphicon-chevron-left"></span>
  </a>
  <a class="right carousel-control" href="##myCarousel" data-slide="next">
    <span class="glyphicon glyphicon-chevron-right"></span>
  </a>
</div>














          <div class="col-lg-6">
                        <h1>#URLDecode(title)#</h1>
           <h2><span class="label label-success tiny">+ <cfif isDefined("delivery") AND delivery NEQ "">
            &pound; #delivery# 
            <cfelse>
              Free Delivery
          </cfif><span class="glyphicon glyphicon-road"></span></span>  <span class="label label-primary tiny">&pound; #price#</span></h2>
          <button type="button" class="btn btn-success">Add to Cart <span class="glyphicon glyphicon-shopping-cart"></span></button>

          <div class="clearfix"></div>
<div>#URLDecode(description)#</div></div>


<cfquery datasource="#application.DSN#" name="related_docs">
  SELECT * 
  FROM uploads
  WHERE product_id = #this_product.id#
</cfquery>

<cfif isDefined("related_docs.recordcount") AND related_docs.recordcount gt 0>

  <div class="col-lg-6 float">
    <cfoutput><h3>Related Documents <span class="badge">#related_docs.recordcount#</span></h3></cfoutput>
<ul>
    <cfloop query="related_docs">
      <li><a href="uploaded_files/#filename#">#URLDecode(filelabel)#</a></li>
    </cfloop>
</ul>
  </div>
  
</cfif>

        
        </div>
        </div>

        <div class="row">

            <hr class="featurette-divider">

          <div class="col-lg-12">#URLDecode(technical)#</div>

          </cfoutput>

  </div><!--- /Content --->

  <hr class="featurette-divider">


</div><!-- /.container -->

  <!-- FOOTER -->
  <cfinclude template="footer.cfm">
  <!--- /FOOTER --->

<cfinclude template="java_insert.cfm">

</body>
</html>