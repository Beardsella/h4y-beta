<!DOCTYPE html>
<html>

<!--- Blank template V 1.0  --->    
<head>

  <LINK REL="SHORTCUT ICON" HREF="images/h4y.ico" />

  <!-- Bootstrap Scalabe screen size -->
  <meta name="viewport" content="width=device-width, initial-scale=1.0">

  <cfset CurrentPage = GetFileFromPath(GetTemplatePath())>

  <cfquery datasource="#application.DSN#" name="get_top_cats">
    SELECT *
    FROM categories
    WHERE parentid = 1 AND display = 1 AND id <> 22
    ORDER BY display_order
  </cfquery>

  <cfquery datasource="#application.DSN#" name="this_product">
    SELECT *
    FROM products
    WHERE SEO_URL like "#CurrentPage#" 
  </cfquery>

  <cfquery datasource="#application.DSN#" name="the_meta_keywords">
    SELECT *
    FROM meta_tags
    WHERE prod_id = "#this_product.id#" 
  </cfquery>

  <title><cfoutput>#URLDecode(this_product.meta_title)# - Hear 4 You</cfoutput></title>
  <meta name="Keywords" content="#URLDecode(the_meta_keywords.name)#" />
  <meta name="Description" content="#URLDecode(this_product.meta_description)#" />

  <cfif find("localhost", "#CGI.http_host#")>
    
    <!-- LESS -->
    <link href="css/bootstrap.less" rel="stylesheet">
    <link href="css/custom.less" rel="stylesheet">  

  <cfelse>

    <!-- CSS -->
    <link href="css/bootstrap.css" rel="stylesheet">
    <link href="css/custom.css" rel="stylesheet">  

  </cfif>

  <link href="css/lightbox.css" rel="stylesheet" />

</head>

<body><!--- START OF CONTENT --->

<!--- SIDE NAV --->
<div class="container col-lg-2 visible-lg">
  <cfinclude template="side_nav.cfm">
</div>

<div class="container col-lg-10" data-spy="scroll" data-target="#mysidenav" id="main_container">

  <cfinclude template="nav.cfm">

  <div class="space_100px"></div>

  <div class="row">
  
    <!-- Carousel
    ================================================== -->
    <div id="myCarousel" class="carousel slide hidden-xs col-lg-12" data-ride="carousel">
      <!-- Indicators -->
      <ol class="carousel-indicators">
        <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
        <li data-target="#myCarousel" data-slide-to="1"></li>
        <li data-target="#myCarousel" data-slide-to="2"></li>
        <li data-target="#myCarousel" data-slide-to="3"></li>
      </ol>
      <div class="carousel-inner">
        <div class="item active mycarousel-item">
         <div class="container">
         <div class="space_50px"></div>
                   <img src="images/PQL-banner.jpg" alt="PQL web design banner" class="img-responsive center-block"> 

            <div class="carousel-caption">
             </div>
          </div>
        </div>
        <div class="item mycarousel-item">
         <div class="container">
         <div class="space_50px"></div>
                   <img src="images/test-slide-1.jpg" alt="PQL web design banner" class="img-responsive center-block"> 

            <div class="carousel-caption">
             </div>
          </div>
        </div>
        <div class="item mycarousel-item">
         <div class="container">
         <div class="space_50px"></div>
                   <img src="images/test-slide-2.jpg" alt="PQL web design banner" class="img-responsive center-block"> 

            <div class="carousel-caption">
            </div>
          </div>
        </div>
        <div class="item mycarousel-item">
         <div class="container">
         <div class="space_50px"></div>
                   <img src="images/mobile-devices-slide.jpg" alt="PQL web design banner" class="img-responsive center-block"> 

            <div class="carousel-caption">
            </div>
          </div>
        </div>

      </div>
      <a class="left carousel-control" href="#myCarousel" data-slide="prev"><span class="glyphicon glyphicon-chevron-left my_bg_circle"></span></a>
      <a class="right carousel-control" href="#myCarousel" data-slide="next"><span class="glyphicon glyphicon-chevron-right my_bg_circle"></span></a>
    </div><!-- /.carousel -->

  <div class="space_30px"></div>

  </div><!-- /ROW -->

  <hr class="hidden-xs featurette-divider"  id="products">


  <div id="products_box" class="row list-group">

<cfset this_cat_count = 0 >

    <cfoutput query="get_top_cats">

      <cfif this_cat_count eq 4>
        <cfset this_cat_count = 0 >
        </div>
        <div id="products_box" class="row list-group">
      </cfif>

      <cfset this_cat_count = #this_cat_count# + 1 >

      <div class="item col-xs-12 col-sm-6 col-md-4 col-lg-3">
        <div class="thumbnail">
          <div class="caption">
            <h4 class="group inner list-group-item-heading">
            <span class="captalize">#URLDecode(cat_name)#</span></h4>
            <div class="space_20px"></div>
            <img src="category_images/thumbs/#cat_img#" class="img-responsive" style="margin:auto;">
            <div class="space_20px"></div>
            <p class="group inner list-group-item-text">
            #URLDecode(cat_description)#</p>
            <div class="row">
              <div class="col-xs-12 col-md-12 col-lg-12">
                <cfif isDefined("SEO_url")>
                  <a href="#SEO_url#" type="button" class="btn btn-primary btn-block">More #URLDecode(cat_name)#</a>
                <cfelse>
                  <a href="index.cfm" type="button" class="btn btn-primary btn-block">More #URLDecode(cat_name)#</a>
                </cfif>
              </div>
            </div>
          </div>
        </div>
      </div>
    </cfoutput>
  </div>


      <hr class="featurette-divider" id="who-we-are">


      <div class="row featurette">
        <div class="col-xs-12 col-sm-8 col-md-7">
          <h2 class="featurette-heading hidden-md">Who We Are <span class="text-muted small">Hearing Products International</span></h2>
          <h2 class="visible-md">Who We Are <span class="text-muted small">Hearing Products International</span></h2>
          <p class="lead">We are the largest supplier of own brand Aids for Living in the country. Echo is the registered Trade Mark of Hearing Products International Limited, established in 1992 in Stockport, England. We have provided Sensory equipment to Social Service Departments over the past 22 years from which our employees have gained a wealth of experience and Knowledge. We are a small Team based in Stockport and we pride ourselves on providing helpful and friendly customer advice and support for people with a hearing loss.</p>
        </div>
        <div class="col-xs-12 col-sm-4 col-md-5">
          <div class="visible-sm space_90px"></div>
          <img class="featurette-image img-responsive" src="images/jack-h4y.jpg" alt="Hearing Products International staff">
        </div>
      </div>

      <hr class="featurette-divider" id="what-we-do">

      <div class="row featurette">
        <div class="col-md-5 col-sm-4 hidden-xs">
                    <div class="visible-sm space_90px"></div>

          <img class="featurette-image img-responsive" src="images/hearing-aids.jpg" alt="Echo Hearing Aids">
        </div>
        <div class="col-xs-12 col-md-7 col-sm-8">
          <h2 class="featurette-heading hidden-md">What Do We Do? <span class="text-muted small">Hearing Aids</span></h2>
          <h2 class="visible-md">What Do We Do? <span class="text-muted small">Hearing Aids</span></h2>
          <p class="lead">We specialise in the design and sale of our own range of products, developed and improved since 1992 to embrace the latest technologies available. Products from Hearing Products International Limited are recognised as quality and easy to use products, representing value for money. We supply mainly to Local and Health Authorities/ Hearing aid dealers and also to the General Public via our website. Through an ever expanding network of authorised distributors, many of whom are renowned as quality hearing aid manufacturers, our products can be purchased worldwide. We continually improve our products and services through an established quality management system. </p>
        </div>
        <div class="col-xs-12 visible-xs">
          <img class="featurette-image img-responsive" src="images/hearing-aids.jpg" alt="Echo Hearing Aids">
        </div>
      </div>

            <hr class="featurette-divider" id="contact_details">

      <div class="row featurette">
        <div class="col-xs-12 col-md-5 col-sm-5">
          <h2 class="featurette-heading">Our Contact <span class="text-muted">Details.</span></h2>
          <p class="lead">Our office is located just outside Manchester between J25 & J26 on the M60.</p>

          <br>

          <div itemscope itemtype="http://schema.org/Organization"> 
             <strong><span itemprop="name">PQL Web Solutions</span></strong>
             <div itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">
                <span itemprop="streetAddress">716 Stockport Road West</span><br>
                <span itemprop="addressLocality">Stockport</span><br>
                <span itemprop="addressRegion">Manchester</span><br>
                 <span itemprop="postal-code">SK6 2EE</span>
              </div>
            <abbr title="Email">E:</abbr> <span itemprop="email"><a href="mailto:info@publicquest.co.uk" sudject="Web Enquiry">info@publicquest.co.uk</a></span><br>
            <abbr title="Phone">P:</abbr>  <span itemprop="telephone"><a href="tel:01612927374">(0161) 292 7374</a></span><br>
             <span itemprop="url" content="www.publicquest.co.uk"></span>
             <span itemprop="alternateName" content="PQL"></span>
             <span itemprop="description" content="A Manchester based Web Design Company"></span>
            <img itemprop="logo" content="images/PQL_banner_logo.jpg"/>

            <a href="https://google.com/+PQLWebSolutionsBredbury"
 rel="publisher">Find us on Google+</a>

          </div>  
        </div>


        <div class="visible-lg col-md-7">
          <iframe width="650" height="500" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="http://maps.google.com/maps?f=q&amp;source=s_q&amp;hl=en&amp;geocode=&amp;q=PQL+Web+Solutions,+Stockport+Road+West,+Bredbury,+Stockport,+UK&amp;aq=0&amp;oq=pql&amp;sll=53.418549,-2.126853&amp;sspn=0.026778,0.066047&amp;t=m&amp;ie=UTF8&amp;hq=&amp;hnear=&amp;ll=53.418141,-2.127831&amp;spn=0.006295,0.006295&amp;iwloc=A&amp;output=embed"></iframe><br /><small><a href="http://maps.google.com/maps?f=q&amp;source=embed&amp;hl=en&amp;geocode=&amp;q=PQL+Web+Solutions,+Stockport+Road+West,+Bredbury,+Stockport,+UK&amp;aq=0&amp;oq=pql&amp;sll=53.418549,-2.126853&amp;sspn=0.026778,0.066047&amp;t=m&amp;ie=UTF8&amp;hq=&amp;hnear=&amp;ll=53.418141,-2.127831&amp;spn=0.006295,0.006295&amp;iwloc=A" style="color:#0000FF;text-align:left">View Larger Map</a></small>
        </div>        
        <div class="visible-md col-md-7">
          <iframe width="600" height="500" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="http://maps.google.com/maps?f=q&amp;source=s_q&amp;hl=en&amp;geocode=&amp;q=PQL+Web+Solutions,+Stockport+Road+West,+Bredbury,+Stockport,+UK&amp;aq=0&amp;oq=pql&amp;sll=53.418549,-2.126853&amp;sspn=0.026778,0.066047&amp;t=m&amp;ie=UTF8&amp;hq=&amp;hnear=&amp;ll=53.418141,-2.127831&amp;spn=0.006295,0.006295&amp;iwloc=A&amp;output=embed"></iframe><br /><small><a href="http://maps.google.com/maps?f=q&amp;source=embed&amp;hl=en&amp;geocode=&amp;q=PQL+Web+Solutions,+Stockport+Road+West,+Bredbury,+Stockport,+UK&amp;aq=0&amp;oq=pql&amp;sll=53.418549,-2.126853&amp;sspn=0.026778,0.066047&amp;t=m&amp;ie=UTF8&amp;hq=&amp;hnear=&amp;ll=53.418141,-2.127831&amp;spn=0.006295,0.006295&amp;iwloc=A" style="color:#0000FF;text-align:left">View Larger Map</a></small>
        </div> 
        <div class="visible-sm col-sm-7"><iframe width="400" height="400" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://maps.google.co.uk/maps?ie=UTF8&amp;q=pql+websolutions&amp;fb=1&amp;gl=uk&amp;hq=&amp;hl=en&amp;hnear=&amp;t=m&amp;ll=53.418139,-2.127829&amp;spn=0.003836,0.006437&amp;z=16&amp;output=embed"></iframe><br /><small><a href="https://maps.google.co.uk/maps?ie=UTF8&amp;q=pql+websolutions&amp;fb=1&amp;gl=uk&amp;hq=&amp;hl=en&amp;hnear=&amp;t=m&amp;ll=53.418139,-2.127829&amp;spn=0.003836,0.006437&amp;z=16&amp;source=embed" style="color:#0000FF;text-align:left">View Larger Map</a></small></div>       
        <div class="visible-xs col-xs-12"><iframe width="280" height="280" class="center-block" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://maps.google.co.uk/maps?ie=UTF8&amp;q=pql+websolutions&amp;fb=1&amp;gl=uk&amp;hq=&amp;hl=en&amp;hnear=&amp;t=m&amp;ll=53.418139,-2.127829&amp;spn=0.003836,0.006437&amp;z=16&amp;output=embed"></iframe><br /><small><a href="https://maps.google.co.uk/maps?ie=UTF8&amp;q=pql+websolutions&amp;fb=1&amp;gl=uk&amp;hq=&amp;hl=en&amp;hnear=&amp;t=m&amp;ll=53.418139,-2.127829&amp;spn=0.003836,0.006437&amp;z=16&amp;source=embed" style="color:#0000FF;text-align:left">View Larger Map</a></small></div>
      </div>      

  <hr class="featurette-divider">


</div><!-- /.container -->


  <!-- FOOTER -->
<!---   <cfinclude template="footer.cfm">
 --->  <!--- /FOOTER --->


<!-- Include all compiled plugins (below), or include individual files as needed -->

<script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>

<!--- Bootstrap ---><script src="js/bootstrap.min.js"></script>
<!-- Less Compiler --><script src="js/less.js" type="text/javascript"></script>
<!--- Lightbox ---><script src="js/lightbox-2.6.min.js"></script>


<script type="text/javascript">
  $(document).ready(function () {

  $('#label_html_css').popover('toggle')

  // $('#main_container').scrollspy({ target: '#mysidenav' }) 

  $(".navbar-nav li a[href^='#']").on('click', function(e) {

  // prevent default anchor click behavior
  e.preventDefault();

  // store hash
  var hash = this.hash;

  // animate
  $('html, body').animate({
  scrollTop: $(this.hash).offset().top
  }, 500, function(){

  // when done, add hash to url
  // (default click behaviour)
  window.location.hash = hash;
  });

  });
  $("#mysidenav a[href^='#']").on('click', function(e) {

  // prevent default anchor click behavior
  e.preventDefault();

  // store hash
  var hash = this.hash;

  // animate
  $('html, body').animate({
  scrollTop: $(this.hash).offset().top
  }, 500, function(){

  // when done, add hash to url
  // (default click behaviour)
  window.location.hash = hash;
  });

  }); 
  $(".index_page a[href^='#']").on('click', function(e) {

  // prevent default anchor click behavior
  e.preventDefault();

  // store hash
  var hash = this.hash;

  // animate
  $('html, body').animate({
  scrollTop: $(this.hash).offset().top
  }, 500, function(){

  // when done, add hash to url
  // (default click behaviour)
  window.location.hash = hash;
  });

  }); 
  });
</script>

</body>
</html>