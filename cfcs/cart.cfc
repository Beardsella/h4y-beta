<cfcomponent>
		
<!--- get the cart info --->	
	<cffunction name="get_cart" access="public" returntype="query">
		
		<cfquery datasource="#application.dsn#" name="cart_info" cachedwithin="#createtimespan(0,0,0,0)#">
			SELECT A.price, A.vat, A.delivery, A.quantity, A.vat_exempt, A.prodid, B.vatexempt
            FROM shoppingcart A INNER JOIN products B ON A.prodid = B.id
			WHERE A.cartid = '#session.visitorid#'
		</cfquery>
		
		<cfreturn cart_info>
	</cffunction>
<!--- --->	

<!--- get the cart totals --->	

	<cffunction name="get_totals" access="public" returntype="struct">
	
		<cfinvoke component="cart" method="get_cart" returnvariable="cart_info">
		</cfinvoke>
		
		<cfset totals.goods = 0>
		<cfset totals.tvat = 0>
		
		<cfloop query="cart_info">
			<cfset linegoods = cart_info.quantity * cart_info.price>
			<cfset linevat = cart_info.quantity * cart_info.vat>
            
			<cfset totals.goods = totals.goods + linegoods>
		<!--- hear4you only add vat if customer NOT exempt AND product qualifies --->
		<cfif cart_info.vat_exempt eq 0 OR cart_info.vat_exempt eq 1 AND cart_info.vatexempt eq 0>	
			<cfset totals.tvat = totals.tvat + linevat>
        </cfif>
            
		</cfloop>
      
		<cfset totals.subtotal = totals.tvat+totals.goods>

<!--- get delivery cost --->
		<cfquery name="setdel" datasource="#application.dsn#">
			SELECT sum(delivery) AS shipping
            FROM shoppingcart
			WHERE cartid = '#session.visitorid#'
		</cfquery>

		<cfif IsDefined("setdel.shipping") AND setdel.shipping neq ''>
			<cfset totals.delivery = setdel.shipping>
        <cfelse>
			<cfset totals.delivery = 0>
        </cfif>
		
		<cfif IsDefined("session.client") AND session.client neq '' AND session.client neq 9999 AND cart_info.recordcount neq 0>
			<cfset totals.delivery = 11.75>
        </cfif>
        
		<cfif IsDefined("totals.subtotal") AND totals.subtotal neq ''>
			<cfset totals.total = totals.delivery + totals.subtotal>
        <cfelse>
			<cfset totals.total = 0>
        </cfif>
		
        <cfreturn totals>
	</cffunction>
<!--- --->
	
</cfcomponent>