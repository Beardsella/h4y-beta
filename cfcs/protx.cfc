<cfcomponent>
	<!--------------------------------------------------- 
	
		Created By: Paul Beardsell
		Date: 09/10/2007
		Version: 0.1
		
		Interfacing with PROTX VSP Server
					
	---------------------------------------------------->
	
	<cffunction access="public" name="initial_post" returntype="any" hint="Send the data to the PROTX servers to initiate the transaction">
		<!--------------------------------------------------- 
		
			A HTTPS post is sent from our servers to Protx, registering the transaction. 
			In response PROTX return a registration Status, further transaction identifiers 
			and a URL to which your site should redirect the customer. 
						
		---------------------------------------------------->
		
		<!--- Required but just use the default values --->
		<cfargument name="VPSProtocol" required="false" type="string" default="2.22">
		<cfargument name="TxType" required="false" type="string" default="PAYMENT">
		
		<!--- Other required variables --->
		<cfargument name="Vendor" required="true" type="string">
		<cfargument name="VendorTXCode" required="true" type="string" hint="This should be your own reference code to the transaction">
		<cfargument name="Amount" required="true" type="Numeric">
		<cfargument name="Currency" required="true" type="string" hint="Three-letter currency code Examples: GBP, EUR and USD">
		<cfargument name="Description" required="true" type="String" hint="Free text description of goods or services being purchased">
		<cfargument name="NotificationURL" required="true" type="String" hint="Callback URL to which Notification POSTs are sent">	
		
		<!--- Optional variables --->
		<cfargument name="BillingAddress" required="false" type="string" hint="If provided this information will populate the Billing Address edit box on the card input screens. This information is required if Address Verification (AVS) is used by your site.">
		<cfargument name="BillingPostCode" required="false" type="string" hint="If provided this information will populate the Billing Post Code edit box on the card input screens. This information is required if Address Verification (AVS) checks are on.">
		<cfargument name="DeliveryAddress" required="false" type="string" hint="Used for fraud screening">
		<cfargument name="DeliveryPostCode" required="false" type="string" hint="Used for fraud screening">
		<cfargument name="CustomerName" required="false" type="string" hint="The customer�s name will appear in the cardholder name box on the payment pages if provided.">
		<cfargument name="ContactNumber" required="false" type="string" hint="Used for fraud screening">
		<cfargument name="CustomerEmail" required="false" type="string" hint="Not used in the version, but possible in future.">
		<cfargument name="Basket" required="false" type="string" hint="You can use this field to supply details of the customers order. This information will be displayed to you in the VSP Admin screens.">
		
		<!--------------------------------------------------- 
		
			Additional Security
			AVS/CV2
			0 = If AVS/CV2 enabled then check them. If rules apply, use rules. (default)
			1 = Force AVS/CV2 checks even if not enabled for the account. If rules apply, use rules.
			2 = Force NO AVS/CV2 checks even if enabled on account.
			3 = Force AVS/CV2 checks even if not enabled for the account but DON�T apply any rules.
						
		---------------------------------------------------->
		<cfargument name="ApplyAVSCV2" required="false" type="Numeric" hint="Using this flag you can fine tune the AVS/CV2 checks and rule set you've defined at a transaction level. This is useful in circumstances where direct and trusted customer contact has been established and you wish to override the default security checks.">
		
		<!--------------------------------------------------- 
		
			Additional Security
			3D Secure
			0 = If 3D-Secure checks are possible and rules allow, perform the checks and apply the authorisation rules. (default)
			1 = Force 3D-Secure checks for this transaction if possible and apply rules for authorisation.
			2 = Do not perform 3D-Secure checks for this transaction and always authorise.
			3 = Force 3D-Secure checks for this transaction if possible but ALWAYS obtain an auth code, irrespective of rule base.
						
		---------------------------------------------------->	
		<cfargument name="Apply3DSecure" required="false" type="Numeric" hint="Using this flag you can fine tune the 3D Secure checks and rule set you've defined at a transaction level. This is useful in circumstances where direct and trusted customer contact has been established and you wish to override the default security checks.">

		<!--- Simulator/Test/Live transaction --->
		<!--------------------------------------------------- 
		
		1 = Simulator
		2 = Test
		3 = Live
						
		---------------------------------------------------->
		<cfargument name="transaction_type" required="false" default="1">
		
		<!--- Post URLs --->
		<cfargument name="post_sim_url" required="false" default="https://test.sagepay.com/Simulator/VSPServerGateway.asp?Service=VendorRegisterTx">
		<cfargument name="post_test_url" required="false" default="https://test.sagepay.com/gateway/service/vspserver-register.vsp">
		<cfargument name="post_live_url" required="false" default="https://live.sagepay.com/gateway/service/vspserver-register.vsp">
		
		<!--------------------------------------------------- 
		
			Create the HTTPS post
						
		---------------------------------------------------->
		<!--- What type of transaction is this? --->
		<cfif transaction_type EQ 3>
			<cfset post = post_live_url>
		<cfelseif transaction_type EQ 2>
			<cfset post = post_test_url>
		<cfelse>
			<cfset post = post_sim_url>
		</cfif>
		
		
		<!--------------------------------------------------- 
		
			Send the data to PROTX and receive their reply
						
		---------------------------------------------------->


		<cfhttp url="#post#" result="return_info" method="post">
			
			<!--- Required --->
			<cfhttpparam name="VPSProtocol" value="#VPSProtocol#" type="formfield">
			<cfhttpparam name="TxType" value="#TxType#" type="formfield">
			<cfhttpparam name="Vendor" value="#Vendor#" type="formfield">
			<cfhttpparam name="VendorTXCode" value="#VendorTXCode#" type="formfield">
			<cfhttpparam name="Amount" value="#Amount#" type="formfield">
			<cfhttpparam name="Currency" value="#Currency#" type="formfield">
			<cfhttpparam name="Description" value="#Description#" type="formfield">
			<cfhttpparam name="NotificationURL" value="#NotificationURL#" type="formfield">
			
			<!--- Optional --->
			<cfif parameterexists(BillingAddress)>
				<cfhttpparam name="BillingAddress" value="#BillingAddress#" type="formfield">
			</cfif>
			<cfif parameterexists(BillingPostCode)>
				<cfhttpparam name="BillingPostCode" value="#BillingPostCode#" type="formfield">
			</cfif>
			<cfif parameterexists(DeliveryAddress)>
				<cfhttpparam name="DeliveryAddress" value="#DeliveryAddress#" type="formfield">
			</cfif>
			<cfif parameterexists(DeliveryPostCode)>
				<cfhttpparam name="DeliveryPostCode" value="#DeliveryPostCode#" type="formfield">
			</cfif>
			<cfif parameterexists(CustomerName)>
				<cfhttpparam name="CustomerName" value="#CustomerName#" type="formfield">
			</cfif>
			<cfif parameterexists(ContactNumber)>
				<cfhttpparam name="ContactNumber" value="#ContactNumber#" type="formfield">
			</cfif>
			<cfif parameterexists(Description)>
				<cfhttpparam name="Description" value="#Description#" type="formfield">
			</cfif>
			<cfif parameterexists(CustomerEMail)>
				<cfhttpparam name="CustomerEMail" value="#CustomerEMail#" type="formfield">
			</cfif>
			
			<!--- Additional Security --->
			<cfif parameterexists(ApplyAVSCV2)>
				<cfhttpparam name="ApplyAVSCV2" value="#ApplyAVSCV2#" type="formfield">
			</cfif>
			<cfif parameterexists(Apply3DSecure)>
				<cfhttpparam name="Apply3DSecure" value="#Apply3DSecure#" type="formfield">
			</cfif>
			
		</cfhttp>

		<!-----------------------------------------
		
			Set the Return Status
			
		------------------------------------------>
		<cfset r_c = return_info.filecontent>
		<cfset out.r_c = r_c>
		
		<cfset status_start = find('Status=',r_c)>
		<cfset status_end = find('StatusDetail',r_c, status_start)>
		<cfset out.status =  mid(r_c,status_start + 7, status_end - status_start - 9)>
		
		
		<cfif out.status EQ 'OK'>
			
			<!-----------------------------------------
		
				If the return status is OK then set the other values
				
			------------------------------------------>

			<cfset VPSTxId_start = find('VPSTxId=',r_c)>
			<cfset VPSTxId_end = find('}',r_c, VPSTxId_start)>
			<cfset out.VPSTxId =  mid(r_c,VPSTxId_start + 8, VPSTxId_end - VPSTxId_start - 7)>
			
			<cfset SecurityKey_start = find('SecurityKey=',r_c)>
			<cfset SecurityKey_end = find('NextURL',r_c, SecurityKey_start)>
			<cfset out.SecurityKey =  mid(r_c,SecurityKey_start + 12, SecurityKey_end - SecurityKey_start - 14)>
			
			<cfset nexturl_start = find('NextURL=',r_c)>
			<cfset nexturl_end = find('}',r_c, nexturl_start)>
			<cfset out.nexturl =  mid(r_c,nexturl_start + 8, nexturl_end)>
			
		<cfelse>
		
			<!-----------------------------------------
		
				If the return status is NOT OK then set the error message
				
			------------------------------------------>
			<cfset StatusDetail_start = find('StatusDetail=',r_c)>
			<cfset out.StatusDetail =  mid(r_c,StatusDetail_start + 13, len(r_c))>
			<cfset out.arguments = arguments>
			
		</cfif>
		
		<!--------------------------------------------------- 
		
			Storing the returned variables as XML to use later
						
		---------------------------------------------------->
		<!---Dummy variables --->
		<!--- <cfset next_url = 'http://localhost:8300/protx/?stop=true'>
		<cfsavecontent variable="protx_xml">
			<cfoutput>
			
			<?xml version="1.0" encoding="UTF-8"?>
			<transaction id="#VendorTXCode#">
				<!--- <VPSTxId>
					#return_info.VPSTxId#
				</VPSTxId> --->
				<statuscode>
					#return_info.statuscode#
				</statuscode>
				<!--- <nexturl>
					#return_info.nexturl#
				</NextURL>
				<SecurityKey>
					#return_info.securitykey#
				</securitykey> --->
			</transaction>
			
			</cfoutput>
		
		</cfsavecontent>
		<cfset path = expandPath('payment_server_return/xml/')>
		<cffile action="write" file="#path##VendorTXCode#.xml" output="#protx_xml#"> --->
		
		<!--------------------------------------------------- 
		
			Now redirect the browser to the NextUrl location
			by passing the variable back to the calling page
						
		---------------------------------------------------->
		<cfreturn #out#>
	</cffunction>


	<cffunction access="public" name="notification_post" hint="Receive data back from PROTX when the transaction is complete">
	
	</cffunction>

</cfcomponent>