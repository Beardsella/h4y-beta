<!DOCTYPE html>
<html>
  <head>
    <title>Hear 4 You</title>
    <LINK REL="SHORTCUT ICON" HREF="images/h4y.ico" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    
    <meta name="Keywords" content="best hearing aids, nhs hearing aids, hearing aid, hearing aids prices, digital hearing aids" />
  
    <meta name="Description" content="We Build Websites and online applications. We specialse in developing bussiness managment systems and E Commerce sites but offer a comprehensive range of web services and solutions. We work together with our clients using our experience and help you plan, build and maintain you digital strategy." />


    
    <cfif find("localhost", "#CGI.http_host#")>
      
      <!-- LESS -->
      <link href="css/bootstrap.less" rel="stylesheet">
      <link href="css/custom.less" rel="stylesheet">  
      <link href="css/products-list-grid.less" rel="stylesheet">  

    <cfelse>

      <!-- CSS -->
      <link href="css/bootstrap.css" rel="stylesheet">
      <link href="css/custom.css" rel="stylesheet">  

    </cfif>




  <cfquery datasource="#application.DSN#" name="get_top_cats">
    SELECT *
    FROM categories
    WHERE parentid = 1 AND display = 1 AND id <> 22
    ORDER BY display_order
  </cfquery>

  </head>
  <body>


        <!-- Marketing messaging and featurettes
    ================================================== -->
    <!-- Wrap the rest of the page in another container to center all the content. -->

    <div class="container col-lg-1 visible-lg">
    <div class="space_90px"></div>

<cfinclude template="side_nav.cfm">
      </div>
    <div class="container col-lg-11" data-spy="scroll" data-target="#mysidenav" id="main_container">
        <cfinclude template="nav.cfm">

<div class="space_30px" id="home"></div>

<div class="row">

    <!-- Carousel
    ================================================== -->
    <div id="myCarousel" class="carousel slide hidden-xs col-lg-12" data-ride="carousel">
      <!-- Indicators -->
      <ol class="carousel-indicators">
        <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
        <li data-target="#myCarousel" data-slide-to="1"></li>
        <li data-target="#myCarousel" data-slide-to="2"></li>
        <li data-target="#myCarousel" data-slide-to="3"></li>
      </ol>
      <div class="carousel-inner">
        <div class="item active mycarousel-item">
         <div class="container">
         <div class="space_50px"></div>
                   <img src="images/PQL-banner.jpg" alt="PQL web design banner" class="img-responsive center-block"> 

            <div class="carousel-caption">
             </div>
          </div>
        </div>
        <div class="item mycarousel-item">
         <div class="container">
         <div class="space_50px"></div>
                   <img src="images/test-slide-1.jpg" alt="PQL web design banner" class="img-responsive center-block"> 

            <div class="carousel-caption">
             </div>
          </div>
        </div>
        <div class="item mycarousel-item">
         <div class="container">
         <div class="space_50px"></div>
                   <img src="images/test-slide-2.jpg" alt="PQL web design banner" class="img-responsive center-block"> 

            <div class="carousel-caption">
            </div>
          </div>
        </div>
        <div class="item mycarousel-item">
         <div class="container">
         <div class="space_50px"></div>
                   <img src="images/mobile-devices-slide.jpg" alt="PQL web design banner" class="img-responsive center-block"> 

            <div class="carousel-caption">
            </div>
          </div>
        </div>

      </div>
      <a class="left carousel-control" href="#myCarousel" data-slide="prev"><span class="glyphicon glyphicon-chevron-left my_bg_circle"></span></a>
      <a class="right carousel-control" href="#myCarousel" data-slide="next"><span class="glyphicon glyphicon-chevron-right my_bg_circle"></span></a>
    </div><!-- /.carousel -->

</div>
<div class="space_30px"></div>


      <hr class="featurette-divider"  id="products">

          <div id="products_box" class="row list-group">


<cfoutput query="get_top_cats">
        <div class="item col-xs-12 col-sm-6 col-md-4 col-lg-3">
            <div class="thumbnail">
                <div class="caption">
                    <h4 class="group inner list-group-item-heading">
                        <span class="captalize">#URLDecode(cat_name)#</span></h4>
                        <div class="space_20px"></div>
                    <img src="category_images/thumbs/#cat_img#" class="img-responsive" style="margin:auto;">
                      <div class="space_20px"></div>
                    <p class="group inner list-group-item-text">
                        #URLDecode(cat_description)#</p>
                    <div class="row">
                        <div class="col-xs-12 col-md-12 col-lg-12">

                          <cfif isDefined("SEO_url")>
                              <a href="#SEO_url#" type="button" class="btn btn-primary btn-block">More #URLDecode(cat_name)#</a>
                          <cfelse>
                              <a href="index.cfm" type="button" class="btn btn-primary btn-block">More #URLDecode(cat_name)#</a>
                          </cfif>

                        </div>
                    </div>
                </div>
            </div>
        </div>
</cfoutput>

</div>

      <hr class="featurette-divider">

<div class="space_75px"></div>
      <!-- Three columns of text below the carousel -->
      <div class="row">

        <div class="col-xs-6 col-sm-4 col-md-4 index_page">
          <a href="#what-we-do"><img class="img-circle img-responsive porthole_hover"  src="images/pql-coding.jpg" alt="Bespoke Code"></a>
          <h2>What We Do</h2>
          <p>We Build Websites and online applications. We specialse in developing bussiness managment systems and E Commerce sites but offer a comprehensive range of web services and solutions. We work together with our clients using our experience and help you plan, build and maintain you digital strategy.</p>
          <p><a class="btn btn-primary" href="#our_services" role="button">Our Services</a></p>
        </div><!-- /.col-lg-4 -->

        <div class="col-xs-6 col-sm-4 col-md-4 index_page">
          <a href="#contact_details"><img class="img-circle img-responsive porthole_hover"  src="images/call-pql.jpg" alt="Call PQL to find out more"></a>
          <h2>Contact Us</h2>
          <p>Not sure about the best direction to go in or maybe a technial question? Email or give us a call and one of our developers can help and advise. <br>

          <div itemscope itemtype="http://schema.org/Organization"> 
             <span itemprop="name" content="PQL Web Solutions"></span>
            <p><abbr title="Email">E:</abbr> <span itemprop="email"><a href="mailto:info@publicquest.co.uk" sudject="Web Enquiry">info@publicquest.co.uk</a></span><br>
            <abbr title="Phone">P:</abbr>  <span itemprop="telephone"><a href="tel:01612927374">(0161) 292 7374</a></span></p>
             <span itemprop="alternateName" content="PQL"></span>
             <span itemprop="description" content="A Manchester based Web Design Company"></span>
          </div>  
          
          <p><a class="btn btn-success" data-toggle="modal" data-target="#contact_modal">Contact Me, Lets talk Business</a></p>
          <p><a class="btn btn-primary" href="#contact_details" role="button">Contact PQL</a></p>


                <!-- contact_modal -->
        <div class="modal fade" id="contact_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">Tell Us More...</h4><br>
                <p>Fill in the contact form below and one of our coders will get back to you ASAP!</p>
                <p>Or you can <a href="mailto:info@publicquest.co.uk">Click here</a> and send us a standard email</p>
              </div>
              <div class="modal-body">
            <form action="send_enquiry.cfm" method="post" class="form" role="form">
            <div class="row">
                <div class="col-xs-6 col-md-6">
                    <input class="form-control" name="firstname" placeholder="Name" type="text"
                        autofocus />
                </div>
                <div class="col-xs-6 col-md-6">
                    <input class="form-control" name="lastname" placeholder="Other Name" type="text" />
                </div>
            </div><div class="input-group">
  <span class="input-group-addon">http://www.</span>
            <input class="form-control" name="website" placeholder="Website" type="text" /></div>
            <input class="form-control" name="youremail" placeholder="Your Email" type="email" />
            <input class="form-control" name="phone" placeholder="Phone Number" type="tel" />
            <p class="form-control">I would prefer to be contacted Via: 
            <label class="radio-inline">
            <input type="radio" name="contact_pref" id="inlineCheckbox1" value="phone" />
                Phone</label>
            <label class="radio-inline">
            <input type="radio" name="contact_pref" id="inlineCheckbox2" value="email" />
                Email</label></p>
             
            <textarea class="form-control" name="message" placeholder="Im intrested in...." cols="30" rows="10"></textarea>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
                <button type="submit" class="btn btn-success">Submit</button>
                </form>

              </div>
            </div><!-- /.modal-content -->
          </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->
            
        </div><!-- /.col-lg-4 -->

        <div class="col-xs-12 col-sm-4 col-md-4 index_page porthole_hover">
          <a href="#about_us"><img class="img-circle img-responsive" src="images/PQL_stickmen_coding.jpg" alt="PQL stick men coding"></a>
          <h2>Who We Are</h2>
          <p>We are a small dedicated team of specilist coders located in Stockport, Manchester. We have decades of experience in Bespoke Web Design & Database contruction. Normaly you'll find our coders in the office speaking to clients and building custom web applications.</p>
          <p><a class="btn btn-primary" href="#who-we-are" role="button">About PQL</a></p>
        </div><!-- /.col-lg-4 -->

      </div><!-- /.row -->

      <!-- START THE FEATURETTES -->

      <hr class="featurette-divider" id="who-we-are">


      <div class="row featurette">
        <div class="col-xs-12 col-sm-8 col-md-7">
          <h2 class="featurette-heading hidden-md">Who We Are <span class="text-muted small">Under Paid Rockstars...</span></h2>
          <h2 class="visible-md">Who We Are <span class="text-muted small">Under Paid Rockstars...</span></h2>
          <p class="lead">A small team of specilist coders located in Stockport, Manchester. For over a decade we have created custom web applications for businesses, providing advice and support whenever needed.</p>
          <p class="lead">We've a simple philosophy base on providing a honest friendly service with great support. We also love a new challange! If we dont already know the answer, we will go and find out!</p>
        </div>
        <div class="col-xs-12 col-sm-4 col-md-5">
          <div class="visible-sm space_90px"></div>
          <img class="featurette-image img-responsive" src="images/jack-pql.jpg" alt="Jack at PQL">
        </div>
      </div>

      <hr class="featurette-divider" id="what-we-do">

      <div class="row featurette">
        <div class="col-md-5 col-sm-4 hidden-xs">
                    <div class="visible-sm space_90px"></div>

          <img class="featurette-image img-responsive" src="images/pql-code.jpg" alt="Abstract code">
        </div>
        <div class="col-xs-12 col-md-7 col-sm-8">
          <h2 class="featurette-heading hidden-md">What Do We Do? <span class="text-muted small">Bespoke Coding</span></h2>
          <h2 class="visible-md">What Do We Do? <span class="text-muted small">Bespoke Coding</span></h2>
          <p class="lead">We specialse in developing bussiness managment systems and E-Commerce sites but offer a comprehensive range of web services and solutions.</p><p class="lead">To Deliver this we work closly together with our clients to understand your goals, this enables us to help create the best digital strategy targeted to engage your audience.</p>
          <p class="lead">Not only that but offer support for the site beyond development throughout it's life. With support and advice form our coders only a phone call away. <a href="#contact_details">Call now</a> and dissuss your plans or needs with us.</p>
        </div>
        <div class="col-xs-12 visible-xs">
          <img class="featurette-image img-responsive" src="images/pql-code.jpg" alt="Abstract code">
        </div>
      </div>

            <hr class="featurette-divider" id="contact_details">

      <div class="row featurette">
        <div class="col-xs-12 col-md-5 col-sm-5">
          <h2 class="featurette-heading">Our Contact <span class="text-muted">Details.</span></h2>
          <p class="lead">Our office is located just outside Manchester between J25 & J26 on the M60.</p>

          <br>

          <div itemscope itemtype="http://schema.org/Organization"> 
             <strong><span itemprop="name">PQL Web Solutions</span></strong>
             <div itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">
                <span itemprop="streetAddress">716 Stockport Road West</span><br>
                <span itemprop="addressLocality">Stockport</span><br>
                <span itemprop="addressRegion">Manchester</span><br>
                 <span itemprop="postal-code">SK6 2EE</span>
              </div>
            <abbr title="Email">E:</abbr> <span itemprop="email"><a href="mailto:info@publicquest.co.uk" sudject="Web Enquiry">info@publicquest.co.uk</a></span><br>
            <abbr title="Phone">P:</abbr>  <span itemprop="telephone"><a href="tel:01612927374">(0161) 292 7374</a></span><br>
             <span itemprop="url" content="www.publicquest.co.uk"></span>
             <span itemprop="alternateName" content="PQL"></span>
             <span itemprop="description" content="A Manchester based Web Design Company"></span>
            <img itemprop="logo" content="images/PQL_banner_logo.jpg"/>

            <a href="https://google.com/+PQLWebSolutionsBredbury"
 rel="publisher">Find us on Google+</a>

          </div>  
        </div>


        <div class="visible-lg col-md-7">
          <iframe width="650" height="500" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="http://maps.google.com/maps?f=q&amp;source=s_q&amp;hl=en&amp;geocode=&amp;q=PQL+Web+Solutions,+Stockport+Road+West,+Bredbury,+Stockport,+UK&amp;aq=0&amp;oq=pql&amp;sll=53.418549,-2.126853&amp;sspn=0.026778,0.066047&amp;t=m&amp;ie=UTF8&amp;hq=&amp;hnear=&amp;ll=53.418141,-2.127831&amp;spn=0.006295,0.006295&amp;iwloc=A&amp;output=embed"></iframe><br /><small><a href="http://maps.google.com/maps?f=q&amp;source=embed&amp;hl=en&amp;geocode=&amp;q=PQL+Web+Solutions,+Stockport+Road+West,+Bredbury,+Stockport,+UK&amp;aq=0&amp;oq=pql&amp;sll=53.418549,-2.126853&amp;sspn=0.026778,0.066047&amp;t=m&amp;ie=UTF8&amp;hq=&amp;hnear=&amp;ll=53.418141,-2.127831&amp;spn=0.006295,0.006295&amp;iwloc=A" style="color:#0000FF;text-align:left">View Larger Map</a></small>
        </div>        
        <div class="visible-md col-md-7">
          <iframe width="600" height="500" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="http://maps.google.com/maps?f=q&amp;source=s_q&amp;hl=en&amp;geocode=&amp;q=PQL+Web+Solutions,+Stockport+Road+West,+Bredbury,+Stockport,+UK&amp;aq=0&amp;oq=pql&amp;sll=53.418549,-2.126853&amp;sspn=0.026778,0.066047&amp;t=m&amp;ie=UTF8&amp;hq=&amp;hnear=&amp;ll=53.418141,-2.127831&amp;spn=0.006295,0.006295&amp;iwloc=A&amp;output=embed"></iframe><br /><small><a href="http://maps.google.com/maps?f=q&amp;source=embed&amp;hl=en&amp;geocode=&amp;q=PQL+Web+Solutions,+Stockport+Road+West,+Bredbury,+Stockport,+UK&amp;aq=0&amp;oq=pql&amp;sll=53.418549,-2.126853&amp;sspn=0.026778,0.066047&amp;t=m&amp;ie=UTF8&amp;hq=&amp;hnear=&amp;ll=53.418141,-2.127831&amp;spn=0.006295,0.006295&amp;iwloc=A" style="color:#0000FF;text-align:left">View Larger Map</a></small>
        </div> 
        <div class="visible-sm col-sm-7"><iframe width="400" height="400" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://maps.google.co.uk/maps?ie=UTF8&amp;q=pql+websolutions&amp;fb=1&amp;gl=uk&amp;hq=&amp;hl=en&amp;hnear=&amp;t=m&amp;ll=53.418139,-2.127829&amp;spn=0.003836,0.006437&amp;z=16&amp;output=embed"></iframe><br /><small><a href="https://maps.google.co.uk/maps?ie=UTF8&amp;q=pql+websolutions&amp;fb=1&amp;gl=uk&amp;hq=&amp;hl=en&amp;hnear=&amp;t=m&amp;ll=53.418139,-2.127829&amp;spn=0.003836,0.006437&amp;z=16&amp;source=embed" style="color:#0000FF;text-align:left">View Larger Map</a></small></div>       
        <div class="visible-xs col-xs-12"><iframe width="280" height="280" class="center-block" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://maps.google.co.uk/maps?ie=UTF8&amp;q=pql+websolutions&amp;fb=1&amp;gl=uk&amp;hq=&amp;hl=en&amp;hnear=&amp;t=m&amp;ll=53.418139,-2.127829&amp;spn=0.003836,0.006437&amp;z=16&amp;output=embed"></iframe><br /><small><a href="https://maps.google.co.uk/maps?ie=UTF8&amp;q=pql+websolutions&amp;fb=1&amp;gl=uk&amp;hq=&amp;hl=en&amp;hnear=&amp;t=m&amp;ll=53.418139,-2.127829&amp;spn=0.003836,0.006437&amp;z=16&amp;source=embed" style="color:#0000FF;text-align:left">View Larger Map</a></small></div>
      </div>

      <hr class="featurette-divider">

      <!-- /END THE FEATURETTES -->


      <!-- FOOTER -->
      <footer>
        <p class="pull-right"><a href="#home">Back to top</a></p>
        <p>Public Quest Limited &copy; 2014</p>
      </footer>

    </div><!-- /.container -->



<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="https://code.jquery.com/jquery.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="js/bootstrap.min.js"></script>
<!-- Less compiler -->
<script src="js/less.js" type="text/javascript"></script>

<script src="js/holder-js/holder.js"></script>

<script type="text/javascript">
    $(document).ready(function () {
    
    $('#label_html_css').popover('toggle')
    
    // $('#main_container').scrollspy({ target: '#mysidenav' }) 

    $(".navbar-nav li a[href^='#']").on('click', function(e) {

   // prevent default anchor click behavior
   e.preventDefault();

   // store hash
   var hash = this.hash;

   // animate
   $('html, body').animate({
       scrollTop: $(this.hash).offset().top
     }, 500, function(){

       // when done, add hash to url
       // (default click behaviour)
       window.location.hash = hash;
     });

});
    $("#mysidenav a[href^='#']").on('click', function(e) {

   // prevent default anchor click behavior
   e.preventDefault();

   // store hash
   var hash = this.hash;

   // animate
   $('html, body').animate({
       scrollTop: $(this.hash).offset().top
     }, 500, function(){

       // when done, add hash to url
       // (default click behaviour)
       window.location.hash = hash;
     });

}); 
        $(".index_page a[href^='#']").on('click', function(e) {

   // prevent default anchor click behavior
   e.preventDefault();

   // store hash
   var hash = this.hash;

   // animate
   $('html, body').animate({
       scrollTop: $(this.hash).offset().top
     }, 500, function(){

       // when done, add hash to url
       // (default click behaviour)
       window.location.hash = hash;
     });

}); 
     });
</script>
<script type="text/javascript">
$(function(){
    $("#mysidenav").scrollspy();
});
</script>

  </body>
</html>

