<div class="clearfix"></div>

<footer id="footer">

<cfquery name="footer_carousel" datasource="#application.DSN#">
    SELECT DISTINCT product_id
    FROM product_images
    WHERE image_name <> ""
    ORDER BY img_order
</cfquery>

<div class="container hidden-xs">
	<div class="row">
		<div class="col-sm-12 col-md-12 col-lg-12">
                <div id="myFooterCarousel" class="carousel slide">

                <!-- Carousel items -->
                <div class="carousel-inner">
<div class="item active">
    <div class="row-fluid">

<cfset footer_img_count = 0>

        <cfoutput query="footer_carousel">

            <cfif footer_img_count eq 4>
                <cfset footer_img_count = 0>

                </div><!--/row-fluid-->
             </div><!--/item-->

             <div class="item">
                <div class="row-fluid">

            </cfif>

        <cfset footer_img_count = #footer_img_count# + 1>

    <cfquery name="footer_image" datasource="#application.DSN#">
        SELECT *
        FROM product_images
        WHERE product_id = #product_id#
    </cfquery>

    <cfquery datasource="#application.DSN#" name="getpage">
        SELECT * 
        FROM Products
        WHERE id = #footer_carousel.product_id#
    </cfquery>    

    <cfquery name="children" datasource="#application.dsn#">
        SELECT *
        FROM child_products 
        WHERE product_id = #footer_carousel.product_id#
        ORDER BY ID ASC
    </cfquery>
                    <form action="cart_add.cfm" method="post" name="form3">

<input type="hidden" name="qty#children.id#" type="text" value="1" size="2" >
          <div class="col-sm-3 col-md-3 col-lg-3 thumbnail" style="margin:10px;">
                <div class="caption">
                    <h4>#URLDecode(getpage.title)#</h4>
                    <div class="space_10px"></div> 
                    <p>#URLDecode(getpage.thumb_description)#</p>
                    <p><a href="#URLDecode(getpage.SEO_URL)#" type="button" class="btn btn-primary">More Info</a>
                    <a href="javascript:child_cart_go(#getpage.id#, 9999)" type="button" class="btn btn-success">Add to Cart <span class="glyphicon glyphicon-shopping-cart"></span></a></p>
                </div>
                <img src="product-images/thumbs/#footer_image.image_name#" style="max-width:230px; padding:4px;" alt="#URLDecode(footer_image.image_caption)#">
            </div>
</form>
         </cfoutput>

	</div><!--/row-fluid-->
 </div><!--/item-->

    </div><!--/carousel-inner-->
                 
                <a class="left carousel-control" href="#myFooterCarousel" data-slide="prev"> <span class="glyphicon glyphicon-chevron-left my_footer_circle"></span></a>
                <a class="right carousel-control" href="#myFooterCarousel" data-slide="next"> <span class="glyphicon glyphicon-chevron-right my_footer_circle"></span></a>
                </div><!--/myFooterCarousel-->
    </div>
        </div>
<div>
    <p class="pull-right" style="margin:20px;"><a href="#top">Back to top</a></p>
    <p style="margin:20px;">Hearing Products International &copy; 2014</p>
</div>                 
</div>





</footer>
