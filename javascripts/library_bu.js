function check_field(type, id){
	var result = true
	if(type == 'num'){
		lastChar = $F(id).charAt($F(id).length-1)
		if(isNaN(lastChar)){
			if(lastChar != '.'){
				$(id).value = $(id).value.substring(0, $F(id).length-1)
				result = false
				return result
				
			}
			else{
				result = true
				return result	
			}
		}
	}
	else if(type == 'email'){
		if($F(id).length !== 0){
			if($F(id).search('@') == -1){
				alert('This must contain an email address')
				$(id).style.border = '1px solid red'
				return false
			}
			else{
				$(id).style.border = ''
				return true
			}
		}
		else{
			$(id).style.border = ''

			return true
		}
	}
}

////////////////////////
function blind(ele){
	Effect.toggle(ele,'slide', {duration:0.5})
}
///////////////////////
function validate_form(id, form, submit_frm){
	var submit_frm = (submit_frm == null) ? true : submit_frm;
	var id_array = id.split("|");
	var valid = true
	var id_array_l = id_array.length
	for(i=0; i <id_array_l; i++){
		var fld = id_array[i]
		var val = $(fld).value
		if(val == '' || val == null){
			$(fld).style.border = '1px solid red'
			valid = false
		}
		else{
			$(fld).style.border = '1px solid green'
		}
	}
	if(!valid){
		alert('Please fill in required fields')
		return false
	}
	else{
		if(submit_frm){
			$(form).submit()
		}
		else{
			return true
		}
		
	}
}
///////////////////////
function rand_no(){
	var rand  = parseInt(Math.random()*99999);
	return rand	
}
 // register event listeners on the Ajax requests to show/hide the processing indicator
 Ajax.Responders.register({
    onCreate: function() {
  if (Ajax.activeRequestCount === 1) {
    $('loading').show();
   }
    },
    onComplete: function() {
   if (Ajax.activeRequestCount === 0) {
    $('loading').hide();
   }
    }
 });
 
 /////////////////////
function create_timeout(time, function_name){
	timeout = window.setTimeout(function_name, time)
}
//////////////////////
function display_added(){
	blind('added')
	create_timeout(3000, "blind('added')")
}


/******************************************
 * Tabs for scriptaculous
 */
/*-----------------------------------------------------------
    Toggles element's display value
    Input: any number of element id's
    Output: none 
    ---------------------------------------------------------*/
function toggleDisp() {
    for (var i=0;i<arguments.length;i++){
        var d = $(arguments[i]);
        if (d.style.display == 'none')
            d.style.display = 'block';
        else
            d.style.display = 'none';
    }
}
/*-----------------------------------------------------------
    Toggles tabs - Closes any open tabs, and then opens current tab
    Input:     1.The number of the current tab
                    2.The number of tabs
                    3.(optional)The number of the tab to leave open
                    4.(optional)Pass in true or false whether or not to animate the open/close of the tabs
    Output: none 
    ---------------------------------------------------------*/
function toggleTab(num,numelems,opennum,animate) {
    if ($('tabContent'+num).style.display == 'none'){
        for (var i=1;i<=numelems;i++){
            if ((opennum == null) || (opennum != i)){
                var temph = 'tabHeader'+i;
                var h = $(temph);
                if (!h){
                    var h = $('tabHeaderActive');
                    h.id = temph;
                }
                var tempc = 'tabContent'+i;
                var c = $(tempc);
                if(c.style.display != 'none'){
                    if (animate || typeof animate == 'undefined')
                        Effect.toggle(tempc,'blind',{duration:0.5, queue:{scope:'menus', limit: 3}});
                    else
                        toggleDisp(tempc);
                }
            }
        }
        var h = $('tabHeader'+num);
        if (h)
            h.id = 'tabHeaderActive';
        h.blur();
        var c = $('tabContent'+num);
        c.style.marginTop = '2px';
        if (animate || typeof animate == 'undefined'){
            Effect.toggle('tabContent'+num,'blind',{duration:0.5, queue:{scope:'menus', position:'end', limit: 3}});
        }else{
            toggleDisp('tabContent'+num);
        }
    }
}

function clear_search(){
	if($F('search_string') == 'Search...'){
		$('search_string').value = ''	
	}
}

/////////////////////

Ajax.Responders.register({
	onCreate: function() {
		if($('notification') && Ajax.activeRequestCount > 0)
			Effect.Appear('notification',{duration: 0.25, queue: 'end'});
	},
	onComplete: function() {
		if($('notification') && Ajax.activeRequestCount == 0)
			Effect.Fade('notification',{duration: 0.25, queue: 'end'});
	}
});
