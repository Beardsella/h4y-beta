/* --------------------------------------------------------------------------------------------------------------
*	API:			JSMX (JavaScript MX) - This is a ColdFusion api for AJAX, using no XML but instead returning raw JavaScript.	
*	AUTHOR: 		Todd Kingham
*	EMAIL: 			todd@lalabird.com
*	CREATED:		8.21.2005
*	VERSION:		2.0.1
*	DESCRIPTION:	This API uses XMLHttpRequest to post/get data from a ColdFusion interface.
*					The CFC's/CFM's will return a string representation of a JS variable: response_param.
*					The "onreadystatechange event handler" will eval() the string into a JS variable 
*					and pass the value back to the "return function". To Download a full copy of the sample 
					application visit: http://www.lalabird.com/JSMX/?fa=JSMX.downloads
*
*
*	LICENSE:		THIS IS A OPEN SOURCE API. YOU ARE FREE TO USE THIS API IN ANY APPLICATION,
*               	TO COPY IT OR MODIFY THE FUNCTIONS FOR YOUR OWN NEEDS, AS LONG THIS HEADER INFORMATION
*              	 	REMAINS IN TACT AND YOU DON'T CHARGE ANY MONEY FOR IT. USE THIS API AT YOUR OWN
*               	RISK. NO WARRANTY IS EXPRESSED OR IMPLIED, AND NO LIABILITY ASSUMED FOR THE RESULT OF
*               	USING THIS API.
*
*               	THIS API IS LICENSED UNDER THE CREATIVE COMMONS ATTRIBUTION-SHAREALIKE LICENSE.
*               	FOR THE FULL LICENSE TEXT PLEASE VISIT: http://creativecommons.org/licenses/by-sa/2.5/
*
-----------------------------------------------------------------------------------------------------------------*/
/* UNCOMMENT THE FOLLOWING LINE IF YOU WILL BE RETURNING QUERY OBJECTS. (note: you may need to point the SRC to an alerternate location.*/
//document.writeln('<SCRIPT TYPE="text/javascript" LANGUAGE="JavaScript" SRC="/CFIDE/scripts/wddx.js"></SCRIPT>');

// perform the XMLHttpRequest();
function http(mthd,url,rm,qry) {
    //reference our arguments
	return_method = rm;
	if(!qry) qry = '';
	qryStr = toQueryString(qry);
	try{//this should work for most modern browsers excluding: IE Mac
		req = ( window.XMLHttpRequest ) ? new XMLHttpRequest() : new ActiveXObject("Microsoft.XMLHTTP") ;
		req.onreadystatechange = processReqChange;
		req.open(mthd, noCache(url), true);
			if(mthd.toLowerCase() == 'post')	
			req.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
		req.send(qryStr);
	}catch(e){//a browser not equiped to handle XMLHttp
		//alert("There was a problem retrieving the data:");
	}
}

// handle onreadystatechange event of req object
function processReqChange(){
 	switch(req.readyState){
		case 1: break;
		case 2: break;
		case 3: break;
		case 4:
			if (req.status == 200)// only if "OK"
				return_method(response2Obj(req.responseText));
	        else 
				alert("There was a problem retrieving the data:\n" + req.statusText);
	        break;
	}
}


// HELPER FUNCTIONS
function response2Obj(str){
	eval("var "+str);
	return eval(firstWord(str));
}

function firstWord(str){
	return str.substring(str.search(/\S/g)).split(' ')[0];
}

function toQueryString(obj){
	//determine the variable type
	if(typeof(obj) == 'string')
		return obj;
	if(typeof(obj) == 'object'){
		if(typeof(obj.elements) == 'undefined')//It's an Object()!
			return object2queryString(obj);
		else //It's a form!
			return form2queryString(obj);
	}	
}

function object2queryString(obj){
	var ar = new Array();
	for(x in obj) ar[ar.length] = x+'='+obj[x];
	return ar.join('&');
}

function form2queryString(form){
	var obj = new Object();
	var ar = new Array();
	for(var i=0;i<form.elements.length;i++){
		try {
			elm = form.elements[i];
			nm = elm.name;
			if(nm != ''){
				switch(elm.type.split('-')[0]){
					case "select":
						for(var s=0;s<elm.options.length;s++){
							if(elm.options[s].selected){
								if(typeof(obj[nm]) == 'undefined') obj[nm] = new Array();
								obj[nm][obj[nm].length] = escape(elm.options[s].value);
							}	
						}
						break;
					
					case "radio":
						if(elm.checked){
							if(typeof(obj[nm]) == 'undefined') obj[nm] = new Array();
							obj[nm][obj[nm].length] = escape(elm.value);
						}	
						break;
					
					case "checkbox":
						if(elm.checked){
							if(typeof(obj[nm]) == 'undefined') obj[nm] = new Array();
							obj[nm][obj[nm].length] = escape(elm.value);
						}	
						break;
					
					default:
						if(typeof(obj[nm]) == 'undefined') obj[nm] = new Array();
						obj[nm][obj[nm].length] = escape(elm.value);
						break;
				}
			}
		}catch(e){}
	}
	for(x in obj) ar[ar.length] = x+'='+obj[x].join(',');
return ar.join('&');
}



//IE likes to cache so we will fix it's wagon!
function noCache(url){
	var qs = new Array();
	var arr = url.split('?');
	var scr = arr[0];
	if(arr[1]) qs = arr[1].split('&');
	qs[qs.length]='nocache='+new Date().getTime();
return scr+'?'+qs.join('&');
}