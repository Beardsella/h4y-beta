<cfparam name="SESSION.sessionID" default="1">
<!--- get the right record in case customer went through checkout twice --->
<cfquery name="thiscust" datasource="#application.dsn#">
SELECT max(custid) AS cust FROM customerinfo WHERE customerid = '#SESSION.visitorid#'
</cfquery>

<cfquery name="getaddress" datasource="#application.dsn#">
SELECT * FROM customerinfo WHERE custid = #thiscust.cust#
</cfquery>

<!--- get the terms of sale for customer to accept --->
<cfquery name="get_terms" datasource="#application.DSN#">
SELECT page_content FROM page_text WHERE id = 6
</cfquery>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"><html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/templates/HPIL.dwt.cfm" codeOutsideHTMLIsLocked="false" -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<title>Hearing Aids and Speech Amplification</title>


<link href="CSS/HPIL.css" rel="stylesheet" type="text/css" media="all">
<link href="CSS/HPIL_nav.css" rel="stylesheet" type="text/css" media="all">


<cfparam name="session.cathide" default="0">

<script language="javascript" src="javascripts/engine.js"></script>
<script language="javascript" src="javascripts/scriptaculous.js"></script>
<script language="javascript" src="javascripts/library.js"></script>
<script type="text/JavaScript">
<!--
function showsubs(cat, clink){ 
	param = new Object();
	param.cat = cat
	param.clink = clink
	http( 'POST'  , 'aj_showsubs.cfm' , nav_response, param ); 
}
function nav_response(obj){ 
	var thiscat = obj.thiscat
	var navlink = obj.navlink
		document.getElementById('subs'+thiscat).style.display = ''
	location.href = navlink
	var catstoreset = obj.catstoreset
	var catarrReset = catstoreset.split( "," );
	for (var i=0; i < catarrReset.length; i++) {
			document.getElementById('subs'+catarrReset[i]).style.display = 'none'
		}
}
function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
//-->
</script>

<!--- check the cart value --->
<cfinvoke 
	component="cfcs.cart"
	method="get_totals"
	returnvariable="totals">
</cfinvoke>
</head>

<body onload="MM_preloadImages('images/brochure_link_over.gif','images/pay_now_over.gif')">
<div id="container">
	<div id="masthead"><cfinclude template="header.cfm"></div>
	<div class="clearline"></div>
    <div id="leftcolumn">
		<div id="leftnav">
                    <cfinclude template="nav.cfm">
  		</div>
		<div id="join_list">
                    <cfinclude template="mailing_list.cfm">
		</div>
        <div class="gofloat_nowidth" style="margin-bottom:20px">
        <a href="product_pdfs/Hearing_Products_ISO9001_Certificate.pdf" target="_blank">
        <img src="images/ISO_9001.gif" width="160" alt="We are ISO9001 accredited" border="0" />
        </a>
        </div>
  <div id="fsb">
        <a href="http://www.fsb.org.uk/" target="_blank"><img src="images/FSB_logo.gif" alt="View the FSB website" border="0" /></a>
  </div>
<img src="images/credit_cards.gif" width="128" height="80" alt="We accept online payments" />
</div>
	<div id="pagecontent">
			
                
            <form action="sagepay_send.cfm" id="check_terms" name="check_terms" method="post">
            <table width="92%" border="0" cellpadding="1" cellspacing="0">
              <cfoutput> 
                <tr> 
                  <td width="20%" class="label">Billing:</td>
                  <td width="56%" class="longtext">#URLDecode(getaddress.Add1)#, #URLDecode(getaddress.Add2)#, #URLDecode(getaddress.Add3)#, #getaddress.County#, #getaddress.Postcode#, #getaddress.Country#</td>
                  <td width="20%" class="longtext"><a href="changeadd.cfm?id=bill">change</a></td>
                  <td width="15%">&nbsp;</td>
                </tr>
               <tr> 
                  <td colspan="4">&nbsp;</td>
                </tr>
                <tr> 
                  <td class="label">Delivery:</td>
                  <td class="longtext"><cfif IsDefined("getaddress.delAdd1") AND getaddress.delAdd1 NEQ "">
                      #getaddress.delAdd1#, #getaddress.delAdd2#, #getaddress.delAdd3#, #getaddress.delCounty#, #getaddress.delPostcode#, #getaddress.delCountry# 
                      <cfelse>
                  AS ABOVE</cfif></td>
                  <td class="longtext"><a href="changeadd.cfm?id=del">change</a></td>
                  <td>&nbsp;</td>
                </tr>
                <tr> 
                  <td>&nbsp;</td>
                  <td class="highlight" colspan="2">&nbsp;</td>
                  <td class="label">
				  </td>
                </tr>
                <tr> 
                  <td colspan="4">&nbsp;</td>
                </tr>
                <tr> 
                  <td>&nbsp;</td>
                  <td class="label">total goods value</td>
                  <td class="money">#LSCurrencyFormat(totals.goods, 'none')#</td>
                  <td>&nbsp;</td>
                </tr></cfoutput>
                <cfoutput>
				<tr> 
                  <td>&nbsp;</td>
                  <td class="label">VAT</td>
                  <td class="money">
			#LSCurrencyFormat(totals.tvat, 'none')#
				  </td>
                 <td>&nbsp;</td>
                </tr>
                <tr> 
                  <td>&nbsp;</td>
                  <td class="label">total order value</td>
                  <td class="money">
			#LSCurrencyFormat(totals.total, 'none')#
				  </td>
				  <td>&nbsp;</td>
                </tr>
                 <tr> 
                  <td colspan="4">&nbsp;</td>
                </tr>
               <tr> 
                  <td colspan="4">&nbsp;</td>
                </tr>
              </cfoutput> 
               <tr> 
                  <td colspan="4">
                  <span id="sale_terms"><cfoutput>#URLDecode(get_terms.page_content)#</cfoutput></span>
                  </td>
                </tr>
                 <tr> 
                  <td colspan="4">&nbsp;</td>
                </tr>
             <tr> 
                <td class="longtext"colspan="4">To ensure the highest level of security your order will be processed by Sagepay on their own secure site. None of your payment details are available to us.
			   </td>
              </tr>
               <tr>
                  <td colspan="2">
                  <input id="accept_terms" name="accept_terms" type="checkbox" value="1" /> I have read and understand the Terms of Sale
                  </td>
                <td class="longtext" colspan="2" align="right"><a href="#"><img src="images/pay_now_up.gif" alt="" name="sagepay" width="88" height="24" id="sagepay" onmouseover="MM_swapImage('sagepay','','images/pay_now_over.gif',1)" onmouseout="MM_swapImgRestore()" border="0" onclick="javascript:document.check_terms.submit()" /></a>
                </td>
               </tr>
    </table>
    </form>
              
         
		 	
	</div>
    <div class="clearline"></div>
    <div id="footer">
      <cfinclude template="footer.cfm">
      <div class="clearline"></div>
</div>
<!---    <div id="footer_pql">
        <a href="http://www.pqlwebsolutions.co.uk" target="_blank">site by PQL Web Solutions</a>
    </div>
--->
</div>
<div class="clearline"></div>
<script src="http://www.google-analytics.com/urchin.js" type="text/javascript"> 
</script>
<script type="text/javascript"> 
_uacct = "UA-1752540-1";
urchinTracker();
</script>
</body>
</html>
