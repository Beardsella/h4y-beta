<div class="navbar-wrapper">
  <div class="container">



    <div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
      <div class="container col-lg-12">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>

          <cfif isDefined("CGI.script_name") AND #CGI.script_name# eq "index.cfm">
          <div class="hidden-xs hidden-sm"><a class="navbar-brand" href="##home" >Hearing Products International ltd</a></div>
          <div class="visible-xs visible-sm"><a class="navbar-brand" href="##home">Hear 4 You</a></div>
          <cfelse>
          <div class="hidden-xs hidden-sm"><a class="navbar-brand" href="index.cfm">Hearing Products International ltd</a></div>
          <div class="visible-xs visible-sm"><a class="navbar-brand" href="index.cfm">Hear 4 You</a></div>
          </cfif>

          <div id="Hear4You-Logo"></div>
          <a class="pull-right visible-xs" href="tel:01612927374" id="nav-phone"><span class="glyphicon glyphicon-earphone hidden-xs"></span> (0161) 292 7374</a>
        </div>
        <div class="navbar-collapse collapse">
          <ul class="nav navbar-nav">
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown">Product Categories<b class="caret"></b></a>
              <ul class="dropdown-menu">
                <li class="dropdown-header">Our Product Categories</li>

                <cfoutput query="get_top_cats">
                    <li><a class="captalize" href="#SEO_url#">#URLDecode(cat_name)#</a></li>
                </cfoutput>

              </ul>
            </li>
            <li><a href="#what-we-do">What We Do</a></li>
            <li><a href="#our_skills">Our Skills</a></li>
            <li><a href="#contact_details">Contact Us</a></li>
            </ul>

          <!-- Trigger Cart modal -->
          <ul class="nav navbar-nav navbar-right">
            <li class="pull-right"><a data-toggle="modal" id="mycart-button" data-target="#Mycart" title="view cart"><span class="glyphicon glyphicon-shopping-cart"></span></a></li>
          </ul>

<!-- Modal -->
<div class="modal fade" id="Mycart" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" aria-labelledby="mycart-button">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">Your Shopping Cart</h4>
      </div>
      <div class="modal-body">
<div id="SmartCart" class="scMain">

<cfquery datasource="#application.DSN#" name="all_products">
  SELECT *
  FROM products
</cfquery>

<cfoutput query="all_products">

<cfquery datasource="#application.DSN#" name="prod_cat">
  SELECT categories.cat_name, product_cats.category_id, product_cats.product_id
  FROM product_cats
  INNER JOIN categories
  ON product_cats.category_id = categories.id
  WHERE product_cats.product_id = #all_products.id#
</cfquery>

<cfquery datasource="#application.DSN#" name="prod_img">
  SELECT image_name
  FROM product_images
  WHERE product_id = #all_products.id#
</cfquery>



  <input type="hidden" pimage="product-images/thumbs/#prod_img.image_name#"
  pprice="#price#" pdesc="#URLDecode(thumb_description)#"
  pcategory="#URLDecode(prod_cat.cat_name)#" pname="#URLDecode(title)#" pid="#id#"
  product_sku="#id#">

</cfoutput>

</div>      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div>
    </div>
  </div>
</div>




            <form class="navbar-form navbar-right visible-lg" role="search">
               <div class="form-group">
              <input type="text" class="form-control my-form-control" placeholder="Search">
              </div>
              <button type="submit" class="btn btn-primary float-left">Go</button>
            </form>

          <ul class="nav navbar-nav navbar-right">
              <li class="pull-right visible-lg"><a href="mailto:info@hear4you.com">info@hear4you.com</a></li>
              <li class="pull-right visible-lg"><a href="tel:+441614808003">Call (0161) 480 8003</a></li>
          </ul>
        </div>
      </div>
    </div>

  </div>
</div>
