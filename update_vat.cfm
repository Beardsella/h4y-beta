<cfsetting showdebugoutput="no" enablecfoutputonly="yes">

<cfif IsDefined("session.vatexempt") AND session.vatexempt eq 0>
	<cfset session.vatexempt = 1>
<cfelse>
	<cfset session.vatexempt = 0>
</cfif>

<cfquery datasource="#application.dsn#">
UPDATE shoppingcart
SET vat_exempt = #session.vatexempt#
WHERE cartid = '#session.visitorid#'
</cfquery> 

<cfset result = structNew()>

<cfset result.sgs= 'sgs'>

<cfwddx action="CFML2JS" input="#result#" toplevelvariable="r">	