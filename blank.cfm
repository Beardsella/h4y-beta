<!DOCTYPE html>
<html>

<!--- Blank template V 1.0  --->    
<head>

<cfinclude template="global_header.cfm">

</head>

<body><!--- START OF CONTENT --->

<!--- SIDE NAV --->
<div class="container col-lg-2 visible-lg">
  <cfinclude template="side_nav.cfm">
</div>

<div class="container col-lg-10" data-spy="scroll" data-target="#mysidenav" id="main_container">

  <cfinclude template="nav.cfm">

  <div class="space_100px"></div>

  <div class="row">
  
    <!--- CONTENT HERE --->



  </div>

  <hr class="featurette-divider">


</div><!-- /.container -->

<!-- FOOTER -->
<cfinclude template="footer.cfm">
<!--- /FOOTER --->

<cfinclude template="java_insert.cfm">

</body>
</html>