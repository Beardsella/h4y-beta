<cfif IsDefined("session.osuffix") AND session.osuffix neq ''>
	<cfset session.osuffix = session.osuffix +1>
<cfelse>
	<cfset session.osuffix = 1>
</cfif>

<cfquery name="cartlines" datasource="#application.dsn#">
SELECT A.*, B.vatexempt
FROM shoppingcart A INNER JOIN products B ON A.prodid = B.ID
WHERE A.cartID = '#SESSION.visitorid#'
</cfquery>

<!--- check the cart value --->
<cfinvoke 
	component="cfcs.cart"
	method="get_totals"
	returnvariable="totals">
</cfinvoke>

<cfquery name="latestrec" datasource="#application.dsn#">
SELECT max(custid) AS thisid FROM customerinfo WHERE customerid='#SESSION.visitorid#'
</cfquery>

<cfquery name="rCustomerInfo" datasource="#application.dsn#">
SELECT * FROM customerinfo WHERE custid=#latestrec.thisid#
</cfquery>

<!--- need to write this away as an unpaid order --->
<cfquery datasource="#application.dsn#">
INSERT INTO ordersummary (cartid, orderdate, customerid, customername, ordervalue, deliverycost, orderstatus)
VALUES ('#SESSION.visitorid#', #now()#, #rCustomerInfo.custid#, '#rCustomerInfo.firstname# #rCustomerInfo.lastname#', #totals.subtotal#, #totals.delivery#, 1)
</cfquery>
<!--- get the order id --->
<cfquery name="getid" datasource="#application.dsn#">
SELECT max(orderid) AS oid FROM ordersummary WHERE cartid = '#SESSION.visitorid#'
</cfquery>

<!--- and the order lines--->
<cfloop query="cartlines">
    <cfquery datasource="#application.dsn#">
    INSERT INTO orders (cartid, customerid, price, vat, quantity, cartdate, prodid, product_option, child_product)
    VALUES ('#SESSION.visitorid#', #rCustomerInfo.custid#, #cartlines.price#,
    <cfif cartlines.vat_exempt eq 0 OR cartlines.vat_exempt eq 1 AND cartlines.vatexempt eq 0>
    	#cartlines.vat#
    <cfelse>
    	0
    </cfif>
    , #cartlines.quantity#, #now()#, #cartlines.prodid#, 			
    <cfif IsDefined("cartlines.product_option") AND cartlines.product_option neq ''>
    	'#cartlines.product_option#'
    <cfelse>
    	null
    </cfif>
    ,
    <cfif IsDefined("cartlines.child_product") AND cartlines.child_product neq ''>
    	#cartlines.child_product#
    <cfelse>
    	null
    </cfif>
	)    
    </cfquery>
</cfloop>

<cfset finalvalue = totals.total>

<font color="#666633" size="2" face="Verdana, Arial, Helvetica, sans-serif">...transferring 
you to secure payment server</font>


<!-----------------------------------------------------------  

	ProtX

------------------------------------------------------------>

<cfsavecontent variable="address">
	<cfoutput>
	#URLDecode(rCustomerInfo.Add1)#
	#URLDecode(rCustomerInfo.Add2)#
	#URLDecode(rCustomerInfo.Add3)#
	#URLDecode(rCustomerInfo.County)#
	#URLDecode(rCustomerInfo.Country)#
	</cfoutput>
</cfsavecontent>

<cfif IsDefined("rCustomerInfo.delAdd1") AND rCustomerInfo.delAdd1 neq ''>
	<cfsavecontent variable="d_address">
		<cfoutput>
		#URLDecode(rCustomerInfo.delAdd1)#
		#URLDecode(rCustomerInfo.delAdd2)#
		#URLDecode(rCustomerInfo.delAdd3)#
		#URLDecode(rCustomerInfo.delCounty)#
		#URLDecode(rCustomerInfo.delCountry)#
		</cfoutput>
	</cfsavecontent>
<cfelse>
	<cfsavecontent variable="d_address">
		<cfoutput>
		#URLDecode(rCustomerInfo.Add1)#
		#URLDecode(rCustomerInfo.Add2)#
		#URLDecode(rCustomerInfo.Add3)#
		#URLDecode(rCustomerInfo.County)#
		#URLDecode(rCustomerInfo.Country)#
		</cfoutput>
	</cfsavecontent>
</cfif>
<!--- get the right delivery postcode --->
<cfif IsDefined("rCustomerInfo.delPostcode") AND rCustomerInfo.delPostcode neq ''>
	<cfset dPostcode = rCustomerInfo.delPostcode>
<cfelse>
	<cfset dPostcode = rCustomerInfo.Postcode>
</cfif>

<cfinvoke component="cfcs.protx" method="initial_post" returnvariable="out">
	
	<cfinvokeargument name="transaction_type" value="3">
	
	<cfinvokeargument name="vendor" value="hearingproduct1">
	<cfinvokeargument name="vendortxcode" value="#SESSION.visitorid#_#session.osuffix#">
	<cfinvokeargument name="amount" value="#numberFormat(finalvalue, '.00')#">
	<cfinvokeargument name="currency" value="GBP">
	<cfinvokeargument name="description" value="Hearing Products International Ltd purchase">
	<cfinvokeargument name="notificationurl" value="http://www.hear4you.com/sagepay_notify.cfm">
	
	<cfinvokeargument name="CustomerName" value="#URLDecode(rCustomerInfo.firstname)# #URLDecode(rCustomerInfo.lastname)#">
	<cfinvokeargument name="CustomerEmail" value="#URLDecode(rCustomerInfo.email)#">
	<cfinvokeargument name="BillingAddress" value="#address#">
	<cfinvokeargument name="BillingPostcode" value="#rCustomerInfo.Postcode#">
	<!--- Only include shipping info if supplied --->
	<cfif d_address NEQ ''>
		<cfinvokeargument name="DeliveryAddress" value="#d_address#">
		<cfinvokeargument name="DeliveryPostcode" value="#dPostcode#">
	</cfif>
	
	<cfinvokeargument name="ContactNumber" value="#rCustomerInfo.telephone#">
	
</cfinvoke>

<cfif out.status NEQ 'OK'>
	<cfoutput>
		#out.statusdetail#
	</cfoutput>
	<cfdump var="#out.arguments#">
<cfelse>
	<!-----------------------------------------
	
		Store VPSTxID A with Out Transaction ID
		
	------------------------------------------>	
	
	<!--- Send to PROTX --->
	<cflocation addtoken="false" url="#out.nexturl#">
	
</cfif>
  