<LINK REL="SHORTCUT ICON" HREF="images/h4y.ico" />

<!-- Bootstrap Scalabe screen size -->
<meta name="viewport" content="width=device-width, initial-scale=1.0">

<cfset CurrentPage = GetFileFromPath(GetTemplatePath())>

<cfquery datasource="#application.DSN#" name="get_top_cats">
  SELECT *
  FROM categories
  WHERE parentid = 1 AND display = 1 AND id <> 22
  ORDER BY cat_name
</cfquery>

<cfquery datasource="#application.DSN#" name="this_product">
  SELECT *
  FROM products
  WHERE SEO_URL like "#CurrentPage#" 
</cfquery>


<!--- If its a Product --->
<cfif this_product.recordcount gt 0>
  
  <cfquery datasource="#application.DSN#" name="the_meta_keywords">
    SELECT *
    FROM meta_tags
    WHERE prod_id = "#this_product.id#" 
  </cfquery>

<cfset this_meta_description = "#URLDecode(this_product.meta_description)#">
  
<title><cfoutput>#URLDecode(this_product.meta_title)# - Hear 4 You</cfoutput></title>

<cfelse>
<!--- If its not a product it must be a category --->

<cftry>
  <cfquery datasource="#application.dsn#" name="this_cat">
    SELECT *
    FROM categories
    WHERE "#CurrentPage#" LIKE SEO_url 
  </cfquery>

  <cfquery datasource="#application.DSN#" name="the_meta_keywords">
    SELECT *
    FROM meta_tags
    WHERE cat_id = "#this_cat.id#" 
  </cfquery>

  <cfquery datasource="#application.dsn#" name="related_products">
    SELECT product_cats.category_id, product_cats.product_id, products.*
    FROM product_cats
    JOIN products
    ON product_cats.product_id = products.id
    WHERE product_cats.category_id = #this_cat.id#
  </cfquery>

<cfset this_meta_description = "#URLDecode(this_cat.meta_description)#">

<title><cfoutput>#URLDecode(this_cat.meta_title)# - Hear 4 You</cfoutput></title>

<cfcatch>
  <title><cfoutput>Hearing Products International LTD</cfoutput></title>
</cfcatch>
</cftry>

</cfif>
<cftry>
  <meta name="Keywords" content="<cfoutput query='the_meta_keywords'>#URLDecode(name)#</cfoutput>" />
  <meta name="Description" content="<cfoutput>#this_meta_description#</cfoutput>" />

  <cfcatch>
  <meta name="Keywords" content="Hear4you, Hearing Products international, hearing aids, hearing loops" />
  <meta name="Description" content="Hearing Products international is the largest supplier of own brand Aids for Living in the country, particularily to Social Service Departments" />
  </cfcatch>
</cftry>
<cfif find("localhost", "#CGI.http_host#")>
  
  <!-- LESS -->
  <link href="css/bootstrap.less" rel="stylesheet">
  <link href="css/custom.less" rel="stylesheet">  

<cfelse>

  <!-- CSS -->
  <link href="css/bootstrap.css" rel="stylesheet">
  <link href="css/custom.css" rel="stylesheet">  

</cfif>

<link href="css/lightbox.css" rel="stylesheet" />
<link href="css/thumbnail_carousel.css" rel="stylesheet" />
<link href="css/thumbnail_rollover.css" rel="stylesheet" />

