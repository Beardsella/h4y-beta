<!DOCTYPE html>
<html>

<!--- Blank template V 1.0  --->    
<head>

<cfinclude template="global_header.cfm">

</head>

<body><!--- START OF CONTENT --->

<!--- SIDE NAV --->
<div class="container col-lg-2 visible-lg">
  <cfinclude template="side_nav.cfm">
</div>

<div class="container col-lg-10" data-spy="scroll" data-target="#mysidenav" id="main_container">

  <cfinclude template="nav.cfm">

  <div class="space_100px"></div>

  <div class="row">
  
    <!--- CONTENT HERE --->


  
        <div class="col-md-3">            
            <div class="thumbnail">
                <div class="caption">
                    <h4>Thumbnail Headline</h4>
                    <p>short thumbnail description</p>
                    <p><a href="" class="label label-danger" rel="tooltip" title="Zoom">Zoom</a>
                    <a href="" class="label label-default" rel="tooltip" title="Download now">Download</a></p>
                </div>
                <img src="http://lorempixel.com/400/300/sports/1/" alt="...">
            </div>
      </div>
      
        <div class="col-md-3">            
            <div class="thumbnail">
                <div class="caption">
                    <h4>Thumbnail Headline</h4>
                    <p>short thumbnail description</p>
                    <p><a href="" class="label label-danger" rel="tooltip" title="Zoom">Zoom</a>
                    <a href="" class="label label-default" rel="tooltip" title="Download now">Download</a></p>
                </div>
                <img src="http://lorempixel.com/400/300/sports/2/" alt="...">
            </div>
      </div>

        <div class="col-md-3">            
            <div class="thumbnail">
                <div class="caption">
                    <h4>Thumbnail Headline</h4>
                    <p>short thumbnail description</p>
                    <p><a href="" class="label label-danger" rel="tooltip" title="Zoom">Zoom</a>
                    <a href="" class="label label-default" rel="tooltip" title="Download now">Download</a></p>
                </div>
                <img src="http://lorempixel.com/400/300/sports/3/" alt="...">
            </div>
      </div>

        <div class="col-md-3">            
            <div class="thumbnail">
                <div class="caption">
                    <h4>Thumbnail Headline</h4>
                    <p>short thumbnail description</p>
                    <p><a href="" class="label label-danger" rel="tooltip" title="Zoom">Zoom</a>
                    <a href="" class="label label-default" rel="tooltip" title="Download now">Download</a></p>
                </div>
                <img src="http://lorempixel.com/400/300/sports/4/" alt="...">
            </div>
      </div>        
        
  

  </div>

  <hr class="featurette-divider">

  <!-- FOOTER -->
  <cfinclude template="footer.cfm">
  <!--- /FOOTER --->

</div><!-- /.container -->


<cfinclude template="java_insert.cfm">

</body>
</html>