
<!-- Include all compiled plugins (below), or include individual files as needed -->
<!---test---><script src="//ajax.googleapis.com/ajax/libs/jquery/1.3.2/jquery.min.js"></script>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>

<!--- Bootstrap ---><script src="js/bootstrap.min.js"></script>
<!-- Less Compiler --><script src="js/less.js" type="text/javascript"></script>
<!--- Lightbox ---><script src="js/lightbox-2.6.min.js"></script>


<script type="text/javascript">
  $(document).ready(function () {

  $('#label_html_css').popover('toggle')

  // $('#main_container').scrollspy({ target: '#mysidenav' }) 

  $(".navbar-nav li a[href^='#']").on('click', function(e) {

  // prevent default anchor click behavior
  e.preventDefault();

  // store hash
  var hash = this.hash;

  // animate
  $('html, body').animate({
  scrollTop: $(this.hash).offset().top
  }, 500, function(){

  // when done, add hash to url
  // (default click behaviour)
  window.location.hash = hash;
  });

  });
  $("#mysidenav a[href^='#']").on('click', function(e) {

  // prevent default anchor click behavior
  e.preventDefault();

  // store hash
  var hash = this.hash;

  // animate
  $('html, body').animate({
  scrollTop: $(this.hash).offset().top
  }, 500, function(){

  // when done, add hash to url
  // (default click behaviour)
  window.location.hash = hash;
  });

  }); 
  $(".index_page a[href^='#']").on('click', function(e) {

  // prevent default anchor click behavior
  e.preventDefault();

  // store hash
  var hash = this.hash;

  // animate
  $('html, body').animate({
  scrollTop: $(this.hash).offset().top
  }, 500, function(){

  // when done, add hash to url
  // (default click behaviour)
  window.location.hash = hash;
  });

  }); 

  // footer carousel - bootsnipp
    $('#myFooterCarousel').carousel({
      interval: 10000
  })


// thumnail rollover - bootsnipp
    $("[rel='tooltip']").tooltip();    
 
    $('.thumbnail').hover(
        function(){
            $(this).find('.caption').slideDown(250); //.fadeIn(250)
        },
        function(){
            $(this).find('.caption').slideUp(250); //.fadeOut(205)
        }
    ); 

  });




// SHOPPING CART
function cart_go()
{
  document.cartadd.submit() ;
}
function child_cart_go(prodid, child) {
  param = new Object();
  param.prodid = prodid
  param.child = child
  if (child != 9999) {
    param.pqty = document.form3.elements['qty'+child].value;
  }
  http( 'POST'  , 'aj_child_cart.cfm' , child_response, param ); 
}
function child_response(obj){ 
  var rcartval = obj.rcartval
  document.getElementById('cartval').innerHTML = obj.rcartval
  var thiskid = obj.thiskid
  if (thiskid != 0) {
  document.getElementById('carttot').innerHTML = obj.rcartval
  }
  alert("Successfully added to your cart")
}

</script>

<!--- Google Tracking Code For H4Y --->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-46084634-1', 'hear4you.com');
  ga('send', 'pageview');

</script>