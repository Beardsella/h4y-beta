<!DOCTYPE html>
<html>

<!--- cat_detail_template.cfm V 1.0  --->    
<head>

<cfinclude template="global_header.cfm">

</head>

<body><!--- START OF CONTENT --->

<!--- SIDE NAV --->
<div class="container col-lg-2 visible-lg">
  <cfinclude template="side_nav.cfm">
</div>

<div class="container col-lg-10" data-spy="scroll" data-target="#mysidenav" id="main_container">

  <cfinclude template="nav.cfm">

  <div class="space_50px"></div>

 <div id="products" class="row list-group">
<cfoutput query="this_cat">
  <h1>#URLDecode(cat_name)# <span class="badge">#related_products.recordcount#</span></h1>


    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
<p class="lead">#URlDecode(cat_description)#</p></div>
</cfoutput>
  
    <!--- CONTENT HERE --->

<cfoutput query="related_products">

  <cfquery datasource="#application.DSN#" name="related_images" maxrows="1">
    SELECT *
    FROM product_images
    WHERE product_id = #id#
    ORDER BY img_order
  </cfquery>

          <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3"> 
          <h4>#URLDecode(title)# - <span class="label label-warning">&pound; #Price#</span></h4>
            <div class="thumbnail">
                <div class="caption">
                    
                    <div class="space_20px hidden-xs"></div> 
                    <div class="space_10px visible-xs"></div> 
                    <p class="big hidden-xs">#URLDecode(thumb_description)#</p>
                    <p class="visible-xs">#URLDecode(thumb_description)#</p>
                    <p><a href="#URLDecode(SEO_URL)#" type="button" class="btn btn-primary">More Info</a>
                    <a type="button" class="btn btn-success">Add to Cart <span class="glyphicon glyphicon-shopping-cart"></span></a></p>
                </div>
                <img src="product-images/thumbs/#related_images.image_name#" class="img-responsive grid-img" alt="#URLDecode(related_images.image_caption)#">
            </div>
      </div>
</cfoutput>

  </div>

  <hr class="featurette-divider">

  <!-- FOOTER -->
  <cfinclude template="footer.cfm">
  <!--- /FOOTER --->

</div><!-- /.container -->


<cfinclude template="java_insert.cfm">

</body>
</html>