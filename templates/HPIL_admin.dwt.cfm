<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<cfif NOT IsDefined ("session.admininitials") OR session.admininitials eq 999>
	<cflocation url="../admin/index.cfm?passwordcheck=failed" addtoken="no">
</cfif>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<!-- TemplateBeginEditable name="doctitle" -->
<title>Website Admin</title>
<!-- TemplateEndEditable -->
<link href="../CSS/HPIL_admin.css" rel="stylesheet" type="text/css" media="all">

<!-- TemplateBeginEditable name="head" -->
<!-- TemplateEndEditable -->
</head>

<body>
<div id="container">
	<div id="logo"><img src="../images/HPIL_logo_gif.gif" /></div>
 	<div class="clearline"></div>
	<div id="nav">
			<cfinclude template="../admin/nav_admin.cfm">	
	</div>
	<div id="pagecontent">
	  	<div id="pagetitle">
	  	<!-- TemplateBeginEditable name="header" -->header<!-- TemplateEndEditable -->
	  	</div>
<div class="clearline"></div>
		
<!-- TemplateBeginEditable name="mainpanel" --><!-- TemplateEndEditable -->
	</div>
</div>
<div class="clearline"></div>
<!--- end container div --->
</body>
</html>
