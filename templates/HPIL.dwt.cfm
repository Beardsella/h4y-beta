<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<!-- TemplateBeginEditable name="doctitle" -->
<title>Aids for Living for Hearing and Speech Impaired People</title>
<!-- TemplateEndEditable -->
<link href="../CSS/HPIL.css" rel="stylesheet" type="text/css" media="all">
<link href="../CSS/HPIL_nav.css" rel="stylesheet" type="text/css" media="all">

<!-- TemplateBeginEditable name="head" -->
<!-- TemplateEndEditable -->
<cfparam name="session.cathide" default="0">

<script language="javascript" src="../javascripts/engine.js"></script>
<script language="javascript" src="../javascripts/scriptaculous.js"></script>
<script language="javascript" src="../javascripts/library.js"></script>
<script type="text/JavaScript">
<!--
function showsubs(cat, clink){ 
	param = new Object();
	param.cat = cat
	param.clink = clink
	http( 'POST'  , 'aj_showsubs.cfm' , nav_response, param ); 
}
function nav_response(obj){ 
	var thiscat = obj.thiscat
	var navlink = obj.navlink
		document.getElementById('subs'+thiscat).style.display = ''
	location.href = navlink
	var catstoreset = obj.catstoreset
	var catarrReset = catstoreset.split( "," );
	for (var i=0; i < catarrReset.length; i++) {
			document.getElementById('subs'+catarrReset[i]).style.display = 'none'
		}
}
function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
//-->
</script>

<!--- check the cart value --->
<cfinvoke 
	component="cfcs.cart"
	method="get_totals"
	returnvariable="totals">
</cfinvoke>
</head>

<body onload="MM_preloadImages('../images/brochure_link_over.gif')">
<div id="container">
	<div id="masthead"><cfinclude template="../header.cfm"></div>
	<div class="clearline"></div>
    <div id="leftcolumn">
		<div id="leftnav">
                    <cfinclude template="../nav.cfm">
  		</div>
		<div id="join_list">
                    <cfinclude template="../mailing_list.cfm">
		</div>
        <div class="gofloat_nowidth" style="margin-bottom:20px">
        <a href="product_pdfs/Hearing_Products_ISO9001_Certificate.pdf" target="_blank">
        <img src="images/ISO_9001.gif" width="160" alt="We are ISO9001 accredited" border="0" />
        </a>
        </div>
  <div id="fsb">
        <a href="http://www.fsb.org.uk/" target="_blank"><img src="../images/FSB_logo.gif" alt="View the FSB website" border="0" /></a>
  </div>
<img src="../images/credit_cards.gif" width="128" height="80" alt="We accept online payments" />
</div>
	<div id="pagecontent">
			<!-- TemplateBeginEditable name="mainpanel" -->			<!-- TemplateEndEditable -->	
	</div>
    <div class="clearline"></div>
    <div id="footer">
      <cfinclude template="../footer.cfm">
      <div class="clearline"></div>
</div>
<!---    <div id="footer_pql">
        <a href="http://www.pqlwebsolutions.co.uk" target="_blank">site by PQL Web Solutions</a>
    </div>
--->
</div>
<div class="clearline"></div>
<script src="http://www.google-analytics.com/urchin.js" type="text/javascript"> 
</script>
<script type="text/javascript"> 
_uacct = "UA-1752540-1";
urchinTracker();
</script>
</body>
</html>
