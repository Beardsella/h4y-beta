<!--- Set application name and enable Client and Session variables. ---> 
<cfapplication name="hear4you14" 
    clientmanagement="yes" 
    clientstorage="cookie"
    loginstorage="session"
    sessionmanagement="Yes"
    sessiontimeout="200"
    setdomaincookies="no"
    applicationtimeout="30"> 

<cfif find("localhost", "#CGI.http_host#")>
  
<cfset application.DSN = "h4y_beta">
<cfset application.dsn = "h4y_beta">

<cfelse>

<cfset application.DSN = "hear4you14">
<cfset application.dsn = "hear4you14">

</cfif>


<cfif NOT IsDefined("session.visitorid") OR session.visitorid eq 99>
	<cfset session.visitorid = createuuid()>
</cfif>
<!--- 
<cfset MappingStruct = { '/cfcs' : '/adamin/cfc' } /> --->

<cfcookie name="CFID" value="#session.CFID#">
<cfcookie name="CFTOKEN" value="#session.CFTOKEN#">