<!DOCTYPE html>
<html>

<!--- Blank template V 1.0  --->    
<head>

<cfinclude template="global_header.cfm">
<link href="css/smart_cart.css" rel="stylesheet" type="text/css">

</head>

<body><!--- START OF CONTENT --->

<!--- SIDE NAV --->
<div class="container col-lg-2 visible-lg">
  <cfinclude template="side_nav.cfm">
</div>

<div class="container col-lg-10" data-spy="scroll" data-target="#mysidenav" id="main_container">

  <cfinclude template="nav.cfm">

  <div class="space_100px"></div>

  <div class="row">
  
    <!--- CONTENT HERE --->





  </div>

  <hr class="featurette-divider">


</div><!-- /.container -->

<!-- FOOTER -->
<cfinclude template="footer.cfm">
<!--- /FOOTER --->

<cfinclude template="java_insert.cfm">

<script type="text/javascript" src="js/jquery.smartCart-2.0.js"></script>

<script type="text/javascript">
    $(document).ready(function(){  
      $('#SmartCart').smartCart();
    });
</script>

</body>
</html>