<!DOCTYPE html>
<html>

<!--- Blank template V 1.0  --->    
<head>

<cfinclude template="global_header.cfm">

  <cfparam name="session.vatexempt" default="0">

<cfquery name="reviewcart" datasource="#application.dsn#">
SELECT A.id, A.quantity, A.price, A.vat, A.prodid, A.child_product, A.vat_exempt, B.title, B.vatexempt
FROM shoppingcart A INNER JOIN products B ON A.prodid = B.id
WHERE A.cartid = '#session.visitorid#'
</cfquery>
<cfset plist = valuelist(reviewcart.prodid)>
<cfparam name="SESSION.sessionid" default="1">

<cfquery name="getcontent" datasource="#application.DSN#">
SELECT page_content FROM page_text WHERE id = 7
</cfquery>


<cfparam name="session.cathide" default="0">

<!--- check the cart value --->
<cfinvoke 
	component="cfcs.cart"
	method="get_totals"
	returnvariable="totals">
</cfinvoke>
</head>

<body><!--- START OF CONTENT --->

<!--- SIDE NAV --->
<div class="container col-lg-2 visible-lg">
  <cfinclude template="side_nav.cfm">
</div>

<div class="container col-lg-10" data-spy="scroll" data-target="#mysidenav" id="main_container">

  <cfinclude template="nav.cfm">

  <div class="space_100px"></div>

  <div class="row">

			
 <h2>Your shopping cart</h2>
			  <table width="100%" cellspacing="0" cellpadding="0">
                <cfif reviewcart.recordcount>
				  <tr>
				    <td colspan="6" class="data">&nbsp;</td>
				    <td class="longtext">&nbsp;</td>
		          </tr>
				  <tr>
                    <td class="longtext">&nbsp;</td>
                    <td class="label" width="100">QTY</td>
                    <td class="label" width="100">Goods</td>
                    <td class="label" width="100">VAT</td>
                    <td class="label" width="100">Total</td>
                    <td>&nbsp;</td>
                    <td class="longtext">&nbsp;</td>
                  </tr>
                   
 		<cfoutput query="reviewcart">
                   <cfif IsDefined("reviewcart.child_product") AND reviewcart.child_product neq ''>
                         <cfquery name="kid" datasource="#application.dsn#">
                         SELECT child_title FROM child_products WHERE id = #reviewcart.child_product#
                         </cfquery>
                    </cfif>
                     <tr>
                        <td class="longtext">
                        <a href="product_detail.cfm?id=#prodid#">#URLDecode(title)#</a>
                           <cfif IsDefined("reviewcart.child_product") AND reviewcart.child_product neq ''>
                           <br />#kid.child_title#
                           </cfif>
                        </td>
                        <td class="longtext"><input name="#id#" type="text" id="#id#" size="2" value="#reviewcart.quantity#" class="longtext" onkeyup="updateQTY(this.value, this.id)" o></td>
                        <td class="longtext">
                        <span id="linegoods#id#">#LSCurrencyFormat(price*quantity, 'none')#</span>
                        </td>
                        <td class="longtext">
                        <span id="linevat#id#">
           <cfif reviewcart.vat_exempt eq 0 OR reviewcart.vat_exempt eq 1 AND reviewcart.vatexempt eq 0>	
                        #LSCurrencyFormat(vat*quantity, 'none')#
            <cfelse>
            			#LSCurrencyFormat(0.00, 'none')#
            </cfif>
                        </span>
                       </td>
                       	<td class="longtext">
						<span id="linetotal#id#">
              <cfif reviewcart.vat_exempt eq 0 OR reviewcart.vat_exempt eq 1 AND reviewcart.vatexempt eq 0>          
                        #LSCurrencyFormat((price+vat)*quantity, 'none')#
              <cfelse>
                        #LSCurrencyFormat(price*quantity, 'none')#
              </cfif>
                        </span>
						</td>
                       	<td>&nbsp;</td>
                        <td width="120" class="longtext"><a href="cart_removeline.cfm?id=#reviewcart.id#">remove</a></td>
                      </tr>
                        <tr>
                          <td class="formlabel">&nbsp;</td>
                          <td class="formlabel">&nbsp;</td>
                          <td class="formlabel">&nbsp;</td>
                          <td class="longtext">&nbsp;</td>
                          <td class="longtext">&nbsp;</td>
                          <td>&nbsp;</td>
                          <td class="longtext">&nbsp;</td>
                        </tr>
         </cfoutput>
                    <tr>
                      <td class="formlabel">Totals</td>
                      <td class="longtext">&nbsp;</td>
                      <td class="longtext">
                        <span id="goods"><cfoutput>#LSCurrencyFormat(totals.goods, 'none')#</cfoutput></span>
                      </td>
                      <td class="longtext">
                        <span id="tvat"><cfoutput>#LSCurrencyFormat(totals.tvat, 'none')#</cfoutput></span>
                      </td>
                      <td class="longtext">
                        <span id="subtotal"><cfoutput>#LSCurrencyFormat(totals.subtotal, 'none')#</cfoutput></span>
                      </td>
                      <td class="longtext">&nbsp;</td>
                      <td class="longtext">&nbsp;</td>
                  </tr>
                     <tr>
                       <td colspan="7">&nbsp;</td>
                    </tr>
                     <tr>
                       <td colspan="7">&nbsp;</td>
                    </tr>
                  <tr>
                        <td width="39%">
                        </td>
                        <td class="formlabel">&nbsp;</td>
                        <td class="formlabel">Cart Total</td>
                        <td class="longtext">&nbsp;</td>
                        <td class="longtext">
						  <span id="carttotal">
						  &pound; <cfoutput>#LSCurrencyFormat(totals.subtotal,'none')#</cfoutput>
						  </span>
					    </td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                  </tr>
                  <tr>
                    <td width="39%"></td>
                        <td class="formlabel">&nbsp;</td>
                        <td class="formlabel">UK Delivery</td>
                        <td class="longtext">&nbsp;</td>
                        <td class="longtext">
						 <cfif IsDefined("totals.delivery") AND totals.delivery neq 0>
						 	&pound; <cfoutput>#LSCurrencyFormat(totals.delivery, 'none')#</cfoutput>
                         <cfelse>
                         	FREE
                         </cfif>
					    </td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                  </tr>
                  <tr>
                    <td width="39%"></td>
                    <td>&nbsp;</td>
                    <td class="formlabel">&nbsp;</td>
                    <td class="longtext">&nbsp;</td>
                    <td class="longtext">&nbsp;</td>
                    <td colspan="2"><a href="checkout.cfm"><img src="images/pay_now_up.gif" name="pay" border="0" id="pay" onmouseover="MM_swapImage('pay','','images/pay_now_over.gif',1)" onmouseout="MM_swapImgRestore()" /></a>
                    </td>
                  </tr>
				  <tr>
				    <td width="39%"></td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                  </tr>
				  <tr>
				    <td width="39%"></td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>OR</td>
                    <td colspan="2">
                      <a href="<cfoutput>#CGI.HTTP_REFERER#</cfoutput>">
                      <img src="images/continue_shopping_up.gif" alt="Continue Shopping" name="continue" width="160" height="24" id="continue" onmouseover="MM_swapImage('continue','','images/continue_shopping_over.gif',1)" onmouseout="MM_swapImgRestore()" border="0" />
                      </a>
                    </td>
                  </tr>
                      <tr>
                        <td width="39%"></td>
                        <td class="longtext">&nbsp;</td>
                        <td class="longtext">&nbsp;</td>
                        <td class="longtext">&nbsp;</td>
                        <td class="longtext">&nbsp;</td>
                        <td class="longtext">&nbsp;</td>
                        <td class="longtext">&nbsp;</td>
                      </tr>
				  <cfelse>
				  <tr>
				    <td width="39%"></td>
                    <td colspan="6"><h3>Your shopping cart is empty</h3></td>
                  </tr>
                </cfif>
			  </table>			  
  </div>
         <div class="row">
		<div id="vat_relief">


<form action="cart_add.cfm" method="post" name="form3">
<input type="hidden" name="qty" value="1" size="2">
<div class="col-sm-3 col-md-3 col-lg-3 thumbnail" style="margin:10px;">
<div class="caption">
<h4>Domestic Room Loop System</h4>
<div class="space_10px"></div> 
<p>The Echo MegaLoop™ induction room loop system is for television, DVD or HiFi listening in the home, for people who have a hearing aid with a telecoil "T" setting. It's a powerful and extremely flexible induction loop amplifier designed with an exclusive background noise control.</p>
<p><a href="domestic-room-loop-system.cfm" type="button" class="btn btn-primary">More Info</a>
<a href="javascript:child_cart_go(18, 9999)" type="button" class="btn btn-success">Add to Cart <span class="glyphicon glyphicon-shopping-cart"></span></a></p>
</div>
<img src="product-images/thumbs/domestic-room-loop-system.jpg" style="max-width:230px; padding:4px;" alt="Domestic Room Loop System">
</div>
</form>



                         <h2>VAT Relief</h2>
                         <p>If you are disabled either by a hearing disability, visual disability, or have a communicative disability you may be eligible for VAT Relief for some items within our 'shop'. </p>

<p>To qualify for VAT exemption, you would need to be registered with your local health authority or GP as having a hearing disability or visual disability or for claiming vat relief for the product you are ordering.</p>

<p>If you are in any doubt as to whether you are eligible to receive goods or services exempt from VAT you should consult your local VAT office before signing the declaration. The VAT Act 1983 provides for severe penalties for anyone who makes use of a document which they know to be false for the purpose of obtaining VAT Relief.</p>

<p>(Notice 7012/7  VAT group 14 of the Zero Rate Schedule to the VAT Act of 1983 Vat relief for disabled people at <a href="http://www.hmrc.gov.uk" target="_blank">www.hmrc.gov.uk</a> or contact the VAT Helpline on 0845 010 9000).</p>

                         <p>If you are entitled to VAT Relief please tick this box <input name="relief" type="checkbox" onclick="updateVat(99)" <cfif IsDefined("session.vatexempt") AND session.vatexempt eq 1>checked</cfif> />
		</div>
         
  </div>

  <hr class="featurette-divider">


</div><!-- /.container -->

<!-- FOOTER -->
<cfinclude template="footer.cfm">
<!--- /FOOTER --->

<cfinclude template="special_java_insert.cfm">

</body>
</html>
